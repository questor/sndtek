
class Entry {
public:
   Entry() {}
   virtual ~Entry() {}

   virtual void init() = 0;
   virtual void deinit() = 0;

   virtual bool doMainLoop() = 0;

   virtual void imguiBeginFrame() = 0;
   virtual void imguiEndFrame() = 0;

   enum class KeyCode {
      eESC = 0,
   };

   virtual bool isKeyPressed(KeyCode keycode) = 0;
};

