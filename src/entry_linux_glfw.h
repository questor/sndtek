

#include "entry.h"

class EntryLinux : public Entry {
public:
   EntryLinux();

   void init();
   void deinit();

   bool doMainLoop();
   void imguiBeginFrame();
   void imguiEndFrame();

   bool isKeyPressed(KeyCode keycode);
};


