

#include "nodegrapheditor.h"

#include "nodes/noteinput.h"
#include "nodes/constoutput.h"
#include "nodes/output.h"
#include "nodes/generator.h"
#include "nodes/debug.h"
#include "nodes/operator.h"
#include "nodes/fmgenerator.h"
#include "nodes/noisegenerator.h"
#include "nodes/biquadfilter.h"
#include "nodes/compressor_webaudio.h"
#include "nodes/dumbdelay.h"
#include "nodes/dumbadsr.h"
#include "nodes/dx7_env.h"
#include "nodes/dx7_osc.h"
#include "nodes/dx7_lfo.h"
//#include "nodes/amigaklang.h"

namespace ImGui {

Node* SoundgenNodeFactory(int nt,const ImVec2& pos,const NodeGraphEditor& /*nge*/) {
   switch (nt) {
   case SNT_NOTE_INPUT:             return NoteInputNode::Create(pos);
   case SNT_CONST_OUTPUT:           return ConstOutputNode::Create(pos);
   case SNT_GENERATOR:              return GeneratorNode::Create(pos);
   case SNT_OUTPUT:                 return OutputNode::Create(pos);
   case SNT_DEBUG:                  return DebugNode::Create(pos);
   case SNT_OPERATOR:               return OperatorNode::Create(pos);
   case SNT_FMGENERATOR:            return FMGeneratorNode::Create(pos);
   case SNT_NOISEGENERATOR:         return NoiseGeneratorNode::Create(pos);
   case SNT_BIQUAD_FILTER:          return BiquadFilterNode::Create(pos);
   case SNT_COMPRESSOR_WEBAUDIO:    return CompressorWebaudioNode::Create(pos);
   case SNT_DUMBDELAY:              return DumbDelayNode::Create(pos);
   case SNT_DUMBADSR:               return DumbADSRNode::Create(pos);
   case SNT_DX7ENV:                 return Dx7EnvNode::Create(pos);
   case SNT_DX7OSC:                 return Dx7OscNode::Create(pos);
   case SNT_DX7LFO:                 return Dx7LfoNode::Create(pos);

#if 0
   case SNT_AK_VOLUME:              return AKVolumeNode::Create(pos);
   case SNT_AK_ADD:                 return AKAddNode::Create(pos);
   case SNT_AK_MUL:                 return AKMulNode::Create(pos);
   case SNT_AK_OSC_SINE:            return AKOscSineNode::Create(pos);
   case SNT_AK_OSC_SAW:             return AKOscSawNode::Create(pos);
   case SNT_AK_CONV_TO_FLOAT:       return AKConvToFloatNode::Create(pos);
   case SNT_AK_ENV_DECAY:           return AKEnvDecayNode::Create(pos);
   case SNT_AK_SVFILTER:            return AKSVFilterNode::Create(pos);
#endif

   default:
      IM_ASSERT(true);    // Missing node type creation
      return NULL;
   }
   return NULL;
}

} //namespace ImGui
