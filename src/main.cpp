
/*  http://blog.demofox.org/diy-synthesizer/

https://github.com/wavepot/wavep0t

https://github.com/opendsp

https://github.com/clone45/EquationComposer


Compressor:
https://christianfloisand.wordpress.com/2014/06/09/dynamics-processing-compressorlimiter-part-1/
https://christianfloisand.wordpress.com/2014/06/16/dynamics-processing-compressorlimiter-part-2/
https://christianfloisand.wordpress.com/2014/07/16/dynamics-processing-compressorlimiter-part-3/

http://music.tutsplus.com/tutorials/5-ways-to-enhance-any-synth-patch--audio-9786
http://www.earlevel.com/main/2013/06/03/envelope-generators-adsr-code/

https://github.com/mtytel/mopo

http://conspiracy.hu/articles/8/                 how to implement panning correctly

ChorusDelayLine: https://bitbucket.org/cascassette/chorusdelayline
Jelani FM Synthesizer: https://bitbucket.org/cascassette/jelani/src




The other thing to note is that there isn't one reverb algorithm that sounds good on all types of material. What sounds good on tonal stuff such as vocals may not sound good on impulsive material like drums. Also, EQing can perfom miracles.
EQ pre-reverb can do wonders. Same goes for delay.
When I work on (regular non-64k) mixes I tend to use one main reverb and one main delay, but 3 buses with different eq-settings routed into the actual delay/reverb buses.
I got the idea from this article, and used the curves as starting points for my own template:
How To EQ Effects(http://bobbyowsinski.blogspot.no/2012/06/how-to-eq-effects.html)
Wow, new trick, thanks. I've read many mixing related articles and some proper books, but the multiple-EQ-buses-in-front-of-single-reverb thing was new to me.


mod matrix and other ideas(i know it's your baby but maybe there are some that you haven't thought of):
-modulate the reverb with lfo/chorus/flange/filter
-modulate the oscillator/filter with reverb
-glide rnd waveform for lfo (quite rare but very useful)
-at least 2 delays for better ambient sound
-pitched noise
-loopable envelopes (double as lfo's)
-envelope routable to delay/reverb attack
-sub osc shape
-panning for every osc separately
-feedback
-alternating the LFO waveform with zero (Interlace)
added on the 2016-12-05 13:01:04 by 1in10 1in10


Actually i would just copy the lfo trig functionality from Elektron Monomachine.
It's quite ingenious.

TRIG (trig mode) is an important parameter for getting the most out of the LFO's. The trig modes are described in the list below, and their function is visualised in combination with the waveforms in Figure 3 on page 32.
• FREE = The LFO is running continuously, never restarting or stopping.
• TRIG = The waveform is restarted for each LFO-trig, and then runs continuously.
• HOLD = The waveform is running free in the background, but the output LFO level is latched for each LFO-trig and held still until the next LFO-trig.
• ONE = The waveform is restarted for each LFO-trig, then runs for one cycle and finally stops and holds the last LFO level.
• HALF = The waveform is restarted for each LFO-trig, then runs for one half cycle and finally stops and holds the last LFO level.

http://www.muzbar.ru/images/ArticleFiles/3007/3007_monomachine_manual_OS1.32.pdf
(Page 35)


drift: Like that, yeah. Just beware that S&H tends to get dirty/alias quite badly if you only do it per-sample and crank up the rate to the kilohertz range. Simple linear "interpolation" as in splitting the sample in before/after-S&H parts already does wonders there, and so does pre-filtering the carrier signal. Really depends on the type of sound you're after.

Also, the C64 noise generator is a 23bit LFSR random generator that has a very distinct tonality to it. Don't worry if you don't get that sound with white noise and S&H. If you really want t go that route, check the reSID source code, it's one of the best documented pieces of code I've ever read :)

Adding a simple 1-pole filter to your noise generators and make it adjustable is a good idea, too. Did that in V2 because I had a "pitch" and "color" parameter anyway that the noise gen didn't use, and it helped a lot.





http://www.earlevel.com/main/2012/12/15/a-one-pole-filter/

https://cytomic.com/files/dsp/SvfLinearTrapOptimised2.pdf

http://www.earlevel.com/main/category/digital-audio/oscillators/wavetable-oscillators/



*/


/*    FM:
The modulator amplitude (Am) determines the peak variation in frequency of the signal while the modulator frequency determines 
the rate at which the frequency changes. When the frequencies of both oscillators are in the audible range, the sidebands are 
also in the audible range. The result is a rich spectrum created by the varying sum and difference of the two frequencies. 
Modulator frequency is  often specified as a multiple of the carrier and expressed by the term c:m ratio. Integer multiples 
produce a harmonic spectrum while non-integer multiples produce an inharmonic spectrum.

We can produce FM by calculating two waveforms simultaneously and use the value from the first waveform to calculate the phase 
increment of the second waveform.

frqRad = twoPI / sampleRate;
modIncr = frqRad * modFrequency;
modPhase = 0;
modAmp = 100;
carPhase = 0;
for (n = 0; n < totalSamples; n++) {
    sample[n] = sin(carPhase);
    modValue = modAmp * sin(modPhase);
    carIncr = frqRad * (carFrequency + modValue);
    carPhase += carIncr;
    if (carPhase >= twoPI)
        carPhase -= twoPI;
    else if (carPhase < 0)
        carPhase += twoPI;

    modPhase += modIncr;
    if (modPhase >= twoPI)
        modPhase -= twoPI;
}
We can re-factor the calculation of the carrier phase as:

carIncr = frqRad * carFrequency;
modValue = (frqRad * modAmp) * sin(modPhase);
carPhase += carIncr + modValue
In this form, the values for frqRad*carFrequency and frqRad*modAmp are loop invariant and can be calculated once. Thus, instead 
of calculating the carrier phase increment repeatedly on each sample, we simply add the modulator value to the carrier phase.

modIncr = frqRad * modFrequency;
carIncr = frqRad * carFrequency;
modAmp = frqRad * 100;
modPhase = 0;
carPhase = 0;
for (n = 0; n < totalSamples; n++) {
   sample[n] = volume * sin(carPhase);
   modValue = modAmp * sin(modPhase);
   carPhase += carIncr + modValue;
   modPhase += modIncr;
}
In the first example we modified the phase increment, while the second example directly modifies the phase. For that reason it 
is sometimes called phase modulation rather than frequency modulation. However, both programs produce the same results.

The only significant difference between the two programs is in how we specify the modulator amplitude. The modulator amplitude 
affects the change in frequency of the carrier (Δf). When using the frequency modulation version, the modAmp value represents 
change in frequency and is limited to the maximum frequency range. When using the phase modulation version, the value represents
change in radians of phase increment and we need to convert the value from frequency to radians.

The value for modulator amplitude must be chosen carefully to avoid driving the carrier past the Nyquist frequency while also 
providing a good range of sum and difference tones. A high value for modulator amplitude will produce a "noisy" signal, which 
can sometimes be used as a special sound effect. But a useful maximum for Δf  is around 2,000, or about 1/10th of the Nyquist 
frequency.

The modulator amplitude affects the spectrum differently at varying carrier and modulator frequencies. For example, a carrier 
of 100Hz and a modulator amplitude of 50 will vary the carrier over the range of 50 to 150 Hz, or 50% of the carrier frequency. 
However, with a carrier at 1000Hz, a modulator amplitude of 50 will only vary the frequency from 950 to 1050Hz, or 5% of the 
frequency. This means that the timbre will vary considerably over the entire frequency range when we have a fixed modulator 
amplitude. For this reason, a good FM instrument uses the value of the "index of modulation" to calculate the modulator 
amplitude. Index of modulation is defined as I=Δf/fm where Δf is the peak frequency deviation and fm is the modulator frequency. 
The modulator amplitude determines the variation in frequency and is thus the same as Δf . We can calculate modulator amplitude 
as Am=I·fm. Using the index of modulation to calculate the modulator amplitude produces a timbre that remains constant across 
all frequencies.

We can extend this basic two-oscillator generator by adding an additional modulator and using two modulating signals on the carrier:

mod1Value = mod1Amp * sin(mod1Phase);
mod2Value = mod2Amp * sin(mod2Phase);
carPhase = carIncrement + mod1Value + mod2Value;
Or, we can cascade two or more modulators:

mod1Value = mod1Amp * sin(mod1Phase);
mod2Phase = mod2Phase + mod2Increment + mod1Value;
mod2Value = mod2Amp * sin(mod2Phase);
carPhase = carIncrement + mod2Value; 
Thus, by adding only a few additional lines of code, we can produce a very wide range of dynamically varying timbres. Note, 
however, that if we add multiple modulators to a single carrier, the index increment can potentially exceed the maximum phase 
increment. When adding multiple phase increments the total value must not exceed π or it will produce an alias frequency. 
Although we could test each modulator on each sample, it would be much more efficient to insure the peak amplitude of the 
modulators is within range before calculating samples, similar to what was done with summation of sine waves.

We are not limited to using sine waves for the carrier and modulator waveforms. We can start with a carrier that is produced 
as a sum of sine waves and then modulate that signal to produce an even more complex and rich spectrum. Finally, applying a 
separate envelope to each modulator allows modulation effects to grow or diminish, or vary in effect, over the duration of 
the sound and on a note to note basis. The wide variety of sounds available from only a few lines of programming is the 
reason so many synthesizer systems rely heavily on FM synthesis techniques.






DX7:
  + added the pitch-based phase to the current phase accumulator
  + added the current phase to the incoming modulation accumulator (M), if any, to produce P
  + looked up the logarithm of sin(P) in a table - call this A
  + looked up either the modulation index or the output level based on
  the EG, depending on whether the operator was a modulator or a carrier
  (two separate lookup tables), and added it to A (log(a) + log(b) =
  log(ab)) to get O
  + converted O into a linear value (using a combination of a shift
  register and another table lookup)
  + added that value either to the overall output, or to M

*/



#ifdef _WIN32
#include "entry_win32.h"
#define ENTRY_CLASS EntryWin32
#else
#include "entry_linux_glfw.h"
#define ENTRY_CLASS EntryLinux
#endif

#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <vector>
#include "imgui.h"

#include "soloud.h"

#include "player.h"
#include "importmidi.h"

int main(void) {

   SoLoud::Soloud soloud;

   SoLoud::result res = soloud.init(SoLoud::Soloud::CLIP_ROUNDOFF);  //SoLoud::Soloud::ENABLE_VISUALIZATION
   if(res != 0) {
      printf("Error during SoLoud initialization %d (%s)\n", res, soloud.getErrorString(res));
   }
   printf("SoLoud Backend: %s\n", soloud.getBackendString());
//   soloud.setGlobalVolume(0.75);
//   soloud.setPostClipScaler(0.75);

   Player player;
   //player.setWaterheadTestSong();
   loadmidi("miditests/cuddly.mid", player.getMusic());

   Entry *entry = new ENTRY_CLASS();
   entry->init();

   ImGui::ResetStyle(ImGuiStyle_DarkOpaque);

   int handle = soloud.play(player);
   soloud.setVolume(handle, 1.0f);            // Set volume; 1.0f is "normal"

   bool finished = false;
   while (!entry->isKeyPressed(Entry::KeyCode::eESC)) {
      
      if(entry->doMainLoop())
         break;

      entry->imguiBeginFrame();
      player.doImgui();
      entry->imguiEndFrame();

   }

   entry->deinit();
   delete entry;

   return 0;
}

