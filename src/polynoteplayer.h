
#ifndef __POLYNOTEPLAYER_H__
#define __POLYNOTEPLAYER_H__

#if 0

#include "instrument.h"

struct PolyNotePlayer {
   static const int numberPolyVoices = 32;
   int mNumberActiveVoices;

   PolyNotePlayer() {
      mNumberActiveVoices = 0;
      for (int i = 0; i < numberPolyVoices; ++i) {
         voices[i].active = false;
      }
   }

   struct PolyVoice {
      bool active;
      Note currentNote;

      InstrumentState state;
   };
   int noteOn(Note &newNote, int oldVoice, InstrumentDescr &descr) {
      //check if slide is used, if yes process old voice, if not use not active channel
      if (newNote.effects & Note::eSlide) {
         //do slide

         return oldVoice;
      } else {
         int voiceToUse = -1;
         for (int i = 0; i < numberPolyVoices; ++i) {
            if (voices[i].active == false) {
               voiceToUse = i;
               break;
            }
         }
         if (voiceToUse == -1) {
            //no voice free, what to do?
         } else {
            mNumberActiveVoices += 1;
            voices[voiceToUse].active = true;
            voices[voiceToUse].currentNote = newNote;
            voices[voiceToUse].state.noteOn(descr, newNote);
         }
         return voiceToUse;
      }
   }
   void renderSamples(float *outputBuffer, int numberSamples) {
      memset(outputBuffer, 0, sizeof(float) * 2 * numberSamples);

#undef WRITE_AFTER_ONE_NOTE
#ifdef WRITE_AFTER_ONE_NOTE
      static int debugState = 0;
#endif
      for (int i = 0; i < numberPolyVoices; ++i) {
         if (voices[i].active) {
            bool keepActive = voices[i].state.renderSamples(outputBuffer, numberSamples);
            if (!keepActive) {
               mNumberActiveVoices -= 1;
               voices[i].active = false;
#ifdef WRITE_AFTER_ONE_NOTE
               debugState = 1;
#endif
            }
         }
      }
#ifdef WRITE_AFTER_ONE_NOTE
      static std::vector<float> writeDebug;
      if (debugState != 1) {
         for (int i = 0; i < numberSamples; ++i) {
            writeDebug.push_back(outputBuffer[i * 2 + 0]);
         }
      } else if (debugState == 1) {
         for (int i = 0; i < numberSamples; ++i) {
            writeDebug.push_back(outputBuffer[i * 2 + 0]);
         }
         FILE *fp = fopen("debug.csv", "wb");
         float *writePtr = &writeDebug[0];
         for (int i = 0; i < writeDebug.size(); ++i) {
            fprintf(fp, "%d;%f\n", i, (*writePtr) * 10000);
            writePtr += 1;
         }
         fclose(fp);
         debugState = 2;
      }
#endif
   }
   PolyVoice voices[numberPolyVoices];
};

#endif


#endif
