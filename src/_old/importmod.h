
#ifndef __IMPORTMOD_H__
#define __IMPORTMOD_H__

class ImportMod {
   int basePTable[61] = {
      0, 1712,1616,1525,1440,1357,1281,1209,1141,1077,1017, 961, 907,
      856, 808, 762, 720, 678, 640, 604, 570, 538, 508, 480, 453,
      428, 404, 381, 360, 339, 320, 302, 285, 269, 254, 240, 226,
      214, 202, 190, 180, 170, 160, 151, 143, 135, 127, 120, 113,
      107, 101,  95,  90,  85,  80,  76,  71,  67,  64,  60,  57,
   };
   typedef struct {
      char name[22];
      uint16_t length;
      int8_t finetune;
      uint8_t volume;
      uint16_t loopStart;
      uint16_t loopLen;
   } Sample;

   typedef struct {
      struct {
         int sample;
         int note;
         int fx;
         int fxParam;
      } mEvents[64][4];
      void load(uint8_t *ptr) {
         for(int row=0; row<64; ++row) {
            for(int ch=0; ch<4; ++ch) {
               Event &e = Events[row][ch];
               e.sample = (ptr[0]&0xf0)|(ptr[2]>>4);
               e.fx = ptr[2] & 0x0f;
               e.fxParam = ptr[3];

               e.note = 0;
               int period = (int(ptr[0]&0x0f)<<8)|ptr[1];
               int bestD = abs(period-basePTable[0]);
               if(period) {
                  for(int i=1; i<=60; ++i) {
                     int d = abs(period-basePTable[i]);
                     if(d < bestD) {
                        bestD = d;
                        e.note = i;
                     }
                  }
               }
               ptr += 4;
            }
         }
      }
   } Pattern;

   uint8_t mPatternList[128];
   int mPatternCount;
   Pattern mPatterns[128];

public:
   void loadMod(uint8_t *moddata) {
      moddata += 20;          //skip name
      int sampleCount = 16;
      int channelCount = 4;

      moddata += 15*sizeof(Sample);
      uint32_t &tag = *(uint32_t*)(moddata+130+16*sizeof(Sample));
      switch(tag) {
      case '.K.M':
      case '4TLF':
      case '!K!M':
         sampleCount = 32;
         break;
      }

      if(sampleCount > 16) {
         moddata += (sampleCount-16)*sizeof(Sample);
      }

      moddata += 2;        //positionCount + unused byte

      memcpy(mPatternList, moddata, 128);
      moddata += 128;

      if(sampleCount > 15) {
         moddata += 4;           //skip tag
      }

      mPatternCount = 0;
      for(int i=0; i<128; ++i) {
         if(patternList[i] > mPatternCount) {
            mPatternCount = patternList[i];
         }
      }
      mPatternCount += 1;

      for(int i=0; i<mPatternCount; ++i) {
         mPatterns[i].load(moddata);
         moddata += 64*4*4;
      }

   }

};

#endif

