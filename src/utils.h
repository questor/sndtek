
#ifndef __UTILS_H__
#define __UTILS_H__

#include <math.h>

static const float cPI = 3.1415926535897932384626433832795f;
static const float cTwoPI = cPI*2.0f;
static const int cSampleRate = 44100;

#undef min     //fuck you windows.h!
#undef max

template<typename T>
static inline T min(T a, T b) {
   return a < b ? a : b;
}

template<typename T>
static inline T max(T a, T b) {
   return a > b ? a : b;
}

template<typename T>
static inline T clamp(T x, T min, T max) {
   return (x < min) ? min : (x > max) ? max : x;
}

template<typename T>
static inline T abs(T v) {
   return v < ((T)0.0) ? -v : v;
}

#define arraySize(array) (sizeof(array)/sizeof(array[0]))

static inline float db2linear(float db) {          //dB to linear
   return powf(10.0f, 0.05f * db);
}

static inline float linear2db(float lin) {         //linear to dB
   return 20.0f * log10f(lin);
}

#endif

