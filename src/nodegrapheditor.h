
#ifndef __NODEGRAPHEDITOR_H__
#define __NODEGRAPHEDITOR_H__

#include "extlibs/imgui/imgui.h"
#undef IMGUI_DEFINE_PLACEMENT_NEW
#define IMGUI_DEFINE_PLACEMENT_NEW
#undef IMGUI_DEFINE_MATH_OPERATORS
#define IMGUI_DEFINE_MATH_OPERATORS
#include <imgui_internal.h>

#include "extlibs/imgui/addons/imguinodegrapheditor/imguinodegrapheditor.h"

#include <vector>

namespace ImGui {

enum SoundgenNodeTypes {
   SNT_NOTE_INPUT = 0,
   SNT_CONST_OUTPUT,
   SNT_GENERATOR,
   SNT_OUTPUT,       //always stereo?
   SNT_DEBUG,
   SNT_OPERATOR,
   SNT_FMGENERATOR,
   SNT_NOISEGENERATOR,
   SNT_BIQUAD_FILTER,
   SNT_COMPRESSOR_WEBAUDIO,
   SNT_DUMBDELAY,
   SNT_DUMBADSR,
   SNT_DX7ENV,
   SNT_DX7OSC,
   SNT_DX7LFO,

#if 0
   SNT_AK_VOLUME,       //AmigaKlang modules
   SNT_AK_OSC_SAW,
   SNT_AK_OSC_TRI,
   SNT_AK_SAMPLE_HOLD,
   SNT_AK_OSC_SINE,
   SNT_AK_OSC_PULSE,
   SNT_AK_OSC_NOISE,
   SNT_AK_ENV_ATTACK,
   SNT_AK_ENV_DECAY,
   SNT_AK_ADD,
   SNT_AK_MUL,
   SNT_AK_DELAY,
   SNT_AK_COMBFILTER,
   SNT_AK_REVERB,
   SNT_AK_CTRL_SIGNAL,
   SNT_AK_SVFILTER,
   SNT_AK_CHORDGEN,
   SNT_AK_LOOPGEN,
   SNT_AK_CONV_TO_FLOAT,   //last node to output float to sound output node
#endif

   SNT_COUNT
};
static const char* SoundgenNodeTypeNames[SNT_COUNT] = {
   "NoteInput", "ConstOutput", "Generator", "Output", "Debug", "Operator", "FMGenerator", "NoiseGenerator",
   "BiquadFilter", "CompressorWebAudio", "DumbDelay", "DumbADSR", "DX7 Env", "DX7 Osc", "DX7 Lfo",

#if 0
   "AK:Volume", "AK:OscSaw", "AK:OscTri", "AK:SampleHold", "AK:OscSine", "AK:OscPulse",
   "AK:OscNoise", "AK:EnvAttack", "AK:EnvDecay", "AK:Add", "AK:Mul", "AK:Delay",
   "AK:CombFilter", "AK:Reverb", "AK:CtrlSignal", "AK:SVFilter", "AK:ChordGen", "AK:LoopGen",
   "AK:ConvToFloat",
#endif
};

class ProcessingNode : public Node {
public:
   ProcessingNode(int numberWorkBuffers) : Node() {
      mWorkBuffers = new ImVector<float>[numberWorkBuffers];
      mNumberWorkBuffers = numberWorkBuffers;
   }
   virtual ~ProcessingNode() {
      delete[] mWorkBuffers;
      mWorkBuffers = nullptr;
      mNumberWorkBuffers = 0;
   }
   int mNumberNotReadyInputSlots;
   int mNumberNotReadyOutputSlots;

   typedef struct {
      ProcessingNode* node;
      int slot;
   } NodeWithSlot;

   ImVector<NodeWithSlot> mInputNodes;        //cache tree structure and recrecte from grapheditor if needed!
   ImVector<NodeWithSlot> mOutputNodes;

   int mNumberWorkBuffers;
   ImVector<float> *mWorkBuffers;

   //helper function to get working buffers for processing data
   bool getInputNodeSlot(int index, ProcessingNode **nodePtr, int *slot) {
      *nodePtr = mInputNodes[index].node;     //the source node for the sound data
      *slot = mInputNodes[index].slot;    //the index of the workingbuffer of the other node connected to this node
      if(*nodePtr == nullptr)
         return false;
      return true;
   }

   virtual void noteOn() {}
   virtual void noteOff() {}

   virtual void renderSamples(int numberSamples) = 0;
};

Node* SoundgenNodeFactory(int nt,const ImVec2& pos,const NodeGraphEditor& /*nge*/);

}     //namespace ImGui

#endif   //#ifndef __NODEGRAPHEDITOR_H__
