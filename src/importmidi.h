
#ifndef __IMPORTMIDI_H__
#define __IMPORTMIDI_H__

#include "player.h"

void loadmidi(char *filename, Music &music);

#endif   //#ifndef __IMPORTMIDI_H__
