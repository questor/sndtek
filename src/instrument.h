
#ifndef __INSTRUMENT_H__
#define __INSTRUMENT_H__

#include "imgui.h"

#include "nodegrapheditor.h"
#include "nodegraphprocessor.h"
#include "nodes/output.h"

#include "utils.h"

struct Instrument {
   ImGui::NodeGraphEditor *mNodeGraphEditor;        //both of this are tightly coupled, processor knows the address of the editor, so don't move it around in memory!
   NodeGraphProcessor mNodeGraphProcessor;

   Instrument() {
      mNodeGraphEditor = new ImGui::NodeGraphEditor();
      mNodeGraphProcessor.setEditor(mNodeGraphEditor);

      for(int i=0; i<arraySize(generatedWaveOneChannel); ++i) {
         generatedWaveOneChannel[i] = 0.0f;
      }
   }
   ~Instrument() {
      delete mNodeGraphEditor;
   }

   void noteOn(float frequency) {
      mFrequency = frequency;
      mNodeGraphProcessor.noteOn();
   }
   void noteOff() {
      mNodeGraphProcessor.noteOff();
   }

   void renderSamples(float *outputBuffer, int numberSamples) {
/*    //Detune:
      while (detuneStates.size() < numberDetuneWaves) {
         PhaseState s;
         detuneStates.push_back(s);
      }
      float mulFactor = 1.0f;
      for (int i = 0; i < numberDetuneWaves; ++i) {
        // w473rh34d: this once was "mFrequency >> + << mulFactor * detune", but that causes inharmonic detuning
       generateSquareWave<AddPolicy>(instrData, numberSamples, mFrequency * pow(detune, i), detuneStates[i]);
       mulFactor = -mulFactor;
      }
      //ADSR:
      adsr.manipulate(instrData, numberSamples);

      //Delay:
      if (useDelay) {
         //TODO: clear delay buffer is just switched on to clear old values left in buffer
         delay.manipulate(instrData, numberSamples);
      }                 */
      //********************************************************************************

      mNodeGraphProcessor.initNodes(numberSamples, mFrequency);
      mNodeGraphProcessor.updateCachedInfos();
      mNodeGraphProcessor.processNodes(numberSamples);
      //now find output node and copy workbuffer to output buffer
      static ImVector<ImGui::Node*> nodes;
      mNodeGraphEditor->getAllNodesOfType(ImGui::SNT_OUTPUT, &nodes);
      if(nodes.size() > 0) {
         assert(nodes.size() == 1);             //we should have only one output node, otherwise we don't know which one to take

         ImGui::OutputNode *outputNode = (ImGui::OutputNode*)nodes[0];
         if(outputNode->mNumberNotReadyInputSlots == NodeGraphProcessor::cAlreadyProcessedThisFrameMarker) {             //make sure it was already processed this frame (internal marker)
            outputNode->createOutputBuffer(numberSamples, outputBuffer);
         }
      }

      //DebugOutput:
      for(int i=0; i<100; ++i) {
         generatedWaveOneChannel[i] = *(outputBuffer+i*2);
      }
   }

   void doImgui() {
      //Should be moved further into the generators/modificators!
/*
      ImGui::Begin("Generator");
      ImGui::DragInt("NumberDetunes", &numberDetuneWaves, 1, 0, 20);
      ImGui::DragFloat("DetuneFreq", &detune, 0.0001f, 0.0f, 100.0f, "%0.4f");

      adsr.doImgui();

      ImGui::Checkbox("Delay", &useDelay);
      if (useDelay) {
         delay.doImgui();
      }
      ImGui::End();  //Delay
*/
      ImGui::Begin("Generated Wave");
      ImGui::PlotLines("Wave", generatedWaveOneChannel, arraySize(generatedWaveOneChannel),
                       0, "", -1.0f, 1.0f, ImVec2(160, 80));
      ImGui::End();  //Generated Wave

      //////////////////////////////////////////////////////////////////////////////////////
      // NodeGraphEditor
      if (ImGui::Begin("Node Graph", NULL)) {
         if (mNodeGraphEditor->isInited()) {
            mNodeGraphEditor->registerNodeTypes(ImGui::SoundgenNodeTypeNames, ImGui::SNT_COUNT, ImGui::SoundgenNodeFactory, NULL, -1);
            mNodeGraphEditor->registerNodeTypeMaxAllowedInstances(ImGui::SNT_OUTPUT, 1);
            //mNodeGraphEditor->show_style_editor = true;
            mNodeGraphEditor->show_left_pane = false;
         }
         mNodeGraphEditor->render();
      }
      ImGui::End();
   }

   void loadInstrument(char *filename) {
      mNodeGraphEditor->clear();
      mNodeGraphEditor->load(filename);
   }

   void saveInstrument(char *filename) {
      mNodeGraphEditor->save(filename);
   }

   void importSysex(char *filename);


   //Debug:
   float generatedWaveOneChannel[100];

private:
   float mFrequency;

};

#endif
