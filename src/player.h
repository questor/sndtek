
#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <vector>
#include <stdint.h>

#include "instrument.h"
#include "utils.h"
#include "polynoteplayer.h"

#include "soloud.h"

typedef struct _Track {
   _Track() :
      lastMusicPlayIndex(0),
      mInstrument(nullptr)
   { }

   enum class MidiType {
      eNoteOn,
      eNoteOff,
   };
   typedef struct _MidiNote {
      MidiType mType;
      uint32_t mTime;      //in ms
      uint8_t mNote;
      uint8_t mVelocity;
      uint32_t mNoteLength; //in ms, used to draw the notes
      uint8_t mChannel; //midichannels are only up to 16

      uint16_t mId;     //usd to identify note-on to note-off

      bool operator < (const _MidiNote &evt) {
         return mTime < evt.mTime;
      }
       _MidiNote &type(MidiType type) { mType = type; return *this; }
       _MidiNote &time(uint32_t time) { mTime = time; return *this; }
       _MidiNote &note(uint8_t note) { mNote = note; return *this; }
       _MidiNote &length(uint32_t length) { mNoteLength = length; return *this; }    
       _MidiNote &channel(uint8_t channel) { mChannel = channel; return *this; }
       _MidiNote &id(uint16_t id) { mId = id; return *this; }
       _MidiNote &velocity(uint8_t vel) { mVelocity = vel; return *this; }
   } MidiNote;

   std::vector<MidiNote> mMidiNotes;
   int32_t lastMusicPlayIndex;

   Instrument *mInstrument;      //this needs to be managed externally by the user of mTracks! so if a track gets deleted first delete this instance!
   //this also needs to be a pointer because inside of the instrument there is a tight
   //coupling between two components that forbid to move the (once created) instrument
   //object in memory (because the address of an object inside is stored "globally")
} Track;

typedef struct _Music {
   ~_Music() {
      clearAll();             //delete instrument instances
   }

   typedef struct {
      //Instrument instrument;
      int instrumentNumber;
      typedef struct {
         int volumeTranspose;
         int noteTranspose;
         int patternNumber;
      } PatternDescr;
      std::vector<PatternDescr> patternlist;
   } PatternListElement;
   std::vector<PatternListElement> mPatternLists;

   std::vector<Track> mTracks;

   void setPatternLength(int newLength) {
      for(int i=0; i<mPatternLists.size(); ++i) {
         mPatternLists[i].patternlist.resize(newLength);
      }
   }

   void clearAll() {
      mPatternLists.clear();
      for(int i=0; i<mTracks.size(); ++i) {
         delete mTracks[i].mInstrument;
      }
      mTracks.clear();
   }
} Music;

class Player : public SoLoud::AudioSourceInstance,
               public SoLoud::AudioSource {
public:
   Player();

   virtual AudioSourceInstance *createInstance() {
      return this;
   }


   void setWaterheadTestSong();

   virtual unsigned int getAudio(float *aBuffer, unsigned int aSamplesToRead, unsigned int aBufferSize);
   virtual bool hasEnded();

   void doImgui();

   Music &getMusic() {
      return mMusic;
   }

private:
   void doPianoRollImgui();
   void doPatternImgui();
   void doMainControlImgui();

   void tickPlayer();

#if 0
   void addMidiNoteOnEvent(uint32_t msTime, int note, int channel) {
      Track::Event event;
      event.msTime = msTime;
      event.type = Track::eNoteOn;
      event.parameter.NoteOn.note = note;
      event.parameter.NoteOn.channel = channel;
      mMusic.mTracks[0].mEvents.push_back(event);
   }
   void addMidiNoteOffEvent(uint32_t msTime, int channel) {
      Track::Event event;
      event.msTime = msTime;
      event.type = Track::eNoteOff;
      event.parameter.NoteOff.channel = channel;
      mMusic.mTracks[0].mEvents.push_back(event);
   }
#endif

   Music mMusic;
   int mCurrentEditTrack;
   float mMasterVolume;

   int32_t mBPM;
   int32_t absoluteMusicTimeMs;

   enum {
      eStopped, ePlay, ePlayTrack, ePlayPattern
   };

   int playState;

};

#endif   //#ifndef __PLAYER_H__
