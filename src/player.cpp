
#include "player.h"
#include <algorithm>
#include "imgui.h"


static float seqZoomX = 1.0f;
static float seqZoomY = 1.0f;

static int canvasPosX = 0;
static int canvasPosY = 0;

ImVec2 logicalToScreen(float srcX, float srcY) {
   return ImVec2(canvasPosX + srcX * seqZoomX, canvasPosY + srcY * seqZoomY);
}
ImVec2 screenToLogical(ImVec2 src) {
   return ImVec2((src.x - canvasPosX)  / seqZoomX, (src.y - canvasPosY) / seqZoomY);
}

Player::Player()
   : mBPM(125*4)
   , absoluteMusicTimeMs(0)
   , playState(eStopped)
   , mCurrentEditTrack(0)
   , mMasterVolume(1.0f)
{
}

unsigned int Player::getAudio(float *outputBuffer, unsigned int numberSamples, unsigned int aBufferSize) {
   if(playState == eStopped) {
      //playstate is stopped, fill with zero!
      for(int i=0; i<numberSamples; ++i) {
         outputBuffer[i] = 0.0f;
      }
      return numberSamples;
   }

   int numberSamplesToPlayer = numberSamples;
   while (numberSamplesToPlayer > 0) {
      const int cBlockToProcess = 128;
      int numberCalcNextBlock = numberSamplesToPlayer < cBlockToProcess ? numberSamplesToPlayer : cBlockToProcess;

      absoluteMusicTimeMs += 1000 * numberCalcNextBlock / cSampleRate;

      tickPlayer();

      numberSamplesToPlayer -= numberCalcNextBlock;

      //TODO: use ALL instruments
      memset(outputBuffer, 0, sizeof(float) * numberCalcNextBlock);
      if(mCurrentEditTrack >= 0 && mCurrentEditTrack < mMusic.mTracks.size()) {
         mMusic.mTracks[mCurrentEditTrack].mInstrument->renderSamples(outputBuffer, numberCalcNextBlock);

         for(int i=0; i<numberCalcNextBlock; ++i) {
            outputBuffer[i] *= mMasterVolume;
         }

#if 0
         static float mFreq = (float)(1.0f / 44100.0f);
         static float mOffset = 0;
         static float phase = 0;
         for (int i = 0; i < numberCalcNextBlock; i++) {
            float t = (float)sin(440.0f * M_PI * 2 * mOffset);
            outputBuffer[i] = t;
            mOffset += mFreq;
         }
#endif

         //Debug: put one channel to draw channel
         Instrument *instrument = mMusic.mTracks[mCurrentEditTrack].mInstrument;
         int writeOffset = 0;
         int numberSamplesToCopy = arraySize(instrument->generatedWaveOneChannel);
         if (numberCalcNextBlock < arraySize(instrument->generatedWaveOneChannel)) {
            int numberOldSamples = arraySize(instrument->generatedWaveOneChannel) - numberCalcNextBlock;
            memmove(&instrument->generatedWaveOneChannel[0], &instrument->generatedWaveOneChannel[numberCalcNextBlock], numberOldSamples * sizeof(float));
            writeOffset = numberOldSamples;
            numberSamplesToCopy = numberCalcNextBlock;
         }

         for (int i = 0; i < numberSamplesToCopy; ++i) {
            instrument->generatedWaveOneChannel[i + writeOffset] = outputBuffer[i];
         }
      }
      outputBuffer += numberCalcNextBlock;
   }

   return numberSamples;
}

bool Player::hasEnded() {
   return false;
}

void Player::tickPlayer() {      //private
   for(int i=0; i<mMusic.mTracks.size(); ++i) {
      Track &track = mMusic.mTracks[i];
      if(track.mMidiNotes.size() == 0)
         continue;
      while(track.mMidiNotes[track.lastMusicPlayIndex].mTime <= absoluteMusicTimeMs) {

         //process event
         Track::MidiNote &event = track.mMidiNotes[track.lastMusicPlayIndex];
         switch(event.mType) {
         case Track::MidiType::eNoteOn:
            {
               int32_t note = event.mNote;

                  /* w473rh34d
                  Let's try MIDI compatible note style, so we can take midi-note indices 1:1, see e.g.
                  https://en.wikipedia.org/wiki/MIDI_Tuning_Standard
                  http://tonalsoft.com/pub/news/pitch-bend.aspx
                  */
//               float freq = (pow(2.0, ((12.0 * octave + note) - 69.0) / 12.0) * 440.0);
               float freq = (pow(2.0, (note - 69.0) / 12.0) * 440.0);
               if(mCurrentEditTrack >= 0 && mCurrentEditTrack < mMusic.mTracks.size())
                  mMusic.mTracks[mCurrentEditTrack].mInstrument->noteOn(freq);
            }
            break;
         case Track::MidiType::eNoteOff:
            if(mCurrentEditTrack >= 0 && mCurrentEditTrack < mMusic.mTracks.size())
               mMusic.mTracks[mCurrentEditTrack].mInstrument->noteOff();
            break;
         default:
            break;
         }
         track.lastMusicPlayIndex++;

         if(track.lastMusicPlayIndex >= track.mMidiNotes.size()) {
            track.lastMusicPlayIndex = 0;
            absoluteMusicTimeMs = 0;
         }
      }
   }
}


void Player::doPianoRollImgui() {
   ImGui::Begin("NoteEditor");

   static int currentEditPattern = 0;

   //Statusline
   {
      ImGui::Text("Pattern: %d(%d)", currentEditPattern, (int)mMusic.mPatternLists.size());
      ImGui::SameLine();
      if(ImGui::SmallButton(" + ##p1")) {
         if(currentEditPattern < mMusic.mPatternLists.size()) {
            ++currentEditPattern;
         }
      }
      ImGui::SameLine();   
      if(ImGui::SmallButton(" - ##p1")) {
         if(currentEditPattern > 0) {
            --currentEditPattern;
         }
      }
      ImGui::SameLine();
      ImGui::Text("   Pattern:");
      ImGui::SameLine();
      if(ImGui::SmallButton("add back")) {
         Track track;
         track.mInstrument = new Instrument();
         mMusic.mTracks.push_back(track);
      }
      ImGui::SameLine();
      if(ImGui::SmallButton("remove last")) {
         if(mMusic.mTracks.size() > 0) {
            delete mMusic.mTracks[mMusic.mTracks.size()-1].mInstrument;
            mMusic.mTracks.erase(mMusic.mTracks.end()-1);
            if(currentEditPattern >= mMusic.mTracks.size()) {
               --currentEditPattern;
            }
         }
      }

   }

   const int cLengthOneSequence = 128;

   const int maxOctave = 7;
   const int whiteBarLength = 30;
   const int whiteBarHeight = 16;
   const int blackBarLength = 20;
   const int blackBarHeight = 8;
   const int octaveOffset = 7 * whiteBarHeight;
   const int defaultNoteLength = 10;

   ImDrawList *drawList = ImGui::GetWindowDrawList();
   ImVec2 canvasPos = ImGui::GetCursorScreenPos();
   ImVec2 canvasSize = ImGui::GetContentRegionAvail();

   ImGui::BeginChild("test", canvasSize, false, ImGuiWindowFlags_HorizontalScrollbar);  // begin a scrolling region. size==0.0f: use remaining window size, size<0.0f: use remaining window size minus abs(size). size>0.0f: fixed size. each axis can use a different mode, e.g. ImVec2(0,400).

   canvasSize.x = defaultNoteLength*cLengthOneSequence + 100;   //scrollbar
   canvasSize.y = octaveOffset * maxOctave;
   ImGui::InvisibleButton("canvas", canvasSize);   //needed to set scrollbars and be able to get values from scrollbars!

   float scrollX = ImGui::GetScrollX();            // get scrolling amount [0..GetScrollMaxX()]
   float scrollY = ImGui::GetScrollY();            // get scrolling amount [0..GetScrollMaxY()]
   float scrollMaxX = ImGui::GetScrollMaxX();      // get maximum scrolling amount ~~ ContentSize.X - WindowSize.X
   float scrollMaxY = ImGui::GetScrollMaxY();      // get maximum scrolling amount ~~ ContentSize.Y - WindowSize.Y

   //set values to be able to use convert routines between logical and screen coordinates:
   canvasPosX = canvasPos.x - scrollX;
   canvasPosY = canvasPos.y - scrollY;

   if(mCurrentEditTrack >= 0 && mCurrentEditTrack < mMusic.mTracks.size()) {

      Track &track = mMusic.mTracks[mCurrentEditTrack];

      //   drawList->AddRect(canvasPos, ImVec2(canvasPos.x + canvasSize.x, canvasPos.y + canvasSize.y), ImColor(255, 255, 255));
      {
         const int noteAreaPosX = whiteBarLength;
         const int noteAreaPosY = 0;

         //draw editable area
         drawList->AddRectFilled(logicalToScreen(noteAreaPosX, noteAreaPosY), logicalToScreen(noteAreaPosX + cLengthOneSequence * defaultNoteLength, noteAreaPosY + canvasSize.y), ImColor(70, 70, 70));

         const int maxValue = cLengthOneSequence * defaultNoteLength;

         for (int i = 0; i < track.mMidiNotes.size(); ++i) {
            Track::MidiNote &evt = track.mMidiNotes[i];
            if(evt.mType == Track::MidiType::eNoteOn) {
//ARGH!!! NOT RIGHT!
               int posY = evt.mNote & 0xffff;               //note
               int oct = (evt.mNote >> 16) & 0xffff;        //octave

               if (posY >= 5) {
                  posY += 1;
               }

               int octDrawY = (maxOctave - oct) * octaveOffset;

               posY += 1;

               const int octaveHeight = whiteBarHeight * 7;

               int drawPosX = maxValue * evt.mTime / (float)(480*cLengthOneSequence);

               drawList->AddRectFilled(logicalToScreen(noteAreaPosX + drawPosX, noteAreaPosY + octDrawY + octaveHeight - posY * whiteBarHeight / 2 + whiteBarHeight / 4),
                                       logicalToScreen(noteAreaPosX + drawPosX+defaultNoteLength, noteAreaPosY + octDrawY + octaveHeight - (posY + 1)*whiteBarHeight / 2 + whiteBarHeight / 4), ImColor(160, 160, 0));
            }
         }
      }

      //draw piano keys on the side
      int octDrawOffsetY = 0;
      for (int oct = maxOctave; oct > 0; --oct) {
         //draw one octave
         //draw bigger white bars
         for (int i = 0; i < 7; ++i) {
            drawList->AddRectFilled(logicalToScreen(0, octDrawOffsetY + i * whiteBarHeight + 1), logicalToScreen(whiteBarLength, octDrawOffsetY + (i + 1)*whiteBarHeight - 1), ImColor(255, 255, 255));
         }

         //draw octave text
         char tmp[10];
         sprintf(tmp, "#%i", oct);
         drawList->AddText(logicalToScreen(0, octDrawOffsetY), ImColor(0, 0, 0), tmp);

         //draw smaller black bars
         drawList->AddRectFilled(logicalToScreen(0, octDrawOffsetY + 1 * whiteBarHeight - blackBarHeight / 2), logicalToScreen(blackBarLength, octDrawOffsetY + 1 * whiteBarHeight + blackBarHeight / 2), ImColor(0, 0, 0));
         drawList->AddRectFilled(logicalToScreen(0, octDrawOffsetY + 2 * whiteBarHeight - blackBarHeight / 2), logicalToScreen(blackBarLength, octDrawOffsetY + 2 * whiteBarHeight + blackBarHeight / 2), ImColor(0, 0, 0));
         drawList->AddRectFilled(logicalToScreen(0, octDrawOffsetY + 3 * whiteBarHeight - blackBarHeight / 2), logicalToScreen(blackBarLength, octDrawOffsetY + 3 * whiteBarHeight + blackBarHeight / 2), ImColor(0, 0, 0));
         //no black bar
         drawList->AddRectFilled(logicalToScreen(0, octDrawOffsetY + 5 * whiteBarHeight - blackBarHeight / 2), logicalToScreen(blackBarLength, octDrawOffsetY + 5 * whiteBarHeight + blackBarHeight / 2), ImColor(0, 0, 0));
         drawList->AddRectFilled(logicalToScreen(0, octDrawOffsetY + 6 * whiteBarHeight - blackBarHeight / 2), logicalToScreen(blackBarLength, octDrawOffsetY + 6 * whiteBarHeight + blackBarHeight / 2), ImColor(0, 0, 0));
         octDrawOffsetY += whiteBarHeight * 7;
      }

      {
         //draw helper line for mouse
         int mouseX = ImGui::GetMousePos().x;  //needs screenToLogical?
         int mouseY = ImGui::GetMousePos().y;
         if (mouseX >= canvasPos.x && mouseY >= canvasPos.y && mouseX < canvasPos.x + canvasSize.x && mouseY < canvasPos.y + canvasSize.y) {
            int drawY = mouseY - canvasPos.y;
            drawY /= whiteBarHeight / 2;
            drawY *= whiteBarHeight / 2;
            drawList->AddLine(logicalToScreen(0, drawY), logicalToScreen(canvasSize.x, drawY), ImColor(255, 255, 0));
         }
      }

      {
         //Draw current playing position
         const int noteAreaPosX = whiteBarLength;
         const int maxValue = cLengthOneSequence * defaultNoteLength;
         int drawPosX = maxValue * absoluteMusicTimeMs / (float)(480*cLengthOneSequence);

         drawList->AddLine(logicalToScreen(noteAreaPosX+drawPosX, 0), 
                           logicalToScreen(noteAreaPosX+drawPosX, canvasSize.y), ImColor(255,0,255));
      }

      {
         //Handle mouse clicks
         ImVec2 mousePos(ImGui::GetMousePos().x, ImGui::GetMousePos().y);
         ImVec2 logMousePos = screenToLogical(mousePos);

         ImGuiWindow* window = ImGui::GetCurrentWindow();
         float titleHeight = window->TitleBarHeight();

         ImVec2 windowPos = ImGui::GetWindowPos();

         if((mousePos.y >= windowPos.y + titleHeight) && (ImGui::IsWindowFocused())) {

            //clicked on keyboard keys?
            if (logMousePos.x < whiteBarLength) {
               if (ImGui::IsMouseClicked(0)) {
                  //TODO: play test note



               }
            } else {
               logMousePos.x -= whiteBarLength;
               if (logMousePos.x < cLengthOneSequence * defaultNoteLength) {
                  int noteIndex = logMousePos.x / defaultNoteLength;
                  int posY = logMousePos.y - whiteBarHeight / 2;

                  posY += 1;      //prevent -1

                  int oct = maxOctave - (int)(logMousePos.y / octaveOffset);
                  int note = posY - (maxOctave-oct)*octaveOffset;
                  note /= (whiteBarHeight/2);
                  note = 12 - note;

                  if(note >= 5) {
                     // ignore this note, it's not a valid one...
                     note -= 1;
                  }

                  if (ImGui::IsMouseReleased(0)) {
                     //create new note and insert it into the list
//                     addMidiNoteOnEvent(noteIndex*480, note | (oct<<16), 0);        //TODO: Channel
//                     sortMidiEvents();
                  }
                  if (ImGui::IsMouseReleased(1)) {                               // 1 => right mouse button
                     // search for exactly this note and delete it
//                     int searchNote = note | (oct<<16);
//                     for(int i=0; i<track.mEvents.size(); ++i) {
//                        if(track.mEvents[i].msTime == noteIndex*480 &&          //Time stimmt nicht?!
//                                    track.mEvents[i].type == Track::eNoteOn &&
//                                    track.mEvents[i].parameter.NoteOn.note == searchNote) {
//                           track.mEvents.erase(track.mEvents.begin()+i);
//                           break;
//                        }
//                     }
                  }
               }
            }
         }
       }
   }
   ImGui::EndChild();

   ImGui::End();   
}

typedef struct {
   std::vector<uint8_t> patterns;   //first byte is always instrument, second byte first pattern, ...
} Pattern;

void Player::doPatternImgui() {
   ImGui::Begin("PatternList");

   //instrument is per patternline, so first select the instrument and then the patterns...

   ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(0,0));
   ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0,0));

   // float glyph_width = ImGui::CalcTextSize("F").x;
   // float cell_width = glyph_width * 3; // "FF " we include trailing space in the width to easily catch clicks everywhere

   ImGui::PushItemWidth(20);

   struct FuncHolder {
       static int Callback(ImGuiTextEditCallbackData* data) {
           int* p_cursor_pos = (int*)data->UserData;
           if (!data->HasSelection())
               *p_cursor_pos = data->CursorPos;
           return 0;
       }
       static void handleDataInput(int *dataAdr) {
         static bool dataEditTakeFocus = false;
         static void* dataEditAdr = nullptr;

         if(dataEditAdr == dataAdr) {
            static char instrStr[3] = "00";

            if(dataEditTakeFocus) {
               ImGui::SetKeyboardFocusHere();
               sprintf(instrStr, "%02d", *dataAdr);
            }
            dataEditTakeFocus = false;

            int cursorPos = -1;
            bool dataWrite = false;
            if(ImGui::InputText("##data", instrStr, sizeof(instrStr), ImGuiInputTextFlags_CharsDecimal, FuncHolder::Callback, &cursorPos)) {
               dataWrite = true;
            }
            if(cursorPos >= 2) {
               dataWrite = true;
            }
            if(dataWrite) {
               int data;
               if(sscanf(instrStr, "%d", &data) == 1) {
                  *dataAdr = data;
                  sprintf(instrStr, "%02d", data);
               }
            }
         } else {
            ImGui::Text("%02d ", *dataAdr);
            if(ImGui::IsItemHovered() && ImGui::IsMouseClicked(0)) {
               dataEditTakeFocus = true;
               dataEditAdr = dataAdr;
            }            
         }
       }
   };

   //first line with command buttons:
   ImGui::Text("PatternLines: ");
   ImGui::SameLine();
   if(ImGui::Button(" + ##1")) {
      Music::PatternListElement pat;
      mMusic.mPatternLists.push_back(pat);
      if(mMusic.mPatternLists.size() > 1) {
         mMusic.setPatternLength(mMusic.mPatternLists[0].patternlist.size());     //resize newly created pattern
      } else {
         //no patterns so far in the vector, create a new one with the default size of 10
         mMusic.setPatternLength(10);     //resize newly created pattern
      }
   }
   ImGui::SameLine();
   ImGui::Text(" ");
   ImGui::SameLine();
   if(ImGui::Button(" - ##1")) {
      if(mMusic.mPatternLists.size() != 0) {
         mMusic.mPatternLists.erase(mMusic.mPatternLists.end()-1);
      }
   }

   ImGui::SameLine();
   ImGui::Text("     Size: ");
   ImGui::SameLine();
   if(ImGui::Button(" + ##2")) {
      if(mMusic.mPatternLists.size() != 0) {
         mMusic.setPatternLength(mMusic.mPatternLists[0].patternlist.size()+1);
      }
   }
   ImGui::SameLine();
   ImGui::Text(" ");
   ImGui::SameLine();
   if(ImGui::Button(" - ##2")) {
      if(mMusic.mPatternLists.size() != 0) {
         if(mMusic.mPatternLists[0].patternlist.size() > 0) {
            mMusic.setPatternLength(mMusic.mPatternLists[0].patternlist.size()-1);
         }
      }
   }

   for(int i=0; i<mMusic.mPatternLists.size(); ++i) {
      ImGui::PushID(i);
      FuncHolder::handleDataInput(&mMusic.mPatternLists[i].instrumentNumber);

      for(int j=0; j<mMusic.mPatternLists[i].patternlist.size(); ++j) {
         ImGui::PushID(j*3+0);
         ImGui::SameLine();
         FuncHolder::handleDataInput(&mMusic.mPatternLists[i].patternlist[j].patternNumber);
         ImGui::PopID();
         ImGui::PushID(j*3+1);
         ImGui::SameLine();
         FuncHolder::handleDataInput(&mMusic.mPatternLists[i].patternlist[j].volumeTranspose);
         ImGui::PopID();
         ImGui::PushID(j*3+2);
         ImGui::SameLine();
         FuncHolder::handleDataInput(&mMusic.mPatternLists[i].patternlist[j].noteTranspose);
         ImGui::PopID();
         ImGui::SameLine();
         ImGui::Text(" ");
      }
      ImGui::PopID();
   }

   ImGui::PopItemWidth();

   ImGui::PopStyleVar(2);

   ImGui::End();
}

#include "extlibs/nativefiledialog/nfd.h"

void Player::doMainControlImgui() {
   ImGui::Begin("Main Controls");

   ImGui::SliderFloat("MasterVolume", &mMasterVolume, 0.0f, 1.0f, "%.3f", /*power*/2.0f);

   ImGui::RadioButton("Stop", &playState, eStopped);
   ImGui::RadioButton("Play", &playState, ePlay);
   ImGui::RadioButton("Play current track", &playState, ePlayTrack);
   ImGui::RadioButton("Play current pattern", &playState, ePlayPattern);
   if(ImGui::Button("Reset play position")) {
      absoluteMusicTimeMs = 0;
   }
   ImGui::Separator();
   if(mMusic.mTracks.size() != 0) {
      ImGui::SliderInt("Track",&mCurrentEditTrack,0,mMusic.mTracks.size()-1);
   }
   if(ImGui::Button("Add Track")) {

   }
   if(ImGui::Button("Delete Track")) {

   }

   if(mCurrentEditTrack >= 0 && mCurrentEditTrack < mMusic.mTracks.size()) {
      if (ImGui::Button("Load Instrument")) {
         nfdchar_t *outPath = nullptr;
         nfdresult_t result = NFD_OpenDialog(nullptr, nullptr, &outPath);
         if(result == NFD_OKAY) {
            mMusic.mTracks[mCurrentEditTrack].mInstrument->loadInstrument(outPath);
            free(outPath);
         } else if(result == NFD_CANCEL) {

         } else {
            //NFD_GetError()
         }
      }
      if (ImGui::Button("Save Instrument")) {
         nfdchar_t *outPath = nullptr;
         nfdresult_t result = NFD_SaveDialog(nullptr, nullptr, &outPath);
         if(result == NFD_OKAY) {
            mMusic.mTracks[mCurrentEditTrack].mInstrument->saveInstrument(outPath);
            free(outPath);
         } else if(result == NFD_CANCEL) {

         } else {
            //NFD_GetError()
         }
      }
      if(ImGui::Button("Import Sysex")) {
         nfdchar_t *outPath = nullptr;
         nfdresult_t result = NFD_OpenDialog(nullptr, nullptr, &outPath);
         if(result == NFD_OKAY) {
            mMusic.mTracks[mCurrentEditTrack].mInstrument->importSysex(outPath);
            free(outPath);
         } else if(result == NFD_CANCEL) {

         } else {
            //NFD_GetError()
         }
      }
   }
   ImGui::End();
}

void Player::doImgui() {
   doPianoRollImgui();
   doPatternImgui();
   doMainControlImgui();

   if(mCurrentEditTrack >= 0 && mCurrentEditTrack < mMusic.mTracks.size()) {
      Instrument *instr = mMusic.mTracks[mCurrentEditTrack].mInstrument;
      if(instr)
         instr->doImgui();
   }
}



// w473rh34d
// And let's have some chilled macros to compose,
// using NOTE_C = pure note C,
// using NOTE_CS = note C sharp (c#)
// (no b's yet defined - tracker users don't know b's ^^)
#define NOTE_TO_INTEGER(octave, note) ((octave << 16) | note)
#define  NOTE_C(octave) NOTE_TO_INTEGER(octave,  0)
#define NOTE_CS(octave) NOTE_TO_INTEGER(octave,  1)
#define  NOTE_D(octave) NOTE_TO_INTEGER(octave,  2)
#define NOTE_DS(octave) NOTE_TO_INTEGER(octave,  3)
#define  NOTE_E(octave) NOTE_TO_INTEGER(octave,  4)
#define  NOTE_F(octave) NOTE_TO_INTEGER(octave,  5)
#define NOTE_FS(octave) NOTE_TO_INTEGER(octave,  6)
#define  NOTE_G(octave) NOTE_TO_INTEGER(octave,  7)
#define NOTE_GS(octave) NOTE_TO_INTEGER(octave,  8)
#define  NOTE_A(octave) NOTE_TO_INTEGER(octave,  9) 
#define NOTE_AS(octave) NOTE_TO_INTEGER(octave, 10)
#define  NOTE_B(octave) NOTE_TO_INTEGER(octave, 11)

// 1000 / 44100 * (44100*60/125)
#define LENGTH_ONE_NOTE 120

void Player::setWaterheadTestSong() {

#if 0
   // Bassline
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*0, NOTE_C(1), 1);
   addMidiNoteOffEvent(LENGTH_ONE_NOTE*1, 1);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*32, NOTE_DS(1), 1);
   addMidiNoteOffEvent(LENGTH_ONE_NOTE*33, 1);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*64, NOTE_F(1), 1);
   addMidiNoteOffEvent(LENGTH_ONE_NOTE*65, 1);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*96, NOTE_DS(1), 1);
   addMidiNoteOffEvent(LENGTH_ONE_NOTE*97, 1);
#endif

#if 1
   for(int i=0; i<128; ++i) {
//      addMidiNoteOnEvent(LENGTH_ONE_NOTE*i, NOTE_A(4),2);
   }
#else
   //Melody
   addMidiNoteOnEvent(LENGTH_ONE_NOTE* 0, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE* 1, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE* 2, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE* 3, NOTE_G(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE* 4, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE* 5, NOTE_G(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE* 6, NOTE_C(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE* 7, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE* 8, NOTE_C(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE* 9, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*10, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*11, NOTE_C(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*12, NOTE_G(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*13, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*14, NOTE_C(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*15, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*16, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*17, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*18, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*19, NOTE_G(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*20, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*21, NOTE_G(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*22, NOTE_C(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*23, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*24, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*25, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*26, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*27, NOTE_C(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*28, NOTE_G(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*29, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*30, NOTE_C(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*31, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*32, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*33, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*34, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*35, NOTE_G(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*36, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*37, NOTE_G(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*38, NOTE_C(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*39, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*40, NOTE_C(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*41, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*42, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*43, NOTE_C(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*44, NOTE_G(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*45, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*46, NOTE_C(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*47, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*48, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*49, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*50, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*51, NOTE_G(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*52, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*53, NOTE_G(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*54, NOTE_C(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*55, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*56, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*57, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*58, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*59, NOTE_C(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*60, NOTE_G(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*61, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*62, NOTE_C(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*63, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*64, NOTE_AS(6),2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*65, NOTE_AS(6),2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*66, NOTE_AS(6),2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*67, NOTE_F(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*68, NOTE_AS(6),2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*69, NOTE_F(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*70, NOTE_C(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*71, NOTE_AS(6),2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*72, NOTE_C(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*73, NOTE_AS(6),2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*74, NOTE_AS(6),2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*75, NOTE_C(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*76, NOTE_F(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*77, NOTE_AS(6),2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*78, NOTE_C(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*79, NOTE_AS(6),2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*80, NOTE_A(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*81, NOTE_A(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*82, NOTE_A(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*83, NOTE_F(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*84, NOTE_A(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*85, NOTE_F(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*86, NOTE_C(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*87, NOTE_A(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*88, NOTE_A(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*89, NOTE_A(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*90, NOTE_A(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*91, NOTE_C(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*92, NOTE_F(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*93, NOTE_A(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*94, NOTE_C(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*95, NOTE_A(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*96, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*97, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*98, NOTE_C(7), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*99, NOTE_G(6), 2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*100, NOTE_C(7),2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*101, NOTE_G(6),2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*102, NOTE_C(6),2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*103, NOTE_C(7),2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*104, NOTE_C(6),2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*105, NOTE_C(7),2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*106, NOTE_C(7),2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*107, NOTE_C(6),2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*108, NOTE_G(6),2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*109, NOTE_C(7),2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*110, NOTE_C(6),2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*111, NOTE_C(7),2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*112, NOTE_C(7),2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*113, NOTE_C(7),2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*114, NOTE_C(7),2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*115, NOTE_G(6),2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*116, NOTE_C(7),2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*117, NOTE_G(6),2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*118, NOTE_C(6),2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*119, NOTE_C(7),2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*120, NOTE_C(7),2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*121, NOTE_C(7),2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*122, NOTE_C(7),2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*123, NOTE_C(6),2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*124, NOTE_G(6),2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*125, NOTE_C(7),2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*126, NOTE_C(6),2);
   addMidiNoteOnEvent(LENGTH_ONE_NOTE*127, NOTE_C(7),2);
   addMidiNoteOffEvent(LENGTH_ONE_NOTE*128, 2);
#endif
}


