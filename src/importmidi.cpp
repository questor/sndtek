
#include "importmidi.h"

#define TML_IMPLEMENTATION
#include "src/extlibs/tinysoundfont/tml.h"

void loadmidi(char *filename, Music &music) {
	music.clearAll();
  tml_message* tinyMidiLoader = tml_load_filename(filename);
  if (!tinyMidiLoader) {
      printf("Could not load MIDI file\n");
      return;
  }
  typedef struct {
      uint8_t note;
      uint8_t channel;
      uint16_t originalId;
      uint32_t startTime;
      uint32_t indexToWriteLengthTo;
  } CalcLengthTemp;
  std::vector<CalcLengthTemp> currentNotesPlaying;
  
  tml_message *midiMsg = tinyMidiLoader;

  uint16_t counter = 0;

  Track track;
  track.mInstrument = new Instrument();
  while(midiMsg) {
  	switch(midiMsg->type) {
  	case TML_NOTE_ON: {
  			if(midiMsg->velocity == 0) {
  				goto handleNoteOff;
  			} else {
  				Track::MidiNote note;
          note.type(Track::MidiType::eNoteOn)
              .time(midiMsg->time)
              .channel(midiMsg->channel)
              .note(midiMsg->key)
              .id(counter++)
              .velocity(midiMsg->velocity);
  				track.mMidiNotes.push_back(note);

          CalcLengthTemp lengthTemp;
          lengthTemp.note = midiMsg->key;
          lengthTemp.channel = midiMsg->channel;
          lengthTemp.startTime = midiMsg->time;
          lengthTemp.indexToWriteLengthTo = track.mMidiNotes.size()-1;
          lengthTemp.originalId = note.mId;
          currentNotesPlaying.push_back(lengthTemp);                        
  			}
	  	}
  		break;
  	case TML_NOTE_OFF: {
handleNoteOff:
        int index=0;
        int sizePlayingNotes = currentNotesPlaying.size();
        while( (index < sizePlayingNotes) && (currentNotesPlaying[index].note != midiMsg->key) &&
            (currentNotesPlaying[index].channel != midiMsg->channel)) {
            ++index;
        }
        uint32_t writeIndex = currentNotesPlaying[index].indexToWriteLengthTo;
        track.mMidiNotes[writeIndex].mNoteLength = midiMsg->time - currentNotesPlaying[index].startTime;
        track.mMidiNotes[writeIndex].mId = currentNotesPlaying[index].originalId;
        currentNotesPlaying.erase(currentNotesPlaying.begin() + index);

        Track::MidiNote note;
        note.type(Track::MidiType::eNoteOff).time(midiMsg->time).channel(midiMsg->channel).note(midiMsg->key).id(currentNotesPlaying[index].originalId);
        track.mMidiNotes.push_back(note);
  		}
  		break;
		default:
			break;
  	}

  	midiMsg = midiMsg->next;
  }

  //now end all still running notes
  uint32_t lastTime = track.mMidiNotes[track.mMidiNotes.size()-1].mTime;
  for(int i=0; i<currentNotesPlaying.size(); ++i) {
      uint32_t writeIndex = currentNotesPlaying[i].indexToWriteLengthTo;
      track.mMidiNotes[writeIndex].mNoteLength = lastTime - currentNotesPlaying[i].startTime;
  }
  //TODO: here no note-off events are generated?

  music.mTracks.push_back(track);
}