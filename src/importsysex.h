
#ifndef __IMPORTSYSEX_H__
#define __IMPORTSYSEX_H__

#include "instrument.h"
#include "nodegrapheditor.h"
#include "nodes/dx7_env.h"
#include "nodes/dx7_osc.h"
#include "nodes/dx7_lfo.h"
#include "nodes/operator.h"
#include "nodes/noteinput.h"
#include "nodes/output.h"

class ImportSysex {
public:
   void importFromFile(const char *filename, int patchNumber, Instrument &instrument) {
      FILE *fp = fopen(filename, "rb");
      if(fp == 0) {
         return;
      }

      fseek(fp, 0, SEEK_END);
      int fileLen = ftell(fp);
      fseek(fp, 0, SEEK_SET);

      uint8_t *mem = new uint8_t[fileLen+16];
      int bytesRead = fread(mem, 1, fileLen, fp);
      fclose(fp);

      uint8_t *patch = mem + 6 + 128*patchNumber;
      importFromMemblock(patch, instrument);

      delete[] mem;
   }

   void importFromMemblock(uint8_t *memBlock, Instrument &instrument) {             //has to be in bulk format (128bytes)
      ImGui::NodeGraphEditor *nodeGraphEditor = instrument.mNodeGraphEditor;

      ImGui::Dx7EnvNode* dx7Env[6];
      ImGui::Dx7OscNode* dx7Osc[6];
      ImGui::Dx7LfoNode* dx7Lfo;
      ImGui::Dx7EnvNode* dx7LfoEnv;

      for(int i=0; i<=5; ++i) {
         int oscStart = i*17;

         dx7Env[i] = (ImGui::Dx7EnvNode*)nodeGraphEditor->addNode(ImGui::SNT_DX7ENV);
         dx7Osc[i] = (ImGui::Dx7OscNode*)nodeGraphEditor->addNode(ImGui::SNT_DX7OSC);
         ImGui::Dx7EnvNode* env = dx7Env[i];
         ImGui::Dx7OscNode* osc = dx7Osc[i];

         env->setRate(0, *(memBlock+oscStart+0));
         env->setRate(1, *(memBlock+oscStart+1));
         env->setRate(2, *(memBlock+oscStart+2));
         env->setRate(3, *(memBlock+oscStart+3));
         env->setLevel(0, *(memBlock+oscStart+4));
         env->setLevel(1, *(memBlock+oscStart+5));
         env->setLevel(2, *(memBlock+oscStart+6));
         env->setLevel(3, *(memBlock+oscStart+7));
         // 8 : keyScaleBreakpoint
         // 9 : keyScaleDepthL
         //10 : keyScaleDepthR
         //11 : keyScaleCurveL & 3
         //11 : keyScaleDepthR >> 2
         //12 : keyScaleRate & 7
         osc->setDetune(((*(memBlock+oscStart+12))>>3)-7);
//TODO!         osc->lfoAmpModSens = (*(memBlock+oscStart+13)) & 3;
         osc->setVelocitySens((*(memBlock+oscStart+13)) >> 2);
         osc->setOutputLevel(*(memBlock+oscStart+14));
         osc->setOscMode((*(memBlock+oscStart+15)) & 1);
         osc->setFreqCoarse((*(memBlock+oscStart+15)) >> 1);
         osc->setFreqFine(*(memBlock+oscStart+16));
         //panning
      }

      int algorithm = *(memBlock+110);
      int feedback = (*(memBlock+111)) & 7;

      dx7Lfo = (ImGui::Dx7LfoNode*)nodeGraphEditor->addNode(ImGui::SNT_DX7LFO);
      dx7Lfo->lfoSpeed = *(memBlock+112);
      dx7Lfo->lfoDelay = *(memBlock+113);
      dx7Lfo->lfoPitchModDepth = *(memBlock+114);
      dx7Lfo->lfoAmpModDepth = *(memBlock+115);
      dx7Lfo->lfoPitchModSens = (*(memBlock+116))>>4;
      dx7Lfo->mWaveform = ((*(memBlock+116)) >> 1 ) & 7;

      dx7Lfo->lfoAmpModSens = 0;      //FUUUUUuuuuuuuucck!

      dx7LfoEnv = (ImGui::Dx7EnvNode*)nodeGraphEditor->addNode(ImGui::SNT_DX7ENV);
      dx7LfoEnv->setRate(0, *(memBlock+102));
      dx7LfoEnv->setRate(1, *(memBlock+103));
      dx7LfoEnv->setRate(2, *(memBlock+104));
      dx7LfoEnv->setRate(3, *(memBlock+105));
      dx7LfoEnv->setLevel(0, *(memBlock+106));
      dx7LfoEnv->setLevel(1, *(memBlock+107));
      dx7LfoEnv->setLevel(2, *(memBlock+108));
      dx7LfoEnv->setLevel(3, *(memBlock+109));

      ImGui::NoteInputNode *input = (ImGui::NoteInputNode*)nodeGraphEditor->addNode(ImGui::SNT_NOTE_INPUT);
      ImGui::OutputNode *output = (ImGui::OutputNode*)nodeGraphEditor->addNode(ImGui::SNT_OUTPUT);

      switch(algorithm) {
      case 2: {
        ImGui::OperatorNode *adder = (ImGui::OperatorNode*)nodeGraphEditor->addNode(ImGui::SNT_OPERATOR);
        adder->setMode(0);         //+

        dx7Osc[0]->setSelfModulation(true);
        dx7Osc[0]->setFeedbackRatio(feedback);

        nodeGraphEditor->addLink(dx7Osc[0], 0, dx7Env[0], 0);
        nodeGraphEditor->addLink(dx7Env[0], 0, dx7Osc[1], 1);

        nodeGraphEditor->addLink(dx7Osc[1], 0, dx7Env[1], 0);
        nodeGraphEditor->addLink(dx7Env[1], 0, dx7Osc[2], 1);

        nodeGraphEditor->addLink(dx7Osc[2], 0, dx7Env[2], 0);
        nodeGraphEditor->addLink(dx7Env[2], 0, adder, 0);

        nodeGraphEditor->addLink(dx7Osc[3], 0, dx7Env[3], 0);
        nodeGraphEditor->addLink(dx7Env[3], 0, dx7Osc[4], 1);

        nodeGraphEditor->addLink(dx7Osc[4], 0, dx7Env[4], 0);
        nodeGraphEditor->addLink(dx7Env[4], 0, dx7Osc[5], 1);

        nodeGraphEditor->addLink(dx7Osc[5], 0, dx7Env[5], 0);
        nodeGraphEditor->addLink(dx7Env[5], 0, adder, 1);

        nodeGraphEditor->addLink(dx7Lfo, 1, dx7LfoEnv, 0);

        nodeGraphEditor->addLink(dx7Lfo, 0, dx7Osc[0], 2);      //pitch
        nodeGraphEditor->addLink(dx7LfoEnv, 0, dx7Osc[0], 3);      //amp
        nodeGraphEditor->addLink(dx7Lfo, 0, dx7Osc[1], 2);      //pitch
        nodeGraphEditor->addLink(dx7LfoEnv, 0, dx7Osc[1], 3);      //amp
        nodeGraphEditor->addLink(dx7Lfo, 0, dx7Osc[2], 2);      //pitch
        nodeGraphEditor->addLink(dx7LfoEnv, 0, dx7Osc[2], 3);      //amp
        nodeGraphEditor->addLink(dx7Lfo, 0, dx7Osc[3], 2);      //pitch
        nodeGraphEditor->addLink(dx7LfoEnv, 0, dx7Osc[3], 3);      //amp
        nodeGraphEditor->addLink(dx7Lfo, 0, dx7Osc[4], 2);      //pitch
        nodeGraphEditor->addLink(dx7LfoEnv, 0, dx7Osc[4], 3);      //amp
        nodeGraphEditor->addLink(dx7Lfo, 0, dx7Osc[5], 2);      //pitch
        nodeGraphEditor->addLink(dx7LfoEnv, 0, dx7Osc[5], 3);      //amp

        nodeGraphEditor->addLink(input, 0, dx7Osc[0], 0);
        nodeGraphEditor->addLink(input, 0, dx7Osc[1], 0);
        nodeGraphEditor->addLink(input, 0, dx7Osc[2], 0);
        nodeGraphEditor->addLink(input, 0, dx7Osc[3], 0);
        nodeGraphEditor->addLink(input, 0, dx7Osc[4], 0);
        nodeGraphEditor->addLink(input, 0, dx7Osc[5], 0);

        nodeGraphEditor->addLink(adder, 0, output, 0);
        nodeGraphEditor->addLink(adder, 0, output, 1);

        } break;
      default:
        break;
      }

/*      TODO: 
        name: voiceData.substring(118, 128),

        lfoSync: voiceData.charCodeAt(116) & 1,
        controllerModVal: 0,
        aftertouchEnabled: 0 */     
   }

};

#endif
