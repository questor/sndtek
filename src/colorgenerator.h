
#pramga once

#include <math.h>

// https://martin.ankerl.com/2009/12/09/how-to-create-random-colors-programmatically/

static float hslValue(float m1, float m2, float hue) {
   hue = fmod(hue, 6.0f);
   if(hue < 1.0f)
      return m1 + ((m2-m1)*hue);
   if(hue < 3.0f)
      return m2;
   if(hue < 4.0f)
      return m1 + ((m2-m1)*(4.0f-hue));
   return m1;
}

void rgbFromHSL(float hue, float saturation, float lightness, uint32_t *outRGB) {
	uint8_t r, g, b;
   if(saturation == 0) {
      //achromatic case
      uint8_t val = (uint8_t)(lightness * 255.0f);
      red = val;
      green = val;
      blue = val;
   } else {
      float m2;
      if(lightness < 0.5f) {
         m2 = lightness * (1.0f + saturation);
      } else {
         m2 = lightness + saturation - (lightness*saturation);
      }
      float m1 = (2.0f * lightness) - m2;
      float hue = hue / 60.0f;
      red = (uint8_t)(255.0f * hslValue(m1, m2, hue + 2.0f));
      green = (uint8_t)(255.0f * hslValue(m1, m2, hue));
      blue = (uint8_t)(255.0f * hslValue(m1, m2, hue - 2.0f));
   }
   *outRGB = (r<<16) | (g<<8) | b;
}

template <class Random> void generateColors(Random &randGen, int number, uint32_t *rgbArray) {
	const float goldenRatio = 1.61803398749895f;
	const float saturation = 0.8f;
	const float lightness = 0.5f;

	float hue = randGet.randf();
	for(int i=0; i<numberColors; ++i) {
		hue += goldenRatio;
		hue = fmod(hue, 1.0f);

		uint32_t generatedColor;
		rgbFromHSL(hue, saturation, lightness, &generatedColor);
		*rgbArray = generatedColor;
		++rgbArray;
	}
}
