
#ifndef __NODEGRAPHPROCESSOR_H__
#define __NODEGRAPHPROCESSOR_H__

#include "nodegrapheditor.h"
#include "nodes/noteinput.h"

class NodeGraphProcessor {
public:

   void setEditor(ImGui::NodeGraphEditor *nge) {
      mNodeGraphEditor = nge;
   }

   void initNodes(int numberSamplesToRender, float frequency) {
      using namespace ImGui;
      int numberNodes = mNodeGraphEditor->getNumNodes();
      for(int i=0; i<numberNodes; ++i) {
         Node *node = mNodeGraphEditor->getNode(i);
         ProcessingNode *procNode = (ProcessingNode*)node;

         //resize work buffers and set initial values to track which buffers are ready
         for(int j=0; j<procNode->mNumberWorkBuffers; ++j) {
            if (procNode->mWorkBuffers[j].size() < numberSamplesToRender) {
               procNode->mWorkBuffers[j].resize(numberSamplesToRender);
            }
         }
         if(node->getType() == SNT_NOTE_INPUT) {
            NoteInputNode *inputNode = (NoteInputNode*)node;
            inputNode->setFrequency(frequency);
         }
      }
   }

   // update cached information about node links
   void updateCachedInfos() {
      using namespace ImGui;
      int numberNodes = mNodeGraphEditor->getNumNodes();
      for(int i=0; i<numberNodes; ++i) {
         Node *node = mNodeGraphEditor->getNode(i);
         ProcessingNode *procNode = (ProcessingNode*)node;

         procNode->mInputNodes.clear();
         procNode->mOutputNodes.clear();

         procNode->mNumberNotReadyInputSlots = 0;
         procNode->mNumberNotReadyOutputSlots = 0;

         static ImVector<Node*> nodes;
         static ImVector<int> slotOut;

         for(int j=0; j<node->getNumInputSlots(); ++j) {
            mNodeGraphEditor->getInputNodesForNodeAndSlot(node, j, nodes, &slotOut);

            if(nodes.size() == 1) {             //only allow 1 input link
               procNode->mNumberNotReadyInputSlots += 1;
               ProcessingNode::NodeWithSlot nodeWithSlot;
               nodeWithSlot.node = (ProcessingNode*)nodes[0];
               nodeWithSlot.slot = slotOut[0];
               procNode->mInputNodes.push_back(nodeWithSlot);
            } else {
               ProcessingNode::NodeWithSlot nodeWithSlot;
               nodeWithSlot.node = nullptr;
               procNode->mInputNodes.push_back(nodeWithSlot);
            }
         }

         for(int j=0; j<node->getNumOutputSlots(); ++j) {
            mNodeGraphEditor->getOutputNodesForNodeAndSlot(node, j, nodes, &slotOut);
            procNode->mNumberNotReadyOutputSlots += nodes.size();
            for(int k=0; k<nodes.size(); ++k) {
               ProcessingNode::NodeWithSlot nodeWithSlot;
               nodeWithSlot.node = (ProcessingNode*)nodes[k];
               nodeWithSlot.slot = slotOut[k];
               procNode->mOutputNodes.push_back(nodeWithSlot);
            }
         }
      }      
   }

   void noteOn() {
      using namespace ImGui;
      int numberNodes = mNodeGraphEditor->getNumNodes();
      for(int i=0; i<numberNodes; ++i) {
         Node *node = mNodeGraphEditor->getNode(i);
         ProcessingNode *procNode = (ProcessingNode*)node;
         procNode->noteOn();
      }
   }
   
   void noteOff() {
      using namespace ImGui;
      int numberNodes = mNodeGraphEditor->getNumNodes();
      for(int i=0; i<numberNodes; ++i) {
         Node *node = mNodeGraphEditor->getNode(i);
         ProcessingNode *procNode = (ProcessingNode*)node;
         procNode->noteOff();
      }
   }

   static const int cAlreadyProcessedThisFrameMarker = 100000;

   void processNodes(int numberSamples) {
      using namespace ImGui;
      int numberNodes = mNodeGraphEditor->getNumNodes();
      bool somethingTodo = true;
      while(somethingTodo) {
         somethingTodo = false;
         for(int i=0; i<numberNodes; ++i) {
            Node *node = mNodeGraphEditor->getNode(i);
            ProcessingNode *procNode = (ProcessingNode*)node;

            if(procNode->mNumberNotReadyInputSlots == 0) {

               //process node
               procNode->renderSamples(numberSamples);

               //no go to all output-nodes and mark one buffer as ready
               for(int k=0; k<procNode->mOutputNodes.size(); ++k) {
                  ProcessingNode *toDecrement = procNode->mOutputNodes[k].node;
                  toDecrement->mNumberNotReadyInputSlots -= 1;
                  somethingTodo = true;
               }
               procNode->mNumberNotReadyInputSlots = cAlreadyProcessedThisFrameMarker;   //marker to not process this node this frame again
            }
         }
      }
   }

protected:
   ImGui::NodeGraphEditor *mNodeGraphEditor;
};

#endif

