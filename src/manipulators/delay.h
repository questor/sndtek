
struct Delay {
    float feedback;    // (0-1]

    float *ringBuffer;          //stereo two buffers?
    int maxSizeOfRingBuffer;
    int sizeOfRingBuffer;
    int indexIntoRingBuffer;

    void setup(int maxSize) {
        ringBuffer = new float[maxSize];
        maxSizeOfRingBuffer = maxSize;
        sizeOfRingBuffer = maxSize;
        indexIntoRingBuffer = 0;
    }

    void setSize(int newSize) {
        if(newSize <= maxSizeOfRingBuffer) {
            sizeOfRingBuffer = newSize;
        }
    }
    void noteOn() {
        
    }

    void noteOff() {
        
    }

    void doImgui() {
        ImGui::DragFloat("Feedback", &feedback, 0.05f, 0.0f, 1.0f - 0.05f);
        ImGui::DragInt("Size", &sizeOfRingBuffer, 100, 0, maxSizeOfRingBuffer);
    }

    void manipulate(float *destBuffer, int numberSamples) {
        for(int i=0; i<numberSamples; ++i) {
            //Optimize for runs
            float value = *(destBuffer)+ringBuffer[indexIntoRingBuffer];  //TODO: how to handle stereo?
            ringBuffer[indexIntoRingBuffer] = value * feedback;
            indexIntoRingBuffer += 1;
            if(indexIntoRingBuffer >= sizeOfRingBuffer)
                indexIntoRingBuffer = 0;

            *(destBuffer++) = value;
        }
    }
};

