
#include "utils.h"

struct ADSR {
    int attackDuration;
    int decayDuration;    //in this duration the attackDuration is added already
    int sustainDuration;    //here the former are all added
    float sustainLevel;     //with which multiplier to keep
    int releaseDuration;    //same

    ADSR() {
        setup(4000, 2000, 10000, 0.6f, 2000);
    }

    void setup(int attack, int decay, int sustain, float _sustainLevel, int release) {
        attackDuration = attack;
        decayDuration = attackDuration + decay;
        sustainDuration = decayDuration + sustain;
        sustainLevel = _sustainLevel;
        releaseDuration = sustainDuration + release;
    }
    void doImgui() {
        int attack = attackDuration;
        int decay = decayDuration - attack;
        int sustain = sustainDuration - decay - attack;
        int release = releaseDuration - sustain - decay - attack;

        ImGui::DragInt("Attack", &attack, 10, 0, 1000000);
        ImGui::DragInt("Decay", &decay, 10, 0, 1000000);
        ImGui::DragInt("Sustain", &sustain, 10, 0, 1000000);
        ImGui::DragFloat("SustainLevel", &sustainLevel, 0.05f, 0.0f, 1.0f);
        ImGui::DragInt("Release", &release, 10, 0, 1000000);

        attackDuration = attack;
        decayDuration = attack + decay;
        sustainDuration = attack + decay + sustain;
        releaseDuration = attack + decay + sustain + release;

        //Draw ADSR
        {
         ImDrawList *drawList = ImGui::GetWindowDrawList();
         ImVec2 canvasPos = ImGui::GetCursorScreenPos();
         ImVec2 canvasSize = ImGui::GetContentRegionAvail();
         canvasSize.x = clamp(canvasSize.x, 50.0f, 300.0f);
         canvasSize.y = clamp(canvasSize.y, 50.0f, 100.0f);

         float scaleX = canvasSize.x / releaseDuration;
         drawList->AddRect(canvasPos, ImVec2(canvasPos.x + canvasSize.x, canvasPos.y + canvasSize.y), ImColor(255, 255, 255));
         ImGui::InvisibleButton("canvas", canvasSize);   //needed?
         drawList->PushClipRect(ImVec2(canvasPos.x, canvasPos.y), ImVec2(canvasPos.x + canvasSize.x, canvasPos.y + canvasSize.y), false);

         //Attack curve
         drawList->AddLine(ImVec2(canvasPos.x, canvasPos.y + canvasSize.y * 1.0f),
                           ImVec2(canvasPos.x + attack * scaleX, canvasPos.y + canvasSize.y * 0.0f), 0xFF00FFFF, 1.0f);
         //Decay curve
         drawList->AddLine(ImVec2(canvasPos.x + attack * scaleX, canvasPos.y + canvasSize.y * 0.0f),
                           ImVec2(canvasPos.x + decayDuration * scaleX, canvasPos.y + canvasSize.y * (1.0f - sustainLevel)), 0xFF00FFFF, 1.0f);
         //Sustain
         drawList->AddLine(ImVec2(canvasPos.x + decayDuration * scaleX, canvasPos.y + canvasSize.y * (1.0f - sustainLevel)),
                           ImVec2(canvasPos.x + sustainDuration * scaleX, canvasPos.y + canvasSize.y * (1.0f - sustainLevel)), 0xFF00FFFF, 1.0f);
         //release
         drawList->AddLine(ImVec2(canvasPos.x + sustainDuration * scaleX, canvasPos.y + canvasSize.y * (1.0f - sustainLevel)),
                           ImVec2(canvasPos.x + releaseDuration * scaleX, canvasPos.y + canvasSize.y * 1.0f), 0xFF00FFFF, 1.0f);

         drawList->PopClipRect();
        }        
    }

    //State:
    void noteOn() {
        posInADSR = 0;
    }
    void noteOff() {
        posInADSR = sustainDuration+1;
    }
    bool isFinished() {
        return posInADSR >= releaseDuration;
    }
    int posInADSR;

    void manipulate(float *destBuffer, int numberSamples) {
        int currentState = 0;
        if(posInADSR > attackDuration) {
            currentState = 1;
        }
        if(posInADSR > decayDuration) {
            currentState = 2;
        }
        if(posInADSR > sustainDuration) {
            currentState = 3;
        }
        if(posInADSR > releaseDuration) {
            //here we are over the releaseDuration, simply null the rest...
            memset(destBuffer, 0, sizeof(float)*2*numberSamples);
            return;
        }

        while(numberSamples != 0) {
            switch(currentState) {
            case 0:
                {
                    //attack
                    int genSamples = min(numberSamples, attackDuration-posInADSR);
                    for(int i=0; i<genSamples; ++i) {
                        float envelopeVolume = (float)posInADSR/(float)attackDuration;      //Optimization: div can be put outside of loop
                        *(destBuffer++) *= envelopeVolume;
                        posInADSR += 1;
                    }
                    numberSamples -= genSamples;
                    if(numberSamples != 0)
                        currentState += 1;
                }
                break;
            case 1:
                {
                    //decay
                    int samplesToDecay = decayDuration-attackDuration;
                    float stepDelta = (1.0f - sustainLevel)/samplesToDecay;       //Optimization: save this into struct
                    int genSamples = min(numberSamples, decayDuration - posInADSR);
                    float currentValue = sustainLevel + stepDelta * (decayDuration-posInADSR);
                    for(int i=0; i<genSamples; ++i) {
                        *(destBuffer++) *= currentValue;
                        currentValue -= stepDelta;
                    }
                    posInADSR += genSamples;
                    numberSamples -= genSamples;
                    if(numberSamples != 0)
                        currentState += 1;
                }
                break;
            case 2:
                {
                    //sustain
                    int genSamples = min(numberSamples, sustainDuration - posInADSR);
                    for(int i=0; i<genSamples; ++i) {
                        *(destBuffer++) *= sustainLevel;
                    }
                    posInADSR += genSamples;
                    numberSamples -= genSamples;
                    if(numberSamples != 0)
                        currentState += 1;
                }
                break;
            case 3:
                {
                    //release
                    int genSamples = min(numberSamples, releaseDuration - posInADSR);
                    int samplesToRelease = releaseDuration - sustainDuration;
                    float stepValue = sustainLevel / samplesToRelease;
                    float currentValue = sustainLevel - stepValue*(samplesToRelease-(releaseDuration - posInADSR));
                    for(int i=0; i<genSamples; ++i) {
                        *(destBuffer++) *= currentValue;
                        currentValue -= stepValue;
                    }
                    posInADSR += genSamples;
                    numberSamples -= genSamples;
                    if(numberSamples != 0)
                        currentState += 1;
                }
                break;
            default:
                {
                    //after release and else => set to zero
                    int genSamples = numberSamples;
                    memset(destBuffer, 0, sizeof(float)*numberSamples);
                    numberSamples -= numberSamples;
                }
                break;
            }        
        }
    }
};
