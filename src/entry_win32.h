
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include <imgui.h>
#include "imgui_impl_dx10.h"
#include <d3d10_1.h>
#include <d3d10.h>
#include <d3dcompiler.h>
#define DIRECTINPUT_VERSION 0x0800
#include <dinput.h>
#include <tchar.h> 

#include "entry.h"

class EntryWin32 : public Entry {
public:
   EntryWin32();

   void init();
   void deinit();

   bool doMainLoop();
   void imguiBeginFrame();
   void imguiEndFrame();

   bool isKeyPressed(KeyCode keycode);

   //================================================

   void CreateRenderTarget();
   void CleanupRenderTarget();
   HRESULT CreateDeviceD3D(HWND hWnd);
   void CleanupDeviceD3D();

   ID3D10Device*           g_pd3dDevice = NULL;
   IDXGISwapChain*         g_pSwapChain = NULL;
   ID3D10RenderTargetView* g_mainRenderTargetView = NULL;  

   WNDCLASSEX wc;
   HWND hwnd;
};


