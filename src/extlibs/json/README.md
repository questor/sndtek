# JSON/SJSON parser - Public Domain
In-place JSON/SJSON parser implemented in a single header file. No memory allocation, all operations are done in-place.

Created by Mattias Jansson / Rampant Pixels - <http://www.rampantpixels.com>

Please consider our Patreon - https://patreon.com/rampantpixels

This library is put in the public domain; you can redistribute it and/or modify it without any restrictions.
