﻿// created by i-saint
// distributed under Creative Commons Attribution (CC BY) license.
// https://github.com/i-saint/DynamicPatcher

#include "DynamicPatcher.h"
#include "dpInternal.h"

static dpContext *g_dpDefaultContext = nullptr;
static __declspec(thread) dpContext *g_dpCurrentContext = nullptr;

dpAPI dpContext* dpGetCurrentContext()
{
    if(!g_dpCurrentContext) { g_dpCurrentContext=g_dpDefaultContext; }
    return g_dpCurrentContext;
}


dpAPI bool dpInitialize(dpOnLoadHandler on_load_handler, dpOnUnloadHandler on_unload_handler, void* user_data)
{
    if(!g_dpDefaultContext) {
        DWORD opt = ::SymGetOptions();
        opt |= SYMOPT_DEFERRED_LOADS | SYMOPT_LOAD_LINES;
        opt &= ~SYMOPT_UNDNAME;
        ::SymSetOptions(opt);
        ::SymInitialize(::GetCurrentProcess(), NULL, TRUE);

        g_dpDefaultContext = new dpContext(on_load_handler, on_unload_handler, user_data);

        return true;
    }
    return false;
}

dpAPI bool dpFinalize()
{
    if(g_dpDefaultContext) {
        delete g_dpDefaultContext;
        g_dpDefaultContext = nullptr;
        return true;
    }
    return false;
}

dpAPI bool   dpReloadDll(const char *path)
{
	return dpGetCurrentContext()->getLoader()->loadDll(path)!=nullptr;
}


dpAPI void dpWatchDll(const char *path)
{
    dpGetCurrentContext()->getBuilder()->addModule(path);
}

dpAPI size_t dpUpdate()
{
    return dpGetCurrentContext()->getBuilder()->update();
}
