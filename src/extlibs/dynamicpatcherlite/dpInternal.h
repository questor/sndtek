﻿// created by i-saint
// distributed under Creative Commons Attribution (CC BY) license.
// https://github.com/i-saint/DynamicPatcher

#include <windows.h>
#include <dbghelp.h>
#include <vector>
#include <string>
#include <set>
#include <algorithm>
#include <time.h>
#include "DynamicPatcher.h"

typedef unsigned long long dpTime;
class dpDllFile;
class dpLoader;
class dpPatcher;
class dpBuilder;
class dpContext;

enum dpSymbolFlags {
    dpE_Code    = 0x1,  // code
    dpE_IData   = 0x2,  // initialized data
    dpE_UData   = 0x4,  // uninitialized data
    dpE_Read    = 0x8,  // readable
    dpE_Write   = 0x10, // writable
    dpE_Execute = 0x20, // executable
    dpE_Shared  = 0x40, // shared
    dpE_Export  = 0x80, // dllexport
    dpE_Handler = 0x100, // dpOnLoad, dpOnUnload, etc
};
#define dpIsFunction(flag) ((flag&dpE_Code)!=0)
#define dpIsExportFunction(flag) ((flag&(dpE_Code|dpE_Export|dpE_Handler))==(dpE_Code|dpE_Export))
enum dpSymbolFlagsEx {
    dpE_HostSymbol      = 0x10000,
    dpE_NameNeedsDelete = 0x20000,
};

struct dpSymbol
{
    const char *name;
    void *address;
    int flags;
    int section;
    dpDllFile *binary;

    dpSymbol(const char *nam, void *addr, int fla, int sect, dpDllFile *bin);
    ~dpSymbol();
};
inline bool operator< (const dpSymbol &a, const dpSymbol &b) { return strcmp(a.name, b.name)<0; }
inline bool operator==(const dpSymbol &a, const dpSymbol &b) { return strcmp(a.name, b.name)==0; }
inline bool operator< (const dpSymbol &a, const char *b) { return strcmp(a.name, b)<0; }
inline bool operator==(const dpSymbol &a, const char *b) { return strcmp(a.name, b)==0; }

template<class T>
struct dpLTPtr {
    bool operator()(const T *a, const T *b) const { return *a<*b; }
};

template<class T>
struct dpEQPtr {
    bool operator()(const T *a, const T *b) const { return *a==*b; }
};

struct dpPatchData
{
    const dpSymbol *target;
    const dpSymbol *hook;
    void *unpatched;
    void *trampoline;
    size_t unpatched_size;

    dpPatchData() : target(), hook(), unpatched(), trampoline(), unpatched_size() {}
};
inline bool operator< (const dpPatchData &a, const dpPatchData &b) { return a.target< b.target; }
inline bool operator==(const dpPatchData &a, const dpPatchData &b) { return a.target==b.target; }


template<class Container, class F> inline auto dpFind(Container &cont, const F &f) -> decltype(cont.begin());

void    dpPrintError(const char* fmt, ...);
void    dpPrintWarning(const char* fmt, ...);
void    dpPrintInfo(const char* fmt, ...);
void    dpPrintDetail(const char* fmt, ...);

void*   dpAllocateBackward(size_t size, void *location);
void    dpDeallocate(void *location, size_t size);
dpTime  dpGetMTime(const char *path);
dpTime  dpGetSystemTime();

bool    dpWriteFile(const char *path, const void *data, size_t size);
bool    dpDeleteFile(const char *path);
bool    dpFileExists(const char *path);
size_t  dpSeparateDirFile(const char *path, std::string *dir, std::string *file);
size_t  dpSeparateFileExt(const char *filename, std::string *file, std::string *ext);


// アラインが必要な section データを再配置するための単純なアロケータ
class dpSectionAllocator
{
public:
    // data=NULL, size_t size=0xffffffff で初期化した場合、必要な容量を調べるのに使える
    dpSectionAllocator(void *data=NULL, size_t size=0xffffffff);
    // align: 2 の n 乗である必要がある
    void* allocate(size_t size, size_t align);
    size_t getUsed() const;
private:
    void *m_data;
    size_t m_size;
    size_t m_used;
};

class dpTrampolineAllocator
{
public:
    static const size_t page_size = 1024*64;
    static const size_t block_size = 32;

    dpTrampolineAllocator();
    ~dpTrampolineAllocator();
    void* allocate(void *location);
    bool deallocate(void *v);

private:
    class Page;
    typedef std::vector<Page*> page_cont;
    page_cont m_pages;

    Page* createPage(void *location);
    Page* findOwnerPage(void *location);
    Page* findCandidatePage(void *location);
};

template<size_t PageSize, size_t BlockSize>
class dpBlockAllocator
{
public:
    static const size_t page_size = PageSize;
    static const size_t block_size = BlockSize;

    dpBlockAllocator();
    ~dpBlockAllocator();
    void* allocate();
    bool deallocate(void *v);

private:
    class Page;
    typedef std::vector<Page*> page_cont;
    page_cont m_pages;
};
typedef dpBlockAllocator<1024*256, sizeof(dpSymbol)> dpSymbolAllocator;

class dpSymbolTable
{
public:
    dpSymbolTable();
    void addSymbol(dpSymbol *v);
    void sort();
    void clear();
    size_t          getNumSymbols() const;
    dpSymbol*       getSymbol(size_t i);
    dpSymbol*       findSymbolByName(const char *name);

    // F: [](const dpSymbol *sym)
    template<class F>
    void eachSymbols(const F &f)
    {
        size_t n = getNumSymbols();
        for(size_t i=0; i<n; ++i) { f(getSymbol(i)); }
    }

private:
    typedef std::vector<dpSymbol*> symbol_cont;
    symbol_cont m_symbols;
};

#define dpGetBuilder()  m_context->getBuilder()
#define dpGetPatcher()  m_context->getPatcher()
#define dpGetLoader()   m_context->getLoader()


// ロード中の dll は上書き不可能で、そのままだと実行時リビルドできない。
// そのため、指定のファイルをコピーしてそれを扱う。(関連する .pdb もコピーする)
// また、.dll だけでなく .exe も扱える (export された symbol がないと意味がないが)
class dpDllFile
{
public:
    dpDllFile(dpContext *ctx);
    ~dpDllFile();
    void unload();
    virtual bool loadFile(const char *path);
    virtual bool loadMemory(const char *name, void *data);

    virtual dpSymbolTable& getSymbolTable();
    virtual const char*    getPath() const;

    // F: [](const dpSymbol *sym)
    template<class F> void eachSymbols(const F &f) { m_symbols.eachSymbols(f); }

private:
    dpContext *m_context;
    HMODULE m_module;
    bool m_needs_freelibrary;
    std::string m_path;
    std::string m_actual_file;
    std::string m_pdb_path;
    dpSymbolTable m_symbols;
};


class dpLoader
{
public:
    dpLoader(dpContext *ctx, dpOnLoadHandler on_load_handler, dpOnUnloadHandler on_unload_handler, void* user_data);
    ~dpLoader();

    dpDllFile* loadDll(const char *path);
    bool       unload(const char *path);
	void       linkDll(dpDllFile* dll);

    dpDllFile* findBinary(const char *name);

    dpSymbol* findSymbolByName(const char *name);
    dpSymbol* findHostSymbolByName(const char *name);

    dpSymbol* newSymbol(const char *nam=nullptr, void *addr=nullptr, int fla=0, int sect=0, dpDllFile *bin=nullptr);
    void deleteSymbol(dpSymbol *sym);

private:
    typedef std::vector<dpDllFile*>  binary_cont;

    dpContext *m_context;
	dpOnLoadHandler m_on_load_handler;
	dpOnUnloadHandler m_on_unload_handler;
	void* m_user_data;
    binary_cont m_binaries;
    dpSymbolTable m_hostsymbols;
    dpSymbolAllocator m_symalloc;

    void       unloadImpl(dpDllFile *bin);
    template<class BinaryType>
    BinaryType* loadBinaryImpl(const char *path);
};


class dpPatcher
{
public:
    dpPatcher(dpContext *ctx);
    ~dpPatcher();
    void*  patch(dpSymbol *target, dpSymbol *hook);
    bool   unpatchByAddress(void *patched);
    void   unpatchAll();

private:
    typedef std::set<dpPatchData> patch_cont;

    dpContext             *m_context;
    dpTrampolineAllocator m_talloc;
    patch_cont            m_patches;

    void         patchImpl(dpPatchData &pi);
    void         unpatchImpl(const dpPatchData &pi);
    patch_cont::iterator findPatchByAddressImpl(void *addr);
};


class dpBuilder
{
public:
    dpBuilder(dpContext *ctx);
    ~dpBuilder();
    void addModule(const char *path);

    size_t update();
    size_t reload();

private:
    dpContext *m_context;
    std::vector<std::string> m_modulepaths;
	dpTime m_lastmodtime;
};


class dpContext
{
public:
    dpContext(dpOnLoadHandler on_load_handler, dpOnUnloadHandler on_unload_handler, void* user_data);
    ~dpContext();
    dpBuilder* getBuilder();
    dpPatcher* getPatcher();
    dpLoader*  getLoader();

    bool   unpatchByAddress(void *target_or_hook_addr);
    void   unpatchAll();

private:
    dpBuilder *m_builder;
    dpPatcher *m_patcher;
    dpLoader  *m_loader;
};


template<class Container, class F>
inline auto dpFind(Container &cont, const F &f) -> decltype(cont.begin())
{
	for (auto i = cont.begin(); i != cont.end(); ++i)
	{
		if (f(*i))
			return i;
	}
	return cont.end();
}

struct dpSymbolS
{
    const char *name;
    void *address;
    int flags;
};

dpAPI bool          dpDemangle(const char *mangled, char *demangled, size_t buflen);
dpAPI void          dpPrint(const char* fmt, ...);
