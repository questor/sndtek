﻿// created by i-saint
// distributed under Creative Commons Attribution (CC BY) license.
// https://github.com/i-saint/DynamicPatcher

#include "DynamicPatcher.h"
#include "dpInternal.h"


static int LoadFileToMemory(const char* path, void** data)
{
	FILE* fp;
	int size = -1;

	*data = NULL;

	if (fp = fopen(path, "rb"))
	{
		fseek(fp, 0, SEEK_END);
		size = ftell(fp);

		if (size > 0)
		{
			*data = malloc(size);
			fseek(fp, 0, SEEK_SET);
			fread(*data, 1, size, fp);
		}

		fclose(fp);
		return size;
	}

	return -1;
}


dpDllFile::dpDllFile(dpContext *ctx)
    : m_context(ctx)	
    , m_module(nullptr), m_needs_freelibrary(false)
{
}

dpDllFile::~dpDllFile()
{
    unload();
}

void dpDllFile::unload()
{
	dpPatcher* patcher = dpGetPatcher();
    eachSymbols([&](dpSymbol *sym){
        if(dpIsFunction(sym->flags))
			patcher->unpatchByAddress(sym->address);
    });

    eachSymbols([&](dpSymbol *sym){ dpGetLoader()->deleteSymbol(sym); });
    if(m_module && m_needs_freelibrary) {
        ::FreeLibrary(m_module);
        dpDeleteFile(m_actual_file.c_str()); m_actual_file.clear();
        dpDeleteFile(m_pdb_path.c_str()); m_pdb_path.clear();
    }
    m_needs_freelibrary = false;
    m_symbols.clear();
}

struct CV_INFO_PDB70
{
    DWORD  CvSignature;
    GUID Signature;
    DWORD Age;
    BYTE PdbFileName[1];
};

// fill_gap: .dll ファイルをそのままメモリに移した場合はこれを true にする必要があります。
// LoadLibrary() で正しくロードしたものは section の再配置が行われ、元ファイルとはデータの配置にズレが生じます。
// fill_gap==true の場合このズレを補正します。
CV_INFO_PDB70* dpGetPDBInfoFromModule(void *pModule, bool fill_gap)
{
    if(!pModule) { return nullptr; }

    PBYTE pData = (PUCHAR)pModule;
    PIMAGE_DOS_HEADER pDosHeader = (PIMAGE_DOS_HEADER)pData;
    PIMAGE_NT_HEADERS pNtHeaders = (PIMAGE_NT_HEADERS)(pData + pDosHeader->e_lfanew);
    if(pDosHeader->e_magic==IMAGE_DOS_SIGNATURE && pNtHeaders->Signature==IMAGE_NT_SIGNATURE) {
        ULONG DebugRVA = pNtHeaders->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_DEBUG].VirtualAddress;
        if(DebugRVA==0) { return nullptr; }

        PIMAGE_SECTION_HEADER pSectionHeader = IMAGE_FIRST_SECTION(pNtHeaders);
        for(size_t i=0; i<pNtHeaders->FileHeader.NumberOfSections; ++i) {
            PIMAGE_SECTION_HEADER s = pSectionHeader+i;
            if(DebugRVA >= s->VirtualAddress && DebugRVA < s->VirtualAddress+s->SizeOfRawData) {
                pSectionHeader = s;
                break;
            }
        }
        if(fill_gap) {
            DWORD gap = pSectionHeader->VirtualAddress - pSectionHeader->PointerToRawData;
            pData -= gap;
        }

        PIMAGE_DEBUG_DIRECTORY pDebug;
        pDebug = (PIMAGE_DEBUG_DIRECTORY)(pData + DebugRVA);
        if(DebugRVA!=0 && DebugRVA < pNtHeaders->OptionalHeader.SizeOfImage && pDebug->Type==IMAGE_DEBUG_TYPE_CODEVIEW) {
            CV_INFO_PDB70 *pCVI = (CV_INFO_PDB70*)(pData + pDebug->AddressOfRawData);
            if(pCVI->CvSignature=='SDSR') {
                return pCVI;
            }
        }
    }
    return nullptr;
}

struct PDBStream70
{
    DWORD impv;
    DWORD sig;
    DWORD age;
    GUID sig70;
};

// pdb ファイルから Age & GUID 情報を抽出します
PDBStream70* dpGetPDBSignature(void *mapped_pdb_file)
{
// thanks to https://code.google.com/p/pdbparser/

#define ALIGN_UP(x, align)      ((x+align-1) & ~(align-1))
#define STREAM_SPAN_PAGES(size) (ALIGN_UP(size,pHeader->dwPageSize)/pHeader->dwPageSize)
#define PAGE(x)                 (pImageBase + pHeader->dwPageSize*(x))
#define PDB_STREAM_PDB    1

    struct MSF_Header
    {
        char szMagic[32];          // 0x00  Signature
        DWORD dwPageSize;          // 0x20  Number of bytes in the pages (i.e. 0x400)
        DWORD dwFpmPage;           // 0x24  FPM (free page map) page (i.e. 0x2)
        DWORD dwPageCount;         // 0x28  Page count (i.e. 0x1973)
        DWORD dwRootSize;          // 0x2c  Size of stream directory (in bytes; i.e. 0x6540)
        DWORD dwReserved;          // 0x30  Always zero.
        DWORD dwRootPointers[0x49];// 0x34  Array of pointers to root pointers stream. 
    };

    BYTE *pImageBase = (BYTE*)mapped_pdb_file;
    MSF_Header *pHeader = (MSF_Header*)pImageBase;

    DWORD RootPages = STREAM_SPAN_PAGES(pHeader->dwRootSize);
    DWORD RootPointersPages = STREAM_SPAN_PAGES(RootPages*sizeof(DWORD));

    std::string RootPointersRaw;
    RootPointersRaw.resize(RootPointersPages * pHeader->dwPageSize);
    for(DWORD i=0; i<RootPointersPages; i++) {
        PVOID Page = PAGE(pHeader->dwRootPointers[i]);
        SIZE_T Offset = pHeader->dwPageSize * i;
        memcpy(&RootPointersRaw[0]+Offset, Page, pHeader->dwPageSize);
    }
    DWORD *RootPointers = (DWORD*)&RootPointersRaw[0];

    std::string StreamInfoRaw;
    StreamInfoRaw.resize(RootPages * pHeader->dwPageSize);
    for(DWORD i=0; i<RootPages; i++) {
        PVOID Page = PAGE(RootPointers[i]);
        SIZE_T Offset = pHeader->dwPageSize * i;
        memcpy(&StreamInfoRaw[0]+Offset, Page, pHeader->dwPageSize);
    }
    DWORD StreamCount = *(DWORD*)&StreamInfoRaw[0];
    DWORD *dwStreamSizes = (DWORD*)&StreamInfoRaw[4];

    {
        DWORD *StreamPointers = &dwStreamSizes[StreamCount];
        DWORD page = 0;
        for(DWORD i=0; i<PDB_STREAM_PDB; i++) {
            DWORD nPages = STREAM_SPAN_PAGES(dwStreamSizes[i]);
            page += nPages;
        }
        DWORD *pdwStreamPointers = &StreamPointers[page];

        PVOID Page = PAGE(pdwStreamPointers[0]);
        return (PDBStream70*)Page;
    }

#undef PDB_STREAM_PDB
#undef PAGE
#undef STREAM_SPAN_PAGES
#undef ALIGN_UP
}


std::string DestructiveGetOutputFile(const std::string& file_a, const std::string& file_b)
{
	// Can't delete a DLL that has an open handle as it's been memory-mapped
	// You can *of course* rename it while memory-mapped
	bool file_a_exists = dpFileExists(file_a.c_str());
	bool file_b_exists = dpFileExists(file_b.c_str());

	// Want to delete all previous output files in case they weren't cleaned up properly
	bool file_a_usable = !file_a_exists || dpDeleteFile(file_a.c_str());
	bool file_b_usable = !file_b_exists || dpDeleteFile(file_b.c_str());

	if (file_a_usable)
		return file_a;
	if (file_b_usable)
		return file_b;

	return "";
}

bool dpDllFile::loadFile(const char *path)
{
    if(m_module && m_path==path) { return true; }

    // ロード中の dll と関連する pdb はロックがかかってしまい、以降のビルドが失敗するようになるため、その対策を行う。
    // .dll と .pdb を一時ファイルにコピーしてそれをロードする。
    // コピーの際、
    // ・.dll に含まれる .pdb へのパスをコピー版へのパスに書き換える
    // ・.dll と .pdb 両方に含まれる pdb の GUID を更新する
    //   (VisualC++2012 の場合、これを怠ると最初にロードした dll の pdb が以降の更新された dll でも使われ続けてしまう)

    std::string pdb_base;
    GUID uuid;
    {
        // dll をメモリに map
        void *data = NULL;
        int data_size = LoadFileToMemory(path, &data);
        if (data_size == -1)
		{
            dpPrintError("file not found %s\n", path);
            return false;
        }

        m_path = path;

        std::string ext;
        dpSeparateFileExt(path, &m_actual_file, &ext);
		std::string test_file_a = m_actual_file + "a." + ext;
		std::string test_file_b = m_actual_file + "b." + ext;
		m_actual_file = DestructiveGetOutputFile(test_file_a, test_file_b);
		if (m_actual_file.empty())
			return false;

        // pdb へのパスと GUID を更新
        if(CV_INFO_PDB70 *cv=dpGetPDBInfoFromModule(data, true))
		{
			// Source PDB filename
            char *pdb = (char*)cv->PdbFileName;
            pdb_base = pdb;

			std::string pdb_file, ext;
			dpSeparateFileExt(pdb, &pdb_file, &ext);
			std::string pdb_file_a = std::string(pdb_file, 0, pdb_file.length() - 3) + ".a.pdb";
			std::string pdb_file_b = std::string(pdb_file, 0, pdb_file.length() - 3) + ".b.pdb";
			m_pdb_path = DestructiveGetOutputFile(pdb_file_a, pdb_file_b);
			if (!m_pdb_path.empty())
			{
				strncpy(pdb, m_pdb_path.c_str(), m_pdb_path.length());
			}

            cv->Signature.Data1 += ::clock();
            uuid = cv->Signature;
        }
        // dll を一時ファイルにコピー
        dpWriteFile(m_actual_file.c_str(), data, data_size);
        free(data);
    }

    {
        // pdb を GUID を更新してコピー
        void* data = NULL;
        int data_size = LoadFileToMemory(pdb_base.c_str(), &data);
        if(data_size != -1)
		{
            if(PDBStream70 *sig = dpGetPDBSignature(data)) {
                sig->sig70 = uuid;
            }
            dpWriteFile(m_pdb_path.c_str(), data, data_size);
            free(data);
        }
    }

    HMODULE module = ::LoadLibraryA(m_actual_file.c_str());
    if(loadMemory(path, module)) {
        m_needs_freelibrary = true;
        return true;
    }
    else {
        dpPrintError("LoadLibraryA() failed. %s\n", path);
    }
    return false;
}

bool dpDllFile::loadMemory(const char *path, void *data)
{
    if(data==nullptr) { return false; }
    if(m_module && m_path==path) { return true; }

    m_path = path;
    m_module = (HMODULE)data;

    size_t ImageBase = (size_t)m_module;
    PIMAGE_DOS_HEADER pDosHeader = (PIMAGE_DOS_HEADER)ImageBase;
    if(pDosHeader->e_magic!=IMAGE_DOS_SIGNATURE) { return false; }

    PIMAGE_NT_HEADERS pNTHeader = (PIMAGE_NT_HEADERS)(ImageBase + pDosHeader->e_lfanew);
    DWORD RVAExports = pNTHeader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress;
	if (RVAExports != NULL)
	{
		IMAGE_EXPORT_DIRECTORY *pExportDirectory = (IMAGE_EXPORT_DIRECTORY *)(ImageBase + RVAExports);
		DWORD *RVANames = (DWORD*)(ImageBase+pExportDirectory->AddressOfNames);
		WORD *RVANameOrdinals = (WORD*)(ImageBase+pExportDirectory->AddressOfNameOrdinals);
		DWORD *RVAFunctions = (DWORD*)(ImageBase+pExportDirectory->AddressOfFunctions);
		for(DWORD i=0; i<pExportDirectory->NumberOfFunctions; ++i) {
			char *pName = (char*)(ImageBase+RVANames[i]);
			void *pFunc = (void*)(ImageBase+RVAFunctions[RVANameOrdinals[i]]);
			m_symbols.addSymbol(dpGetLoader()->newSymbol(pName, pFunc, dpE_Code|dpE_Read|dpE_Execute|dpE_Export, 0, this));
		}
	}

    m_symbols.sort();
    return true;
}

dpSymbolTable& dpDllFile::getSymbolTable()            { return m_symbols; }
const char*    dpDllFile::getPath() const             { return m_path.c_str(); }
