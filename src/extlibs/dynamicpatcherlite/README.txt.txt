
 Don Williamson ?@Donzanoid  17 Min.vor 17 Minuten 
@ocornut pre-release of DynamicPatcherLite (more work to do before release). 
Help you relieve the E&C dependency :)


@Donzanoid Are you working on that? Instructions at 
https://github.com/i-saint/DynamicPatcher � unclear on how to use for whole project.

@ocornut no, it's my own fork. Original code is vastly over-engineered, confusing 
and handles unneeded cases. But bloody useful.

@ocornut check DynamicPatcher.h in zip for API difference

@Donzanoid cool! Do i need to move all my code to a DLL? Patching will happen within 
dpUpdate()?

@ocornut 
init:
dpInitialize(OnLoadDLL, OnUnloadDLL, &m_Ctx);
dpWatchDll("Game.dynamic.dll");

main loop:
dpUpdate();

shut:
dpFinalize();

@ocornut you should build to Game.dll if the game isn't running; otherwise build to 
Game.dynamic.dll

 
@ocornut note that it works with LOAD time DLLs so you don't have to hide everything 
behind interfaces and just expose your APIs

@Donzanoid Not sure to understand that sentence. Load DLL?

@ocornut this works for load-time DLLs whereas LoadLibrary works for run-time DLLS 
https://msdn.microsoft.com/en-us/library/windows/desktop/ms684184(v=vs.85).aspx 
vs 
https://msdn.microsoft.com/en-us/library/windows/desktop/ms685090(v=vs.85).aspx

@Donzanoid Hmm i didn't even knew there was two types of dll linkage!

@ocornut well, if you use LoadLibrary then you won't need dp-style reload. Just use LoadLibrary and wrap your entire game
