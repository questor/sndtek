﻿// created by i-saint
// distributed under Creative Commons Attribution (CC BY) license.
// https://github.com/i-saint/DynamicPatcher

#pragma once

#ifndef dpDisable

#define dpDLLExport     __declspec(dllexport)
#define dpDLLImport     __declspec(dllimport)

#if defined(dpDLLImpl)
#   define dpAPI dpDLLExport
#elif !defined(dpNoLib)
#   define dpAPI dpDLLImport
#else
#   define dpAPI
#endif

#define dpScope(...)    __VA_ARGS__

typedef void (*dpOnLoadHandler)(void* user_data);
typedef void (*dpOnUnloadHandler)(void* user_data);

dpAPI bool   dpInitialize(dpOnLoadHandler on_load_handler, dpOnUnloadHandler on_unload_handler, void* user_data);
dpAPI void   dpWatchDll(const char *path);
dpAPI bool   dpReloadDll(const char* path);
dpAPI size_t dpUpdate(); // reloads and links modified modules.
dpAPI bool   dpFinalize();

#else  // dpDisable

#define dpScope(...) 
#define dpInitialize(...) 
#define dpWatchDll(...) 
#define dpReloadDll(...)
#define dpUpdate(...) 
#define dpFinalize(...) 

#endif // dpDisable
