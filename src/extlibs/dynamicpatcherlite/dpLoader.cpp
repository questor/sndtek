﻿// created by i-saint
// distributed under Creative Commons Attribution (CC BY) license.
// https://github.com/i-saint/DynamicPatcher

#include "DynamicPatcher.h"
#include "dpInternal.h"
#include <psapi.h>
#pragma comment(lib, "psapi.lib")


static const int g_host_symbol_flags = dpE_Code|dpE_Read|dpE_Execute|dpE_HostSymbol|dpE_NameNeedsDelete;


dpSymbol* dpLoader::findHostSymbolByName(const char *name)
{
    if(dpSymbol *sym = m_hostsymbols.findSymbolByName(name)) {
        return sym;
    }

    char buf[sizeof(SYMBOL_INFO)+1024];
    PSYMBOL_INFO sinfo = (PSYMBOL_INFO)buf;
    sinfo->SizeOfStruct = sizeof(SYMBOL_INFO);
    sinfo->MaxNameLen = 1024;
    if(::SymFromName(::GetCurrentProcess(), name, sinfo)==FALSE) {
        return nullptr;
    }
    char *namebuf = new char[sinfo->NameLen+1];
    strncpy(namebuf, sinfo->Name, sinfo->NameLen+1);
    m_hostsymbols.addSymbol(newSymbol(namebuf, (void*)sinfo->Address, g_host_symbol_flags, 0, nullptr));
    m_hostsymbols.sort();
    return m_hostsymbols.findSymbolByName(name);
}

dpLoader::dpLoader(dpContext *ctx, dpOnLoadHandler on_load_handler, dpOnUnloadHandler on_unload_handler, void* user_data)
    : m_context(ctx)
	, m_on_load_handler(on_load_handler)
	, m_on_unload_handler(on_unload_handler)
	, m_user_data(user_data)
{
}

dpLoader::~dpLoader()
{
    while(!m_binaries.empty()) { unloadImpl(m_binaries.front()); }
    m_hostsymbols.eachSymbols([&](dpSymbol *sym){ deleteSymbol(sym); });
    m_hostsymbols.clear();
}

void dpLoader::unloadImpl( dpDllFile *bin )
{
    m_binaries.erase(std::find(m_binaries.begin(), m_binaries.end(), bin));
	m_on_unload_handler(m_user_data);
    std::string path = bin->getPath();
    delete bin;
    dpPrintInfo("unloaded \"%s\"\n", path.c_str());
}

dpDllFile* dpLoader::loadDll(const char *path)
{
    dpDllFile *old = findBinary(path);
    dpDllFile *ret = new dpDllFile(m_context);
    if(ret->loadFile(path)) {
        if(old) { unloadImpl(old); }
        m_binaries.push_back(ret);
		linkDll(ret);
        dpPrintInfo("loaded \"%s\"\n", ret->getPath());
    }
    else {
        delete ret;
        ret = nullptr;
    }
    return ret;
}

bool dpLoader::unload(const char *path)
{
    if(dpDllFile *bin=findBinary(path)) {
        unloadImpl(bin);
        return true;
    }
    return false;
}

void dpLoader::linkDll(dpDllFile* dll)
{
    // 有効にされていれば dllexport な関数を自動的に patch
    dll->eachSymbols([&](dpSymbol *sym){
        if(dpIsExportFunction(sym->flags)) {
            dpGetPatcher()->patch(findHostSymbolByName(sym->name), sym);
        }
    });

	m_on_load_handler(m_user_data);
}

dpSymbol* dpLoader::findSymbolByName(const char *name)
{
    size_t n = m_binaries.size();
    for(size_t i=0; i<n; ++i) {
        if(dpSymbol *s = m_binaries[i]->getSymbolTable().findSymbolByName(name)) {
            return s;
        }
    }
    return nullptr;
}

dpDllFile* dpLoader::findBinary(const char *name)
{
    auto p = dpFind(m_binaries,
        [=](dpDllFile *b){ return _stricmp(b->getPath(), name)==0; }
    );
    return p!=m_binaries.end() ? *p : nullptr;
}

dpSymbol* dpLoader::newSymbol(const char *nam, void *addr, int fla, int sect, dpDllFile *bin)
{
    return new (m_symalloc.allocate()) dpSymbol(nam, addr, fla, sect, bin);
}

void dpLoader::deleteSymbol(dpSymbol *sym)
{
    sym->~dpSymbol();
    m_symalloc.deallocate(sym);
}
