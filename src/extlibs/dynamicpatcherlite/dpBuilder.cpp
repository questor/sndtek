﻿// created by i-saint
// distributed under Creative Commons Attribution (CC BY) license.
// https://github.com/i-saint/DynamicPatcher

#include "DynamicPatcher.h"
#include "dpInternal.h"

dpBuilder::dpBuilder(dpContext *ctx)
    : m_context(ctx)
	, m_lastmodtime(dpGetSystemTime())
{
}

dpBuilder::~dpBuilder()
{
}

void dpBuilder::addModule(const char *path)
{
    std::string tmp = path;
    auto p = dpFind(m_modulepaths, [&](const std::string &s){return s==tmp;});
    if(p==m_modulepaths.end()) {
        m_modulepaths.push_back(tmp);
    }
}

size_t dpBuilder::update()
{
	return reload();
}

size_t dpBuilder::reload()
{
    size_t n = 0;
	bool changed = false;
    for(size_t i=0; i<m_modulepaths.size(); ++i) {
		const std::string& path = m_modulepaths[i];
        if(dpGetMTime(path.c_str())>m_lastmodtime) {
			changed = true;
            dpDllFile *old = dpGetLoader()->findBinary(path.c_str());
            if(dpDllFile *bin = dpGetLoader()->loadDll(path.c_str())) {
                if(bin!=old) { ++n; }
            }
        }
    }

	if (changed)
		m_lastmodtime = dpGetSystemTime();

    return n;
}
