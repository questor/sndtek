﻿// created by i-saint
// distributed under Creative Commons Attribution (CC BY) license.
// https://github.com/i-saint/DynamicPatcher

#include "DynamicPatcher.h"
#include "dpInternal.h"

dpContext::dpContext(dpOnLoadHandler on_load_handler, dpOnUnloadHandler on_unload_handler, void* user_data)
{
    m_builder = new dpBuilder(this);
    m_patcher = new dpPatcher(this);
    m_loader  = new dpLoader(this, on_load_handler, on_unload_handler, user_data);
}

dpContext::~dpContext()
{
    // バイナリ unload 時に適切に unpatch するには patcher より先に loader を破棄する必要がある
    delete m_loader;  m_loader=nullptr;
    delete m_patcher; m_patcher=nullptr;
    delete m_builder; m_builder=nullptr;
}

dpBuilder* dpContext::getBuilder() { return m_builder; }
dpPatcher* dpContext::getPatcher() { return m_patcher; }
dpLoader*  dpContext::getLoader()  { return m_loader; }

bool dpContext::unpatchByAddress(void *target_or_hook_addr)
{
    return m_patcher->unpatchByAddress(target_or_hook_addr);
}

void dpContext::unpatchAll()
{
    return m_patcher->unpatchAll();
}
