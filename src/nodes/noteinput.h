
#ifndef __NODES_NOTEINPUT_H__
#define __NODES_NOTEINPUT_H__

namespace ImGui {

class NoteInputNode : public ProcessingNode {
protected:
   NoteInputNode() : ProcessingNode(1) {}
   static const int TYPE = SNT_NOTE_INPUT;

   const char* getTooltip() const {return "Frequency of note to play.";}
   const char* getInfo() const {return "NoteInput";}

   float mFrequency;

public:
   static NoteInputNode* Create(const ImVec2& pos) {
      NoteInputNode* node = (NoteInputNode*) ImGui::MemAlloc(sizeof(NoteInputNode));
      IM_PLACEMENT_NEW (node) NoteInputNode();

      node->init("Note Input",pos,"","freq",TYPE);
      return node;
   }

   void setFrequency(float freq) {
      mFrequency = freq;
   }

   void renderSamples(int numberSamples) {
      for(int i=0; i<numberSamples; ++i) {
         mWorkBuffers[0][i] = mFrequency;        
      }
   }
};

}; //namespace ImGui

#endif   //#ifndef __NODES_NOTEINPUT_H__
