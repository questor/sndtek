
#ifndef __NODES_BIQUADFILTER_H__
#define __NODES_BIQUADFILTER_H__

#include "src/utils.h"

namespace ImGui {

//taken from https://github.com/voidqk/sndfilter/

/*
A lowpass filter allows frequencies below the cutoff frequency to pass through and attenuates frequencies above the cutoff. It 
implements a standard second-order resonant lowpass filter with 12dB/octave rolloff.
frequency   -  The cutoff frequency
Q           -  Controls how peaked the response will be at the cutoff frequency. A large value makes the response more peaked. 
               Please note that for this filter type, this value is not a traditional Q, but is a resonance value in decibels.
gain        -  Not used in this filter type

highpass
A highpass filter is the opposite of a lowpass filter. Frequencies above the cutoff frequency are passed through, but frequencies 
below the cutoff are attenuated. It implements a standard second-order resonant highpass filter with 12dB/octave rolloff.
frequency   -  The cutoff frequency below which the frequencies are attenuated
Q           -  Controls how peaked the response will be at the cutoff frequency. A large value makes the response more peaked. Please note that for this filter type, this value is not a traditional Q, but is a resonance value in decibels.
gain        -  Not used in this filter type

bandpass 
A bandpass filter allows a range of frequencies to pass through and attenuates the frequencies below and above this frequency 
range. It implements a second-order bandpass filter.
frequency   -  The center of the frequency band
Q           -  Controls the width of the band. The width becomes narrower as the Q value increases.
gain        -  Not used in this filter type

lowshelf 
The lowshelf filter allows all frequencies through, but adds a boost (or attenuation) to the lower frequencies. It implements 
a second-order lowshelf filter.
frequency   -  The upper limit of the frequences where the boost (or attenuation) is applied.
Q           -  Not used in this filter type.
gain        -  The boost, in dB, to be applied. If the value is negative, the frequencies are attenuated.

highshelf   
The highshelf filter is the opposite of the lowshelf filter and allows all frequencies through, but adds a boost to the higher 
frequencies. It implements a second-order highshelf filter
frequency   -  The lower limit of the frequences where the boost (or attenuation) is applied.
Q           -  Not used in this filter type.
gain        -  The boost, in dB, to be applied. If the value is negative, the frequencies are attenuated.

peaking  
The peaking filter allows all frequencies through, but adds a boost (or attenuation) to a range of frequencies.
frequency   -  The center frequency of where the boost is applied.
Q           -  Controls the width of the band of frequencies that are boosted. A large value implies a narrow width.
gain        -  The boost, in dB, to be applied. If the value is negative, the frequencies are attenuated.

notch 
The notch filter (also known as a band-stop or band-rejection filter) is the opposite of a bandpass filter. It allows all 
frequencies through, except for a set of frequencies.
frequency   -  The center frequency of where the notch is applied.
Q           -  Controls the width of the band of frequencies that are attenuated. A large value implies a narrow width.
gain        -  Not used in this filter type.

allpass  
An allpass filter allows all frequencies through, but changes the phase relationship between the various frequencies. It 
implements a second-order allpass filter
frequency   -  The frequency where the center of the phase transition occurs. Viewed another way, this is the frequency with maximal group delay.
Q           -  Controls how sharp the phase transition is at the center frequency. A larger value implies a sharper transition and a larger group delay.
gain        -  Not used in this filter type. 


Q of type AudioParam, readonly
The Q factor has a default value of 1. Its nominal range is (−∞,∞). This is not used for lowshelf or highshelf filters.

frequency of type AudioParam, readonly
The frequency at which the BiquadFilterNode will operate, in Hz. Its default value is 350Hz. Its nominal range is [0, Nyquist frequency].

gain of type AudioParam, readonly
The gain has a default value of 0. Its nominal range is  (−∞,∞). Its value is in dB units. The gain is only used for lowshelf, highshelf, and peaking filters.
 */

class BiquadFilterNode : public ProcessingNode {
protected:
   BiquadFilterNode() : ProcessingNode(1) {}
   static const int TYPE = SNT_BIQUAD_FILTER;

   const char* getTooltip() const {return "Fixed value.";}
   const char* getInfo() const {return "Fixed value.\n\nTakes the first sample of the workbuffer and uses these values for the next block\nGain used by Peaking, Lowshelf and Highshelf.\nLow- and Highpass use resonace instead of Q";}

   static bool getTextFromEnumIndex(void* ,int value,const char** pTxt) {
      if(!pTxt) 
         return false;
      static const char* values[] = {"Lowpass", "Highpass", "Bandpass", "Notch", "Peaking", "Allpass", "Lowshelf", "Highshelf"};
      static int numValues = (int)(sizeof(values)/sizeof(values[0]));
      if(value>=0 && value<numValues) 
         *pTxt = values[value];
      else 
         *pTxt = "UNKNOWN";
      return true;
   }

   int mFilterType;

public:
   static BiquadFilterNode* Create(const ImVec2& pos) {
      BiquadFilterNode* node = (BiquadFilterNode*) ImGui::MemAlloc(sizeof(BiquadFilterNode));
      IM_PLACEMENT_NEW (node) BiquadFilterNode();

      node->init("Biquad Filter",pos,"snd;freq;q;gain","snd",TYPE);
      node->fields.addFieldEnum(&node->mFilterType,8,&getTextFromEnumIndex,"Type","Choose your favourite");

      node->mFilterType = 0;

      return node;
   }

   typedef struct {
      float b0, b1, b2;
      float a1, a2;
      float xn1, xn2, yn1, yn2;
   } BiquadState;

   BiquadState mState;

   void scaleState(float amt) {
      mState.b0 = amt;
      mState.b1 = 0.0f;
      mState.b2 = 0.0f;
      mState.a1 = 0.0f;
      mState.a2 = 0.0f;
   }

   void passThroughState() {
      scaleState(1.0f);
   }
   void zeroState() {
      scaleState(0.0f);
   }

   void processBiquad(int numberSamples) {
      float b0 = mState.b0;
      float b1 = mState.b1;
      float b2 = mState.b2;
      float a1 = mState.a1;
      float a2 = mState.a2;
      float xn1 = mState.xn1;
      float xn2 = mState.xn2;
      float yn1 = mState.yn1;
      float yn2 = mState.yn2;

      ProcessingNode *inputNodeSnd = mInputNodes[0].node;
      int inputSlot = mInputNodes[0].slot;

      for(int i=0; i<numberSamples; ++i) {
         float val = inputNodeSnd->mWorkBuffers[inputSlot][i];
         float out = b0*val + b1*xn1 + b2*xn2 - a1*yn1 - a2*yn2;
         mWorkBuffers[0][i] = out;
         xn2 = xn1;
         xn1 = val;
         yn2 = yn1;
         yn1 = out;
      }

      mState.xn1 = xn1;
      mState.xn2 = xn2;
      mState.yn1 = yn1;
      mState.yn2 = yn2;
   }

   void renderSamples(int numberSamples) {
      if (mInputNodes.size() == 3 || mInputNodes.size() == 4) {

         ProcessingNode *inputNodeSnd = mInputNodes[0].node;
         if(inputNodeSnd == nullptr)
            return;

         ProcessingNode *inputNodeFreq = mInputNodes[1].node;
         if(inputNodeFreq == nullptr)
            return;
         float freq = inputNodeFreq->mWorkBuffers[mInputNodes[1].slot][0];

         ProcessingNode *inputNodeQ = mInputNodes[2].node;
         if(inputNodeQ == nullptr)
            return;
         float q = inputNodeQ->mWorkBuffers[mInputNodes[2].slot][0];

         ProcessingNode *inputNodeGain = mInputNodes[3].node;
         int inputSlotGain = mInputNodes[3].slot;

         float nyquist = cSampleRate * 0.5f;

         switch(mFilterType) {
         case 0:  //Lowpass
            {
               float cutoff = freq / nyquist;
               if(cutoff >= 1.0f) {
                  passThroughState();
               } else if(cutoff <= 0.0f) {
                  zeroState();
               } else {
                  float resonance = db2linear(q);      //convert from db to linear
                  float theta = cPI*2.0f*cutoff;
                  float alpha = sin(theta)/(2.0f*resonance);
                  float cosw = cosf(theta);
                  float beta = (1.0f - cosw)*0.5f;
                  float a0inv = 1.0f / (1.0f+alpha);
                  mState.b0 = a0inv * beta;
                  mState.b1 = a0inv * 2.0f * beta;
                  mState.b2 = a0inv * beta;
                  mState.a1 = a0inv * -2.0f * cosw;
                  mState.a2 = a0inv * (1.0f - alpha);
               }
               processBiquad(numberSamples);
            }
            break;
         case 1:  //Highpass
            {
               float cutoff = freq / nyquist;
               if(cutoff >= 1.0f) {
                  zeroState();
               } else if(cutoff <= 0.0f) {
                  passThroughState();
               } else {
                  float resonance = db2linear(q);      //convert from db to linear
                  float theta = cPI*2.0f*cutoff;
                  float alpha = sin(theta)/(2.0f*resonance);
                  float cosw = cosf(theta);
                  float beta = (1.0f + cosw)*0.5f;
                  float a0inv = 1.0f / (1.0f+alpha);
                  mState.b0 = a0inv * beta;
                  mState.b1 = a0inv * -2.0f * beta;
                  mState.b2 = a0inv * beta;
                  mState.a1 = a0inv * -2.0f * cosw;
                  mState.a2 = a0inv * (1.0f - alpha);
               }
               processBiquad(numberSamples);
            }
            break;
         case 2:  //Bandpass
            {
               freq /= nyquist;
               if(freq <= 0.0f || freq >= 1.0f) {
                  zeroState();
               } else if(q <= 0.0f) {
                  passThroughState();
               } else {
                  float w0 = cPI * 2.0f * freq;
                  float alpha = sin(w0) / (2.0f * q);
                  float k = cos(w0);
                  float a0inv = 1.0f / (1.0f + alpha);

                  mState.b0 = a0inv * alpha;
                  mState.b1 = 0;
                  mState.b2 = a0inv * -alpha;
                  mState.a1 = a0inv * -2.0f * k;
                  mState.a2 = a0inv * (1.0f - alpha);
               }
               processBiquad(numberSamples);
            }
            break;
         case 3:  //Notch
            {
               freq /= nyquist;
               if(freq <= 0.0f || freq >= 1.0f) {
                  passThroughState();
               } else if(q <= 0.0f) {
                  zeroState();
               } else {
                  float w0 = cPI * 2.0f * freq;
                  float alpha = sin(w0) / (2.0f * q);
                  float k = cos(w0);
                  float a0inv = 1.0f / (1.0f + alpha);

                  mState.b0 = a0inv;
                  mState.b1 = a0inv * -2.0f * k;
                  mState.b2 = a0inv;
                  mState.a1 = a0inv * -2.0f * k;
                  mState.a2 = a0inv * (1.0f - alpha);
               }
               processBiquad(numberSamples);
            }
            break;
         case 4:  //Peaking
            {
               freq /= nyquist;
               if(freq <= 0.0f || freq >= 1.0f) {
                  passThroughState();
               } else {
                  if(inputNodeGain == nullptr)
                     return;
                  float gain = inputNodeGain->mWorkBuffers[inputSlotGain][0];

                  float a = powf(10.0f, gain*0.025f);          //square root of gain converted from dB to linear

                  if(q <= 0.0f) {
                     scaleState(a*a);
                  } else {
                     float w0 = cPI * 2.0f * freq;
                     float alpha = sin(w0) / (2.0f * q);
                     float k = cos(w0);
                     float a0inv = 1.0f / (1.0f + alpha / a);
                     mState.b0 = a0inv * (1.0f + alpha * a);
                     mState.b1 = a0inv * -2.0f * k;
                     mState.b2 = a0inv * (1.0f - alpha * a);
                     mState.a1 = a0inv * -2.0f * k;
                     mState.a2 = a0inv * (1.0f - alpha / a);
                  }
               }
               processBiquad(numberSamples);
            }
            break;
         case 5:  //Allpass
            {
               freq /= nyquist;
               if(freq <= 0.0f || freq >= 1.0f) {
                  passThroughState();
               } else if(q <= 0.0f) {
                  scaleState(-1.0f);            //invert the sample
               } else {
                  float w0 = cPI * 2.0f * freq;
                  float alpha = sin(w0) / (2.0f * q);
                  float k = cos(w0);
                  float a0inv = 1.0f / (1.0f + alpha);
                  mState.b0 = a0inv * (1.0f - alpha);
                  mState.b1 = a0inv * -2.0f * k;
                  mState.b2 = a0inv * (1.0f + alpha);
                  mState.a1 = a0inv * -2.0f * k;
                  mState.a2 = a0inv * (1.0f - alpha);                  
               }
               processBiquad(numberSamples);
            }
            break;
         case 6:  //Lowshelf
            {
               freq /= nyquist;
               if(freq <= 0.0f || q == 0.0f) {
                  passThroughState();
               } else {
                  if(inputNodeGain == nullptr)
                     return;
                  float gain = inputNodeGain->mWorkBuffers[inputSlotGain][0];

                  float a = powf(10.0f, gain*0.025f);          //square root of gain converted from dB to linear

                  if(freq >= 1.0f) {
                     scaleState(a*a);
                  } else {
                     float w0 = cPI * 2.0f * freq;
                     float ainn = (a + 1.0f / a) * (1.0f / q - 1.0f) + 2.0f;
                     if(ainn < 0)
                        ainn = 0;
                     float alpha = 0.5f * sin(w0) * sqrt(ainn);
                     float k = cos(w0);
                     float k2 = 2.0f * sqrt(a) * alpha;
                     float ap1 = a + 1.0f;
                     float am1 = a - 1.0f;
                     float a0inv = 1.0f / (ap1 + am1 * k + k2);
                     mState.b0 = a0inv * a * (ap1 - am1 * k + k2);
                     mState.b1 = a0inv * 2.0f * a * (am1 - ap1 * k);
                     mState.b2 = a0inv * a * (ap1 - am1 * k - k2);
                     mState.a1 = a0inv * -2.0f * (am1 + ap1 * k);
                     mState.a2 = a0inv * (ap1 + am1 * k - k2);
                  }
               }
               processBiquad(numberSamples);
            }
            break;
         case 7:  //Highshelf
            {
               freq /= nyquist;
               if(freq <= 0.0f || q == 0.0f) {
                  passThroughState();
               } else {
                  if(inputNodeGain == nullptr)
                     return;
                  float gain = inputNodeGain->mWorkBuffers[inputSlotGain][0];

                  float a = powf(10.0f, gain*0.025f);          //square root of gain converted from dB to linear

                  if(freq <= 0.0f) {
                     scaleState(a*a);
                  } else {
                     float w0 = cPI * 2.0f * freq;
                     float ainn = (a + 1.0f / a) * (1.0f / q - 1.0f) + 2.0f;
                     if(ainn < 0)
                        ainn = 0;
                     float alpha = 0.5f * sin(w0) * sqrt(ainn);
                     float k = cos(w0);
                     float k2 = 2.0f * sqrt(a) * alpha;
                     float ap1 = a + 1.0f;
                     float am1 = a - 1.0f;
                     float a0inv = 1.0f / (ap1 - am1 * k + k2);
                     mState.b0 = a0inv * a * (ap1 + am1 * k + k2);
                     mState.b1 = a0inv * -2.0f * a * (am1 + ap1 * k);
                     mState.b2 = a0inv * a * (ap1 + am1 * k - k2);
                     mState.a1 = a0inv * 2.0f * (am1 - ap1 * k);
                     mState.a2 = a0inv * (ap1 - am1 * k - k2);
                  }
               }
               processBiquad(numberSamples);               
            }
            break;
         default:
            break;
         }
      }
   }
};

}; //namespace ImGui

#endif   //#ifndef __NODES_BIQUADFILTER_H__
