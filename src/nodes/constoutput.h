
#ifndef __NODES_CONSTOUTPUT_H__
#define __NODES_CONSTOUTPUT_H__

namespace ImGui {

class ConstOutputNode : public ProcessingNode {
protected:
   ConstOutputNode() : ProcessingNode(1) {}
   static const int TYPE = SNT_CONST_OUTPUT;

   const char* getTooltip() const {return "Fixed value.";}
   const char* getInfo() const {return "Fixed value.\n\nThis is supposed to display some info about this node.";}

   float mValue;

public:
   static ConstOutputNode* Create(const ImVec2& pos) {
      ConstOutputNode* node = (ConstOutputNode*) ImGui::MemAlloc(sizeof(ConstOutputNode));
      IM_PLACEMENT_NEW (node) ConstOutputNode();

      node->init("Const Input",pos,"","value",TYPE);
      node->fields.addField(&node->mValue,1,"value", "", 3, -100000, 100000);

      return node;
   }

   void renderSamples(int numberSamples) {
      for(int i=0; i<numberSamples; ++i) {
         mWorkBuffers[0][i] = mValue;
      }
   }
};

}; //namespace ImGui

#endif   //#ifndef __NODES_CONSTINPUT_H__
