
#ifndef __NODES_DUMBDELAY_H__
#define __NODES_DUMBDELAY_H__

#include "src/utils.h"
#include <math.h>

namespace ImGui {

class DumbDelayNode : public ProcessingNode {
protected:
   DumbDelayNode() : ProcessingNode(1) {}
   static const int TYPE = SNT_DUMBDELAY;

   const char* getTooltip() const {return "DumbDelay";}
   const char* getInfo() const {return "really simple delay\n\n";}

   //parameters:
   int mSize;
   float mFeedback;     //(0-1]

   //state:
   float *mDelayBuffer;
   int mIndexIntoDelayBuffer;

   static const int cMaxSize = 10000;

public:
   ~DumbDelayNode() {
      delete[] mDelayBuffer;
   }

   static DumbDelayNode* Create(const ImVec2& pos) {
      DumbDelayNode* node = (DumbDelayNode*) ImGui::MemAlloc(sizeof(DumbDelayNode));
      IM_PLACEMENT_NEW (node) DumbDelayNode();

      node->mDelayBuffer = new float[cMaxSize];

      node->init("Generator", pos, "snd", "snd", TYPE);
      node->fields.addField(&node->mFeedback, 1, "Feedback", nullptr, 3, 0.0f, 1.0f);
      node->fields.addField(&node->mSize, 1, "Amp", nullptr, 3, 0, cMaxSize);
      node->mFeedback = 0.25f;
      node->mSize = 1000;
      node->mIndexIntoDelayBuffer = 0;

      return node;
   }

   void renderSamples(int numberSamples) {
      if (mInputNodes.size() != 0) {
         assert(mInputNodes.size() == 1);

         ProcessingNode *inputNode = mInputNodes[0].node;
         int inputSlot = mInputNodes[0].slot;
         if(inputNode == nullptr)
            return;

         for(int i=0; i<numberSamples; ++i) {
            float val = inputNode->mWorkBuffers[inputSlot][i] + mDelayBuffer[mIndexIntoDelayBuffer];
            mDelayBuffer[mIndexIntoDelayBuffer] = val * mFeedback;
            mIndexIntoDelayBuffer += 1;
            if(mIndexIntoDelayBuffer >= mSize)
               mIndexIntoDelayBuffer = 0;
            mWorkBuffers[0][i] = val;
         }
      }
   }

 };

}; //namespace ImGui

#endif   //#ifndef __NODES_DUMBDELAY_H__
