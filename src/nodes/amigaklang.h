
/* These Amiga-Klang generators does NOT work like in AmigaKlang itself because
 * in AmigaKlang first based on the nodes samples are generated and afterwards
 * these samples are played with different frequencies to get the according notes
 * and not each sample is generated with the right pitch */

#ifndef __NODES_AMIGAKLANG_H__
#define __NODES_AMIGAKLANG_H__

#include "src/utils.h"
#include <math.h>

namespace ImGui {

/* Nodes available:
	ConvToFloat
	Volume
	OscSine
	OscSaw
	Add
 	Mul
 	EnvDecay
  	SVFilter
*/

/*
0, 0, 0, 0, 0, 0, 0, 0
$ JosSs_Lead_01, 12304, 6152, 6152, Y
#
v1 = osc_saw(0, 1310, 127);
v2 = envd(1, 11, 0, 127);
v1 = mul(v1, v2);
v1 = sv_flt_n(3, v1, 85, 63, 0);
v1 = reverb(v1, 85, 53);
v3 = osc_saw(6, 1306, 11);
v2 = osc_sine(7, v3, 26);
v2 = sv_flt_n(8, v2, 88, 9, 0);
v1 = add(v1, v2);

$ JosSs_Chord_A_02_(Lead_01), 5238, 4392, 846, N
#
v1 = chordgen(0, 0, 4, 7, 12, 0);
v2 = envd(1, 12, 0, 127);
v1 = mul(v1, v2);

$ JosSs_Chord_A_03_(Lead_01), 5238, 4392, 846, N
#
v1 = chordgen(0, 0, 3, 5, 8, 0);
v2 = envd(1, 12, 0, 127);
v1 = mul(v1, v2);

$ JosSs_Lead_02, 34952, 17475, 17477, Y
#
v1 = osc_sine(0, 1316, 127);
v2 = osc_saw(1, 1310, 98);
v3 = osc_sine(2, 1322, 110);
v1 = add(v1, v2);
v1 = add(v1, v3);
v1 = sv_flt_n(6, v1, 85, 110, 2);
v1 = reverb(v1, 24, 59);

$ JosSs_Chord_B_01_(Lead_02), 2304, 2264, 40, N
#
v1 = chordgen(0, 3, 4, 7, 10, 0);
v2 = envd(1, 8, 0, 127);
v2 = mul(v2, 96);
v1 = sv_flt_n(4, v1, v2, 99, 2);

$ JosSs_Chord_B_02_(Lead_02), 2304, 2264, 40, N
#
v1 = chordgen(0, 3, 3, 5, 8, 0);
v2 = envd(1, 8, 0, 127);
v2 = mul(v2, 96);
v1 = sv_flt_n(4, v1, v2, 99, 2);

$ JosSs_Chord_B_03_(Lead_02), 2304, 2264, 40, N
#
v1 = chordgen(0, 3, 3, 5, 10, 0);
v2 = envd(1, 8, 0, 127);
v2 = mul(v2, 96);
v1 = sv_flt_n(4, v1, v2, 99, 2);

$ JosSs_BD_01, 1788, 1786, 2, N
#
v2 = envd(0, 7, 0, 127);
v2 = mul(v2, 830);
v1 = osc_sine(2, v2, 127);
v2 = envd(4, 1, 0, 127);
v2 = mul(v2, 128);
v3 = osc_saw(6, 2127, v2);
v1 = add(v1, v3);

$ JosSs_Claps_01, 1396, 768, 628, N
#
v2 = envd(0, 6, 0, 101);
v2 = mul(v2, 528);
v1 = osc_noise(v2);
v1 = sv_flt_n(5, v1, 64, 35, 1);

$ JosSs_Hh_01, 2256, 1538, 718, N
#
v2 = envd(0, 8, 0, 60);
v2 = mul(v2, 128);
v1 = osc_noise(v2);
v1 = sv_flt_n(5, v1, 122, 127, 1);

$ JosSs_Snare_01, 1728, 1470, 258, N
#
v3 = envd(0, 7, 16, 127);
v3 = mul(v3, 128);
v4 = envd(2, 8, 10, 127);
v1 = osc_saw(3, v2, v3);
v1 = sv_flt_n(5, v1, 12, 8, 1);
v1 = add(v1, v3);
v2 = add(v1, -12823);
v1 = mul(v1, v4);

$ JosSs_BD_01 + Snare_01, 1818, 1752, 66, N
#
v1 = clone(smp,7, 0);
v2 = clone(smp,10, 0);
v1 = add(v1, v2);

$ JosSs_Bass_01, 10310, 5698, 4612, Y
#
v1 = osc_sine(0, 1310, 127);
v2 = osc_sine(1, 1318, 127);
v1 = mul(v1, v2);
v1 = cmb_flt_n(3, v1, v2, 72, 94);
v2 = osc_saw(4, 1309, 85);
v1 = add(v1, v2);

$ JosSs_Chord_Loop_C_01_(Bass_01), 5566, 2784, 2782, Y
#
v1 = chordgen(0, 12, 4, 7, 10, 127);
v2 = enva(1, 20, 0, 127);
v1 = mul(v1, v2);
v1 = reverb(v1, 6, 23);
v1 = sv_flt_n(4, v1, 28, 20, 0);

$ JosSs_Chord_Loop_C_02_(Bass_01), 5566, 2782, 2784, Y
#
v1 = chordgen(0, 12, 3, 5, 8, 116);
v2 = enva(1, 20, 0, 127);
v1 = mul(v1, v2);
v1 = reverb(v1, 6, 27);
v1 = sv_flt_n(4, v1, 28, 20, 0);

$ JosSs_Chord_Loop_C_03_(Bass_01), 5566, 2782, 2784, Y
#
v1 = chordgen(0, 12, 4, 7, 12, 116);
v2 = enva(1, 20, 0, 127);
v1 = mul(v1, v2);
v1 = reverb(v1, 6, 29);
v1 = sv_flt_n(4, v1, 28, 20, 0);

$ JosSs_303_01, 510, 508, 2, N
#
v2 = envd(0, 3, 0, 127);
v1 = osc_saw(1, 1310, 127);
v3 = mul(v2, -288);
v1 = sv_flt_n(3, v1, v3, 0, 2);
v1 = mul(v2, v1);

$ JosSs_303_02, 510, 508, 2, N
#
v2 = envd(0, 3, 0, 127);
v1 = osc_saw(1, 1310, 127);
v3 = mul(v2, -81);
v1 = sv_flt_n(3, v1, v3, 0, 2);
v1 = mul(v2, v1);

$ JosSs_303_03, 510, 508, 2, N
#
v2 = envd(0, 3, 0, 127);
v1 = osc_saw(1, 1310, 127);
v3 = mul(v2, 96);
v1 = sv_flt_n(3, v1, v3, 0, 2);
v1 = mul(v2, v1);

$ JosSs_Lead_05, 36032, 18015, 18017, Y
#
v2 = envd(0, 52, 0, 127);
v4 = mul(v2, 128);
v3 = osc_saw(3, 1310, 127);
v1 = osc_saw(5, 1313, 127);
v1 = add(v3, v1);
v3 = osc_saw(7, 1307, 127);
v1 = add(v1, v3);
v1 = sv_flt_n(10, v1, v4, 13, 0);

$ JosSs_Bass_05_02, 10310, 5698, 4612, Y
#
v1 = osc_sine(0, 650, 127);
v2 = osc_sine(1, 658, 127);
v1 = mul(v1, v2);
v1 = cmb_flt_n(4, v1, v2, 33, 127);
v2 = osc_saw(6, 642, 85);
v1 = add(v1, v2);
v1 = sv_flt_n(10, v1, 10, 127, 0);

 */



//Clamps a signal without wrapping
inline float static clampF (float val) {
	val = val >  32767 ?  32767:val; 
	val = val < -32768 ? -32768:val; 
	return val;
}


class AKConvToFloatNode : public ProcessingNode {
protected:
   AKConvToFloatNode() : ProcessingNode(1) {}	//number of working buffers
   static const int TYPE = SNT_AK_CONV_TO_FLOAT;

   const char* getTooltip() const {return "AmigaKlang:ConvToFloat";}
   const char* getInfo() const {return "AmigaKlang: Converts AK-Buffers(16-bit) to floats";}

   //parameters:

   //state:

public:
  static AKConvToFloatNode* Create(const ImVec2& pos) {
    AKConvToFloatNode* node = (AKConvToFloatNode*) ImGui::MemAlloc(sizeof(AKConvToFloatNode));
    IM_PLACEMENT_NEW (node) AKConvToFloatNode();
  
    //init(const char* name, const ImVec2& pos,const char* inputSlotNamesSeparatedBySemicolons=NULL,const char* outputSlotNamesSeparatedBySemicolons=NULL,int _nodeTypeID=0/*,float currentWindowFontScale=-1.f*/);
    node->init("AK:ConvToFloat",pos,"snd","conv",TYPE);

    return node;
  }

  void renderSamples(int numberSamples) {
    if (mInputNodes.size() == 1) {

			ProcessingNode *sndNode;
			int sndSlot;
	   	if(!getInputNodeSlot(0, &sndNode, &sndSlot))
      		return;

	    for(int i=0; i<numberSamples; ++i) {
	    	mWorkBuffers[0][i] = sndNode->mWorkBuffers[sndSlot][i] / 32767.0f;
	    }
		}
  }
};


class AKVolumeNode : public ProcessingNode {
protected:
   AKVolumeNode() : ProcessingNode(1) {}	//number of working buffers
   static const int TYPE = SNT_AK_VOLUME;

   const char* getTooltip() const {return "AmigaKlang:Volume";}
   const char* getInfo() const {return "AmigaKlang: Volume";}

   //parameters:

   //state:

public:
  static AKVolumeNode* Create(const ImVec2& pos) {
    AKVolumeNode* node = (AKVolumeNode*) ImGui::MemAlloc(sizeof(AKVolumeNode));
    IM_PLACEMENT_NEW (node) AKVolumeNode();
  
    //init(const char* name, const ImVec2& pos,const char* inputSlotNamesSeparatedBySemicolons=NULL,const char* outputSlotNamesSeparatedBySemicolons=NULL,int _nodeTypeID=0/*,float currentWindowFontScale=-1.f*/);
    node->init("AK:Volume",pos,"snd;gain","snd",TYPE);

    return node;
  }

  void renderSamples(int numberSamples) {
      if (mInputNodes.size() == 2) {

		ProcessingNode *sndNode, *gainNode;
		int sndSlot, gainSlot;
      	if(!getInputNodeSlot(0, &sndNode, &sndSlot))
      		return;
      	if(!getInputNodeSlot(1, &gainNode, &gainSlot))
      		return;

	    for(int i=0; i<numberSamples; ++i) {
	    	mWorkBuffers[0][i] = (sndNode->mWorkBuffers[sndSlot][i] * gainNode->mWorkBuffers[gainSlot][i]) / 128.0f;
	    }
	}
  }
};


class AKOscSineNode : public ProcessingNode {
protected:
   AKOscSineNode() : ProcessingNode(1) {}	//number of working buffers
   static const int TYPE = SNT_AK_OSC_SINE;

   const char* getTooltip() const {return "AmigaKlang:OscSine";}
   const char* getInfo() const {return "AmigaKlang: OscSine";}

   //parameters:

   //state:
   short counter = 0;

public:
  static AKOscSineNode* Create(const ImVec2& pos) {
    AKOscSineNode* node = (AKOscSineNode*) ImGui::MemAlloc(sizeof(AKOscSineNode));
    IM_PLACEMENT_NEW (node) AKOscSineNode();
  
    //init(const char* name, const ImVec2& pos,const char* inputSlotNamesSeparatedBySemicolons=NULL,const char* outputSlotNamesSeparatedBySemicolons=NULL,int _nodeTypeID=0/*,float currentWindowFontScale=-1.f*/);
    node->init("AK:OscSine",pos,"freq;gain","snd",TYPE);

    return node;
  }

  void renderSamples(int numberSamples) {
    if (mInputNodes.size() == 2) {

			ProcessingNode *freqNode, *gainNode;
			int freqSlot, gainSlot;
    	if(!getInputNodeSlot(0, &freqNode, &freqSlot))
    		return;
    	if(!getInputNodeSlot(1, &gainNode, &gainSlot))
    		return;

    	float phaseStepFloat = freqNode->mWorkBuffers[freqSlot][0];
    	short phaseStep = (short)(32767.0*phaseStepFloat / cSampleRate);

	    for(int i=0; i<numberSamples; ++i) {
	    	counter += phaseStep;
	    	short buf = counter;
	    	buf -= 16384;

	    	short abs = buf < 0 ? -buf:buf; 

	    	int temp = (buf*(32767-abs));
	    	short res = temp >> 16;
	    	res <<= 3;

	    	mWorkBuffers[0][i] = (res * gainNode->mWorkBuffers[gainSlot][i]) / 128.0f;
	    }
		}
  }
};

class AKOscSawNode : public ProcessingNode {
protected:
   AKOscSawNode() : ProcessingNode(1) {}	//number of working buffers
   static const int TYPE = SNT_AK_OSC_SAW;

   const char* getTooltip() const {return "AmigaKlang:OscSaw";}
   const char* getInfo() const {return "AmigaKlang: OscSaw";}

   //parameters:

   //state:
   short counter = 0;

public:
  static AKOscSawNode* Create(const ImVec2& pos) {
    AKOscSawNode* node = (AKOscSawNode*) ImGui::MemAlloc(sizeof(AKOscSawNode));
    IM_PLACEMENT_NEW (node) AKOscSawNode();
  
    //init(const char* name, const ImVec2& pos,const char* inputSlotNamesSeparatedBySemicolons=NULL,const char* outputSlotNamesSeparatedBySemicolons=NULL,int _nodeTypeID=0/*,float currentWindowFontScale=-1.f*/);
    node->init("AK:OscSaw",pos,"freq;gain","snd",TYPE);

    return node;
  }

  void renderSamples(int numberSamples) {
      if (mInputNodes.size() == 2) {

		ProcessingNode *freqNode, *gainNode;
		int freqSlot, gainSlot;
      	if(!getInputNodeSlot(0, &freqNode, &freqSlot))
      		return;
      	if(!getInputNodeSlot(1, &gainNode, &gainSlot))
      		return;

    	float phaseStepFloat = freqNode->mWorkBuffers[freqSlot][0];
    	short phaseStep = (short)(65536.0*phaseStepFloat / cSampleRate);

	    for(int i=0; i<numberSamples; ++i) {
	    	counter += phaseStep;

				float tmp = (float)counter;
				float gain = gainNode->mWorkBuffers[gainSlot][i];
				tmp = tmp * gain;
				tmp = tmp / 128.0f;
	    	mWorkBuffers[0][i] = tmp;
	    }
	}
  }
};

class AKAddNode : public ProcessingNode {
protected:
   AKAddNode() : ProcessingNode(1) {}	//number of working buffers
   static const int TYPE = SNT_AK_ADD;

   const char* getTooltip() const {return "AmigaKlang:Add";}
   const char* getInfo() const {return "AmigaKlang: Add";}

   //parameters:

   //state:

public:
  static AKAddNode* Create(const ImVec2& pos) {
    AKAddNode* node = (AKAddNode*) ImGui::MemAlloc(sizeof(AKAddNode));
    IM_PLACEMENT_NEW (node) AKAddNode();
  
    //init(const char* name, const ImVec2& pos,const char* inputSlotNamesSeparatedBySemicolons=NULL,const char* outputSlotNamesSeparatedBySemicolons=NULL,int _nodeTypeID=0/*,float currentWindowFontScale=-1.f*/);
    node->init("AK:Add",pos,"snd1;snd2","snd",TYPE);

    return node;
  }

  void renderSamples(int numberSamples) {
      if (mInputNodes.size() == 2) {
		ProcessingNode *val1Node, *val2Node;
		int val1Slot, val2Slot;
      	if(!getInputNodeSlot(0, &val1Node, &val1Slot))
      		return;
      	if(!getInputNodeSlot(1, &val2Node, &val2Slot))
      		return;

	    for(int i=0; i<numberSamples; ++i) {
	    	float val = val1Node->mWorkBuffers[val1Slot][i] + val2Node->mWorkBuffers[val2Slot][i];
	    	val = val >  32767 ?  32767 : val;
	    	val = val < -32768 ? -32768 : val;
	    	mWorkBuffers[0][i] = val;
	    }
	}
  }
};

class AKMulNode : public ProcessingNode {
protected:
   AKMulNode() : ProcessingNode(1) {}	//number of working buffers
   static const int TYPE = SNT_AK_MUL;

   const char* getTooltip() const {return "AmigaKlang:Mul";}
   const char* getInfo() const {return "AmigaKlang: Mul";}

   //parameters:

   //state:

public:
  static AKMulNode* Create(const ImVec2& pos) {
    AKMulNode* node = (AKMulNode*) ImGui::MemAlloc(sizeof(AKMulNode));
    IM_PLACEMENT_NEW (node) AKMulNode();
  
    //init(const char* name, const ImVec2& pos,const char* inputSlotNamesSeparatedBySemicolons=NULL,const char* outputSlotNamesSeparatedBySemicolons=NULL,int _nodeTypeID=0/*,float currentWindowFontScale=-1.f*/);
    node->init("AK:Mul",pos,"snd1;snd2","snd",TYPE);

    return node;
  }

  void renderSamples(int numberSamples) {
      if (mInputNodes.size() == 2) {

		ProcessingNode *val1Node, *val2Node;
		int val1Slot, val2Slot;
      	if(!getInputNodeSlot(0, &val1Node, &val1Slot))
      		return;
      	if(!getInputNodeSlot(1, &val2Node, &val2Slot))
      		return;

	    for(int i=0; i<numberSamples; ++i) {
	    	mWorkBuffers[0][i] = (val1Node->mWorkBuffers[val1Slot][i] * val2Node->mWorkBuffers[val2Slot][i]) / 32768.0f;
	    }
	}
  }
};

static short decayTable[128] = { 32767, 32767, 32767, 16384, 10922, 8192, 6553, 4681, 3640, 2978, 2520, 2048, 1724, 1489, 1310, 1129, 992,
885, 799, 712, 642, 585, 537, 489, 448, 414, 385, 356, 330, 309, 289, 270, 254, 239, 225, 212, 201, 190, 181, 171, 163, 155, 148, 141, 134,
129, 123, 118, 113, 108, 104, 100, 96, 93, 89, 86, 83, 80, 77, 75, 72, 70, 68, 65, 63, 61, 60, 58, 56, 54, 53, 51, 50, 49, 47, 46, 45, 44,
43, 41, 40, 39, 38, 38, 37, 36, 35, 34, 33, 33, 32, 31, 30, 30, 29, 29, 28, 27, 27, 26, 26, 25, 25, 24, 24, 23, 23, 22, 22, 22, 21, 21, 20,
20, 20, 19, 19, 19, 18, 18, 18, 17, 17, 17, 17, 16, 16, 16 };


class AKEnvDecayNode : public ProcessingNode {
protected:
   AKEnvDecayNode() : ProcessingNode(1) {}	//number of working buffers
   static const int TYPE = SNT_AK_ENV_DECAY;

   const char* getTooltip() const {return "AmigaKlang:EnvDecay";}
   const char* getInfo() const {return "AmigaKlang: EnvDecay";}

   //parameters:

   //state:
   int sampleCounter = 0;

public:
  static AKEnvDecayNode* Create(const ImVec2& pos) {
    AKEnvDecayNode* node = (AKEnvDecayNode*) ImGui::MemAlloc(sizeof(AKEnvDecayNode));
    IM_PLACEMENT_NEW (node) AKEnvDecayNode();
  
    //init(const char* name, const ImVec2& pos,const char* inputSlotNamesSeparatedBySemicolons=NULL,const char* outputSlotNamesSeparatedBySemicolons=NULL,int _nodeTypeID=0/*,float currentWindowFontScale=-1.f*/);
    node->init("AK:EnvDecay",pos,"decay;sustain;gain","snd",TYPE);

    return node;
  }

  void noteOn() {
  	sampleCounter = 0;
  }

  void renderSamples(int numberSamples) {
      if (mInputNodes.size() == 2) {

		ProcessingNode *decayNode, *sustainNode, *gainNode;
		int decaySlot, sustainSlot, gainSlot;
      	if(!getInputNodeSlot(0, &decayNode, &decaySlot))
      		return;
      	if(!getInputNodeSlot(1, &sustainNode, &sustainSlot))
      		return;
      	if(!getInputNodeSlot(2, &gainNode, &gainSlot))
      		return;

	    for(int i=0; i<numberSamples; ++i) {

	    	short sustain16 = sustainNode->mWorkBuffers[sustainSlot][i] * 256.0f;
	    	uint8_t decay = (uint8_t)decayNode->mWorkBuffers[decaySlot][i];

	    	if(decay > 127)				//table has only 128 values!
	    		decay = 127;

	    	short t = decayTable[decay];
	    	int buf = 32767-((sampleCounter*t)/256);
	    	if(buf <= sustain16)
	    		buf = sustain16;
	    	mWorkBuffers[0][i] = (buf * gainNode->mWorkBuffers[gainSlot][i])/128.0f;
	    }
	}
  }
};


class AKSVFilterNode : public ProcessingNode {		//StateVariableFilter
protected:
   AKSVFilterNode() : ProcessingNode(1) {}	//number of working buffers
   static const int TYPE = SNT_AK_SVFILTER;

   const char* getTooltip() const {return "AmigaKlang:SVFilter";}
   const char* getInfo() const {return "AmigaKlang: SVFilter";}

   //parameters:
   static bool getTextFromEnumIndex(void* ,int value,const char** pTxt) {
      if(!pTxt) 
         return false;
      static const char* values[] = {"Lowpass", "Highpass", "Bandpass", "Notch"};
      static int numValues = (int)(sizeof(values)/sizeof(values[0]));
      if(value>=0 && value<numValues) 
         *pTxt = values[value];
      else 
         *pTxt = "UNKNOWN";
      return true;
   }

   //state:
   int mFilterType;

public:
  static AKSVFilterNode* Create(const ImVec2& pos) {
    AKSVFilterNode* node = (AKSVFilterNode*) ImGui::MemAlloc(sizeof(AKSVFilterNode));
    IM_PLACEMENT_NEW (node) AKSVFilterNode();
  
    //init(const char* name, const ImVec2& pos,const char* inputSlotNamesSeparatedBySemicolons=NULL,const char* outputSlotNamesSeparatedBySemicolons=NULL,int _nodeTypeID=0/*,float currentWindowFontScale=-1.f*/);
    node->init("AK:SVFilter",pos,"cutoff;resonance;snd","snd",TYPE);
    node->fields.addFieldEnum(&node->mFilterType,4,&getTextFromEnumIndex,"Type","Choose your favourite");

    return node;
  }

  float mLpfBuffer, mHpfBuffer, mBpfBuffer;

  void renderSamples(int numberSamples) {
      if (mInputNodes.size() == 3) {
		ProcessingNode *val1Node, *val2Node, *val3Node;
		int val1Slot, val2Slot, val3Slot;
      	if(!getInputNodeSlot(0, &val1Node, &val1Slot))
      		return;
      	if(!getInputNodeSlot(1, &val2Node, &val2Slot))
      		return;
      	if(!getInputNodeSlot(2, &val3Node, &val3Slot))
      		return;

	    for(int i=0; i<numberSamples; ++i) {
	    	float cutoff = val1Node->mWorkBuffers[val1Slot][i];
	    	float resonance = val2Node->mWorkBuffers[val1Slot][i];
	    	float snd = val3Node->mWorkBuffers[val1Slot][i];

	    	float lpf = mLpfBuffer;
	    	float hpf = mHpfBuffer;
	    	float bpf = mBpfBuffer;

	    	lpf = clampF(lpf + (bpf/128.0f)*cutoff);
	    	hpf = clampF(snd - lpf - (bpf/128.0f)*resonance);
	    	bpf = clampF(bpf + (hpf/128.0f)*cutoff);

	    	mLpfBuffer = lpf;
	    	mHpfBuffer = hpf;
	    	mBpfBuffer = bpf;

	    	switch(mFilterType) {
    		case 0: mWorkBuffers[0][i] = lpf; break;
    		case 1: mWorkBuffers[0][i] = hpf; break;
    		case 2: mWorkBuffers[0][i] = bpf; break;
    		case 3: mWorkBuffers[0][i] = hpf+bpf; break;
	    	}
	    }
	}
  }
};


}; //namespace ImGui

#endif   //#ifndef __NODES_NOISEGENERATOR_H__



#if 0

//Examples:

// V1 - Chord Gen(Sample 1, Note1 2, Note2 3, Note3 12, Shift Value 110)
// V2 - Osc Sin (Frequence Val 335, Gain Val 42)
// V1 - Add (Value1 V1, Value2 V2)


// V1 - Osc Saw (Freq: 1310, Gain: 92)
// V2 - Env Decay (Decay: 8, Sustain: 0, Gain: 127)
// V1 - Multiply (V1, V2)
// V1 - SV Filter (V1, Cutoff: 57, Resonance: 53, Mode:LowPass)
// V1 - Reverb (V1, Feedback: 109, Gain:39)

static short decayTable[128] = { 32767, 32767, 32767, 16384, 10922, 8192, 6553, 4681, 3640, 2978, 2520, 2048, 1724, 1489, 1310, 1129, 992,
885, 799, 712, 642, 585, 537, 489, 448, 414, 385, 356, 330, 309, 289, 270, 254, 239, 225, 212, 201, 190, 181, 171, 163, 155, 148, 141, 134,
129, 123, 118, 113, 108, 104, 100, 96, 93, 89, 86, 83, 80, 77, 75, 72, 70, 68, 65, 63, 61, 60, 58, 56, 54, 53, 51, 50, 49, 47, 46, 45, 44,
43, 41, 40, 39, 38, 38, 37, 36, 35, 34, 33, 33, 32, 31, 30, 30, 29, 29, 28, 27, 27, 26, 26, 25, 25, 24, 24, 23, 23, 22, 22, 22, 21, 21, 20,
20, 20, 19, 19, 19, 18, 18, 18, 17, 17, 17, 17, 16, 16, 16 };

static short counter_sh[16];
static short buffer_sh[16];

static short buffern[2048*16];


// from AmigaKlang which is used for very size resticted intros which means
// no clean handling of stuff :/

void initBuffers(){
	for (short l=0;l<16;l++) {
		for (short j=0;j<2048;j++) {
	 		buffern[l][j] = 0;
		} 
		buffer_sh[l]=0;		//sample&hold buffer
		counter_sh[l]=0;	//sample&hold counter
	}
}


short abs(short val) {
	val = val < 0 ? -val:val; 
	return val;
}

//Clamps a signal without wrapping
inline short clamp (int val){
	val = val >  32767 ?  32767:val; 
	val = val < -32768 ? -32768:val; 
	return val;
}

//Volume: will scale the amplitude of a signal
//gain: 0..127
//output: 16bit (-32768..32767)
inline short vol(short val, BYTE gain){
	return mulsw(val, gain) >> 7;
}

//Sawtooth Oscillator
//frequency: 0..10000
//gain: 0..127
short osc_saw (BYTE instance, short freq, BYTE gain){
	static short counter[16]; 
	counter[instance]+=freq; 
	return vol(counter[instance],gain);		
}

//sample & hold (reduces the sampling frequency of a signal)
//step: 0..127
short sh(BYTE instance, short val1, BYTE step){
	short step2 = (step*step) >> 2;
	if (counter_sh[instance] == 0) 
		buffer_sh[instance] = val1;
	if (counter_sh[instance] == step2) 
		counter_sh[instance] = -1; 
	counter_sh[instance] += 1;
	return buffer_sh[instance];
}

//Triangle Oscillator
short osc_tri (BYTE instance, short freq, BYTE gain){
	static short counter[16]; 
	short buf = counter[instance]+=freq; 
	if (buf < 0) 
		buf = 65535 - buf;		
	buf -= 16384; 
	buf <<= 1; 
	return vol(buf,gain);								
}

//Sine Oscillator
short osc_sine (BYTE instance, short freq, BYTE gain){
	static short counter[16];
	counter[instance]+=freq;
	short buf = counter[instance];				[-32767..32767]
	buf -= 16384;
	int temp = mulsw(buf, 32767-abs(buf));
	short res = temp>>16;
	res<<=3;
	return vol(res,gain);								
}

//Pulse (rectangle) oscillator
short osc_pulse (BYTE instance, short freq, BYTE gain, BYTE dutycycle){
	static short counter[16]; 
	short buf = counter[instance]+=freq; 	
	if (buf<(dutycycle-63)<<9) 
		buf= -32768;	
	else 
		buf = 32767; 
	return vol(buf,gain);							
}

//White Noise Oscillator
short osc_noise (int sample, BYTE gain) {
	static int g_x1 = 0x67452301; 
	static int g_x2 = 0xefcdab89; 
	static int g_x3 = 0;			
 	
 	g_x1 ^= g_x2; 
 	g_x3 += g_x2; 
 	g_x2 += g_x1; 
 	short buf = g_x3; 
	return vol(buf,gain);	 
}

//Env Attack (simple attack envelope)
short enva (int sample, BYTE attack, BYTE sustain, BYTE gain){
   	short t= decayTable[attack]; 
   	int buf= mulsw(sample, t) >> 8;
   	if (buf>32767) 
   		buf = 32767; 
	return vol(buf,gain);
}

//Env Decay (simple decay envelope)
short envd (int sample, BYTE decay, BYTE sustain, BYTE gain){
	short sustain16= sustain<<8; 
	short t= decayTable[decay]; 
	int buf= 32767 - (mulsw(sample, t) >> 8);
   	if (buf <= sustain16) 
   		buf= sustain16; 
	return vol(buf,gain); 
}

//add
//output: 16bit(0..32767)
inline short add (short val1, short val2){
	return clamp(val1 + val2);
}

//mul
//output: 16bit(0..32767)
inline short mul (short val1, short val2){
	return mulsw(val1, val2) >> 15;
}

//delay a signal
//delay: 0..2047 samples
short dly_cyc(BYTE instance, short val, short delay, BYTE gain){ 
	static short i[16]; buffern[instance][i[instance]] = vol(val,gain);
	if (++i[instance]>=delay) i[instance]=0; 
	return buffern[instance][i[instance]];	
}

//CombFilter (delay a signal with feedback)
//delay: 0..2047
//feedback: 0..127
short cmb_flt_n (BYTE instance, short val, short delay, BYTE feedback, BYTE gain){
	static short i[16];
	buffern[instance][i[instance]] = add(val, (vol(buffern[instance][i[instance]], feedback)));
	short output = buffern[instance][i[instance]]; 
	if (++i[instance]>=delay) 
		i[instance]=0; 
	return vol(output, gain);
}

//reverb (simple freeverb implementation)
//feedback:0..127
short reverb (short val, BYTE feedback, BYTE gain){
	int buf = cmb_flt_n (0, val, 557, feedback,gain); 
	buf += cmb_flt_n (1, val, 593, feedback,gain);		
	buf += cmb_flt_n (2, val, 641, feedback,gain); 
	buf += cmb_flt_n (3, val, 677, feedback,gain);			 
	buf += cmb_flt_n (4, val, 709, feedback,gain); 
	buf += cmb_flt_n (5, val, 743, feedback,gain);			 
	buf += cmb_flt_n (6, val, 787, feedback,gain); 
	buf += cmb_flt_n (7, val, 809, feedback,gain); 
	return clamp(buf);
}

//CtrlSignal (makes control signal out of 16 bit signals)
inline BYTE ctrl(short val){
	return (val>>9)+64;
}

//SV Filter (state variable filter)
//cutoff: 0..127
//resonance: 0..127
//mode: lowpass, highpass, bandpass, notch
short sv_flt_n (BYTE instance, short val, short cutoff, short resonance, BYTE mode){
   static short filterBuffer[16 * 4]; short* buffer = filterBuffer + (instance << 2);
   short lpf = buffer[0]; short hpf = buffer[1]; short bpf = buffer[2];
   lpf= clamp( lpf + (mulsw(bpf>>7, cutoff)) );
   hpf= clamp( val - lpf - (mulsw(bpf>>7, resonance)) );
   bpf= clamp( bpf + (mulsw(hpf>>7, cutoff)) );
   buffer[0] = lpf; buffer[1] = hpf; buffer[2] = bpf;
   switch (mode) {
      case 0: return lpf; break;
      case 1: return hpf; break;
      case 2: return bpf; break;
      case 3: return hpf + bpf; break;
   }
   return 0;
}

//chord gen(creates a 4-note chord from a previous instrument)
//shift manipulates the offset of the sample
//can't be used on instrument 1
//samplenr: sample number to clone from
//note1: 0..12 (0=none)
//note2: 0..12 (0=none)
//note3: 0..12 (0=none)
//shift: 0..127
short chordgen (int sample, void* BaseAdr, BYTE n1, BYTE n2, BYTE n3, BYTE shift){
	int buf = 							*(BYTE*)(BaseAdr+sample)<<7;				
	if(n1==1 ||n2==1 ||n3==1) 	buf += 	*(BYTE*)(shift+BaseAdr+((mulsw(sample,271))>>8))<<7;			
	if(n1==2 ||n2==2 ||n3==2) 	buf += 	*(BYTE*)(shift+BaseAdr+((mulsw(sample,287))>>8))<<7;
	if(n1==3 ||n2==3 ||n3==3) 	buf += 	*(BYTE*)(shift+BaseAdr+((mulsw(sample,304))>>8))<<7;				
	if(n1==4 ||n2==4 ||n3==4) 	buf += 	*(BYTE*)(shift+BaseAdr+((mulsw(sample,322))>>8))<<7;
	if(n1==5 ||n2==5 ||n3==5) 	buf += 	*(BYTE*)(shift+BaseAdr+((mulsw(sample,342))>>8))<<7;			
	if(n1==6 ||n2==6 ||n3==6) 	buf += 	*(BYTE*)(shift+BaseAdr+((mulsw(sample,362))>>8))<<7;
	if(n1==7 ||n2==7 ||n3==7) 	buf += 	*(BYTE*)(shift+BaseAdr+((mulsw(sample,383))>>8))<<7;
	if(n1==8 ||n2==8 ||n3==8) 	buf += 	*(BYTE*)(shift+BaseAdr+((mulsw(sample,203))>>7))<<7;				
	if(n1==9 ||n2==9 ||n3==9) 	buf += 	*(BYTE*)(shift+BaseAdr+((mulsw(sample,215))>>7))<<7;				
	if(n1==10||n2==10||n3==10) 	buf += 	*(BYTE*)(shift+BaseAdr+((mulsw(sample,228))>>7))<<7;			
	if(n1==11||n2==11||n3==11) 	buf += 	*(BYTE*)(shift+BaseAdr+((mulsw(sample,483))>>8))<<7;
	if(n1==12||n2==12||n3==12) 	buf += 	*(BYTE*)(shift+BaseAdr+(sample<<1))<<7;			
	return clamp(buf);
}	

//LoopGen (loop generator generates a smooth crossfade loop)
//repeat offset: 0..65536
//repeat length: 2..65536
void loopgen(WORD repeat_length, WORD repeat_offset, void* BaseAdr) {
	int smp;
	BYTE* src1 = BaseAdr + repeat_offset;
	BYTE* src2 = BaseAdr + repeat_offset - repeat_length;
	int delta = divsw((32767 << 8), repeat_length);
	int rampup = 0;
	int rampdown = 32767<<8;
	for (smp = 0; smp<repeat_length; smp++)	{
		short a = (rampup >> 8);  
		short b = (rampdown >> 8);	
		BYTE s1 = src1[smp];
		BYTE s2 = src2[smp];
		BYTE blend = (mulsw(s1, b) + mulsw(s2, a)) >> 15;
		src1[smp] = blend;
		rampup += delta;
		rampdown -= delta;
	}
}

#endif
