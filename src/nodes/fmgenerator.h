
#ifndef __NODES_FMGENERATOR_H__
#define __NODES_FMGENERATOR_H__

#include "src/utils.h"
#include <math.h>

namespace ImGui {

class FMGeneratorNode : public ProcessingNode {
protected:
   FMGeneratorNode() : ProcessingNode(1) {}
   static const int TYPE = SNT_FMGENERATOR;

   const char* getTooltip() const {return "FMGenerator";}
   const char* getInfo() const {return "FMGenerator info.\n\nAlways aliased waveforms, but frequency changes every sample supported.";}

   static bool getTextFromEnumIndex(void* ,int value,const char** pTxt) {
      if(!pTxt) 
         return false;
      static const char* values[] = {"SIN", "SQUARE", "SAW", "TRIANGLE"};
      static int numValues = (int)(sizeof(values)/sizeof(values[0]));
      if(value>=0 && value<numValues) 
         *pTxt = values[value];
      else 
         *pTxt = "UNKNOWN";
      return true;
   }

   //parameters:
   int mGeneratorType;
   float mFreqMul;
   float mAmplitude;

   //state:
   float phase;

public:
   static FMGeneratorNode* Create(const ImVec2& pos) {
      FMGeneratorNode* node = (FMGeneratorNode*) ImGui::MemAlloc(sizeof(FMGeneratorNode));
      IM_PLACEMENT_NEW (node) FMGeneratorNode();
    
      node->init("FMGenerator",pos,"freq","snd",TYPE);
      node->fields.addFieldEnum(&node->mGeneratorType,4,&getTextFromEnumIndex,"Type","Choose your favourite");
      node->fields.addField(&node->mFreqMul, 1, "FreqMul", nullptr, 3, 0.00001f, 1000.0f);
      node->fields.addField(&node->mAmplitude, 1, "Amp", nullptr, 3, 0.0f, 1000.0f);
      node->mGeneratorType = 0;
      node->mFreqMul = 1.0f;
      node->mAmplitude = 0.2f;

      return node;
   }

   void renderSamples(int numberSamples) {
      if(mInputNodes.size() != 0) {
         assert(mInputNodes.size() == 1);

         ProcessingNode *inputNode = mInputNodes[0].node;
         int inputSlot = mInputNodes[0].slot;
         if(inputNode == nullptr)
            return;

         switch(mGeneratorType) {
         case 0:              //sin
             for(int i=0; i<numberSamples; ++i) {
              float freq = inputNode->mWorkBuffers[inputSlot][i] * mFreqMul;
              float phaseStep = 2*cPI*freq/cSampleRate;
              float value = sin(phase) * mAmplitude;
              mWorkBuffers[0][i] = value;
              phase += phaseStep;
             }
             //renormalize phase
             while(phase >= 2*cPI)
                 phase -= 2*cPI;
             while(phase < 0)
                 phase += 2*cPI;

            /*
             float phaseStep = 2*cPI*frequency/cSampleRate;
             for(int i=0; i<numberSamples; ++i) {
                 float value = sin(state.phase) * mAmplitude;
                 operationPolicy::op(value, destBuffer);
                 destBuffer += 1;
                 operationPolicy::op(value, destBuffer);
                 destBuffer += 1;

                 state.phase += phaseStep;
             }    */
            break;
         case 1:              //square
            //with aliasing:
            //TODO: can be heavily optimized
            for(int i=0; i<numberSamples; ++i) {
               float freq = inputNode->mWorkBuffers[inputSlot][i] * mFreqMul;
               float phaseStep = freq / cSampleRate;
               while(phase > 1.0f)
                  phase -= 1.0f;
               while(phase<0.0f)
                  phase += 1.0f;

               float val;
               if(phase <= 0.5f)
                  val = -mAmplitude;
               else
                  val = mAmplitude;

               mWorkBuffers[0][i] = val;
               phase += phaseStep;
            }
            break;
         case 2:              //saw
            //with aliasing:
            //TODO: can be heavily optimized
            for(int i=0; i<numberSamples; ++i) {
               float freq = inputNode->mWorkBuffers[inputSlot][i] * mFreqMul;
               float phaseStep = freq / cSampleRate;
               while(phase > 1.0f)
                  phase -= 1.0f;
               while(phase<0.0f)
                  phase += 1.0f;

               float val = phase*2.0f-1.0f;
               val *= mAmplitude;
               mWorkBuffers[0][i] = val;
               phase += phaseStep;
            }
            break;
         case 3:              //triangle
             //with aliasing:
             //TODO: can be heavily optimized
             for(int i=0; i<numberSamples; ++i) {
                float freq = inputNode->mWorkBuffers[inputSlot][i] * mFreqMul;
                float phaseStep = freq / cSampleRate;
                while(phase > 1.0f)
                   phase -= 1.0f;
                while(phase<0.0f)
                   phase += 1.0f;

                float val;
                if(phase <= 0.5f)
                   val = phase*2;
                else
                   val = (1.0f-phase)*2;
                val = (val*2.0f)-1.0f;
                val *= mAmplitude;
                mWorkBuffers[0][i] = val;
                phase += phaseStep;
             }
            break;
         default:
            break;
         }
      }
   }
};

}; //namespace ImGui

#endif   //#ifndef __NODES_FMGENERATOR_H__
