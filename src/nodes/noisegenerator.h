
#ifndef __NODES_NOISEGENERATOR_H__
#define __NODES_NOISEGENERATOR_H__

#include "src/utils.h"
#include <math.h>

namespace ImGui {

class NoiseGeneratorNode : public ProcessingNode {
protected:
   NoiseGeneratorNode() : ProcessingNode(1) {}
   static const int TYPE = SNT_NOISEGENERATOR;

   const char* getTooltip() const {return "NoiseGenerator";}
   const char* getInfo() const {return "NoiseGenerator info.\n\nNoiseGenerator independent of frequency";}

   static bool getTextFromEnumIndex(void* ,int value,const char** pTxt) {
      if(!pTxt) 
         return false;
      static const char* values[] = {"WhiteNoise 0..+1", "WhiteNoise -1..+1"};
      static int numValues = (int)(sizeof(values)/sizeof(values[0]));
      if(value>=0 && value<numValues) 
         *pTxt = values[value];
      else 
         *pTxt = "UNKNOWN";
      return true;
   }

   //parameters:
   int mGeneratorType;
   float mAmplitude;

   //state:
   double mNoise;

public:
  static NoiseGeneratorNode* Create(const ImVec2& pos) {
    NoiseGeneratorNode* node = (NoiseGeneratorNode*) ImGui::MemAlloc(sizeof(NoiseGeneratorNode));
    IM_PLACEMENT_NEW (node) NoiseGeneratorNode();
  
    node->init("NoiseGenerator",pos,"","snd",TYPE);
    node->fields.addFieldEnum(&node->mGeneratorType,2,&getTextFromEnumIndex,"Type","Choose your favourite");
    node->fields.addField(&node->mAmplitude, 1, "Amp", nullptr, 3, 0.0f, 1000.0f);
    node->mGeneratorType = 0;
    node->mAmplitude = 0.2f;

    node->mNoise = 19.1919191919191919191919191919191919191919;       //init

    return node;
  }

  void renderSamples(int numberSamples) {
    switch(mGeneratorType) {
    case 0:           // WhiteNoise 0..+1
      for(int i=0; i<numberSamples; ++i) {
        double mNoiselast = mNoise;
        mNoise = mNoise + 19;
        mNoise = mNoise * mNoise;
        mNoise = mNoise + ((-mNoise + mNoiselast) * 0.5);
        int i_noise = mNoise;
        mNoise = mNoise - i_noise;

        mWorkBuffers[0][i] = mNoise * mAmplitude;
      }
      break;
    case 1:           // WhiteNoise -1..+1
      for(int i=0; i<numberSamples; ++i) {
        mNoise = mNoise * mNoise;
        double i_noise = floor(mNoise);
        mNoise = mNoise - i_noise;
        mWorkBuffers[0][i] = (mNoise-0.5) * mAmplitude;
        mNoise = mNoise + 19;
      }
      break;
    }

  }
};

}; //namespace ImGui

#endif   //#ifndef __NODES_NOISEGENERATOR_H__
