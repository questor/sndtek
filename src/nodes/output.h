
#ifndef __NODES_OUTPUT_H__
#define __NODES_OUTPUT_H__

namespace ImGui {

class OutputNode : public ProcessingNode {
protected:
   OutputNode() : ProcessingNode(0) {}
   static const int TYPE = SNT_OUTPUT;

   const char* getTooltip() const {return "Sends data to soundcard.";}
   const char* getInfo() const {return "Output info";}

public:
   static OutputNode* Create(const ImVec2& pos) {
      OutputNode* node = (OutputNode*) ImGui::MemAlloc(sizeof(OutputNode));
      IM_PLACEMENT_NEW (node) OutputNode();

      node->init("Output",pos,"snd","",TYPE);
      return node;
   }

   void renderSamples(int numberSamples) {         //virtual interface of ProcessingNade
      //empty, special export is done in another function because of bigger buffer needed (workbuffer is too small for stereo!)
   }

   void createOutputBuffer(int numberSamples, float *outputBuffer) {
      if(mInputNodes.size() != 0) {
         assert(mInputNodes.size() <= 2);          //left/right channels

         ProcessingNode *inputNodeL = mInputNodes[0].node;

         float *workBufferL = nullptr;
         if(inputNodeL)
            workBufferL = &inputNodeL->mWorkBuffers[mInputNodes[0].slot][0];

         if(workBufferL != 0) {
            for(int i=0; i<numberSamples; ++i) {
               *(outputBuffer++) = *(workBufferL++);
            }
         }
      }      
   }

};

}; //namespace ImGui

#endif   //#ifndef __NODES_OUTPUT_H__
