
#ifndef __NODES_DUMBADSR_H__
#define __NODES_DUMBADSR_H__

#include "src/utils.h"

namespace ImGui {

class DumbADSRNode : public ProcessingNode {
protected:
   DumbADSRNode() : ProcessingNode(1) {}
   static const int TYPE = SNT_DUMBADSR;

   const char* getTooltip() const {return "DumbADSR";}
   const char* getInfo() const {return "Really simple adsr envelope\n\n";}

   //parameters:
   int mAttack;
   int mDecay;
   int mSustain;
   float mSustainLevel;
   int mRelease;

   //state:
   int mPosInADSR;

public:
   static DumbADSRNode* Create(const ImVec2& pos) {
      DumbADSRNode* node = (DumbADSRNode*) ImGui::MemAlloc(sizeof(DumbADSRNode));
      IM_PLACEMENT_NEW (node) DumbADSRNode();

      node->init("DumbADSR", pos, "snd", "snd", TYPE);
      node->fields.addField(&node->mAttack, 1, "attack", nullptr, 3, 0, 100000);
      node->fields.addField(&node->mDecay, 1, "decay", nullptr, 3, 0, 100000);
      node->fields.addField(&node->mSustain, 1, "sustain", nullptr, 3, 0, 100000);
      node->fields.addField(&node->mSustainLevel, 1, "sustain level", nullptr, 3, 0.0f, 1.0f);
      node->fields.addField(&node->mRelease, 1, "release", nullptr, 3, 0, 100000);

      node->mAttack = 80;
      node->mDecay = 1000;
      node->mSustain = 4500;
      node->mSustainLevel = 0.4f;
      node->mRelease = 1850;

      node->mPosInADSR = 0;

      return node;
   }

   void noteOn() {
      mPosInADSR = 0;
   }
   void noteOff() {
      mPosInADSR = 1000000;
   }

   void renderSamples(int numberSamples) {
      if (mInputNodes.size() != 0) {
         assert(mInputNodes.size() == 1);

         ProcessingNode *inputNode = mInputNodes[0].node;
         int inputSlot = mInputNodes[0].slot;
         if(inputNode == nullptr)
            return;

         int attackDuration = mAttack;
         int decayDuration = mAttack + mDecay;
         int sustainDuration = decayDuration + mSustain;
         int releaseDuration = sustainDuration + mRelease;

         int currentState = 0;
         if(mPosInADSR > attackDuration)
            currentState = 1;
         if(mPosInADSR > decayDuration)
            currentState = 2;
         if(mPosInADSR > sustainDuration)
            currentState = 3;
         if(mPosInADSR > releaseDuration) {
            memset(&mWorkBuffers[0][0], 0, sizeof(float)*numberSamples);
            return;
         }

         float *destBuffer = &mWorkBuffers[0][0];

         while(numberSamples > 0) {
            switch(currentState) {
            case 0:              //attack
               {
                  int genSamples = min(numberSamples, attackDuration - mPosInADSR);
                  for(int i=0; i<genSamples; ++i) {
                     float envelopeVolume = (float)mPosInADSR / (float)attackDuration;
                     *(destBuffer++) = envelopeVolume * inputNode->mWorkBuffers[inputSlot][i];
                  }
                  numberSamples -= genSamples;
                  currentState += 1;
               }
               break;
            case 1:              //decay
               {
                  int genSamples = min(numberSamples, decayDuration - mPosInADSR);
                  int samplesToDecay = decayDuration - attackDuration;
                  float stepDelta = (1.0f - mSustainLevel) / samplesToDecay;
                  float currentValue = mSustainLevel + stepDelta * (decayDuration - mPosInADSR);
                  for(int i=0; i<genSamples; ++i) {
                     *(destBuffer++) = currentValue * inputNode->mWorkBuffers[inputSlot][i];
                     currentValue -= stepDelta;
                  }
                  mPosInADSR += genSamples;
                  numberSamples -= genSamples;
                  currentState += 1;
               }
               break;
            case 2:              //sustain
               {
                  int genSamples = min(numberSamples, sustainDuration - mPosInADSR);
                  for(int i=0; i<genSamples; ++i) {
                     *(destBuffer++) = mSustainLevel * inputNode->mWorkBuffers[inputSlot][i];
                  }
                  mPosInADSR += genSamples;
                  numberSamples -= genSamples;
                  currentState += 1;
               }
               break;
            case 3:              //release
               {
                  int genSamples = min(numberSamples, releaseDuration - mPosInADSR);
                  int samplesToRelease = releaseDuration - sustainDuration;
                  float stepDelta = mSustainLevel / samplesToRelease;
                  float currentValue = mSustainLevel - stepDelta * (samplesToRelease - (releaseDuration - mPosInADSR));
                  for(int i=0; i<genSamples; ++i) {
                     *(destBuffer++) = currentValue * inputNode->mWorkBuffers[inputSlot][i];
                     currentValue -= stepDelta;
                  }
                  mPosInADSR += genSamples;
                  numberSamples -= genSamples;
                  currentState += 1;
               }
               break;
            default:             //after release and elase => set to zero
               {
                  memset(destBuffer, 0, sizeof(float)*numberSamples);
                  numberSamples -= numberSamples;
               }
               break;
            }
         }
      }
   }

 };

}; //namespace ImGui

#endif   //#ifndef __NODES_DUMBADSR_H__
