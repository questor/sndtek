
#ifndef __NODES_DEBUG_H__
#define __NODES_DEBUG_H__

namespace ImGui {

class DebugNode : public ProcessingNode {
protected:
   DebugNode() : ProcessingNode(0) {}
   static const int TYPE = SNT_DEBUG;

   const char* getTooltip() const {return "Debug.";}
   const char* getInfo() const {return "Debug.\n\nThis is supposed to display some info about this node.";}

   float mMinVal, mMaxVal;
   bool mResetFlag;

public:
   static DebugNode* Create(const ImVec2& pos) {
      DebugNode* node = (DebugNode*) ImGui::MemAlloc(sizeof(DebugNode));
      IM_PLACEMENT_NEW (node) DebugNode();

      node->init("Debug",pos,"value","",TYPE);
      node->fields.addField(&node->mMinVal, 1, "MinVal");
      node->fields.addField(&node->mMaxVal, 1, "MaxVal");
      node->fields.addField(&node->mResetFlag, "Reset");
      node->mResetFlag = false;
      return node;
   }

   void renderSamples(int numberSamples) {
      if((mInputNodes.size() == 1) || (mResetFlag)) {
         ProcessingNode *inputNode = mInputNodes[0].node;
         if(inputNode == nullptr)
            return;
         int inputNodeSlot = mInputNodes[0].slot;
         for(int i=0; i<numberSamples; ++i) {
            float val = inputNode->mWorkBuffers[inputNodeSlot][i];
            if(val > mMaxVal) {
               mMaxVal = val;
            } else if(val < mMinVal) {
               mMinVal = val;
            }
         }
      } else {
         mMinVal = 100000;
         mMaxVal = 0;
      }
   }
};

}; //namespace ImGui

#endif   //#ifndef __NODES_CONSTINPUT_H__
