
#ifndef __NODES_OPERATOR_H__
#define __NODES_OPERATOR_H__

#include "src/utils.h"
#include <math.h>

namespace ImGui {

class OperatorNode : public ProcessingNode {
protected:
   OperatorNode() : ProcessingNode(1) {}
   static const int TYPE = SNT_OPERATOR;

   const char* getTooltip() const {return "Generic Operator";}
   const char* getInfo() const {return "Generic Operator info";}

   static bool getTextFromEnumIndex(void* ,int value,const char** pTxt) {
      if(!pTxt) 
         return false;
      static const char* values[] = {"+", "-", "*", "/"};
      static int numValues = (int)(sizeof(values)/sizeof(values[0]));
      if(value>=0 && value<numValues) 
         *pTxt = values[value];
      else 
         *pTxt = "UNKNOWN";
      return true;
   }

   int mOperatorType;

public:
   static OperatorNode* Create(const ImVec2& pos) {
      OperatorNode* node = (OperatorNode*) ImGui::MemAlloc(sizeof(OperatorNode));
      IM_PLACEMENT_NEW (node) OperatorNode();
    
      node->init("Operator",pos,"val1;val2","out",TYPE);
      node->fields.addFieldEnum(&node->mOperatorType,4,&getTextFromEnumIndex,"Type","Choose your favourite");
      node->mOperatorType = 0;

      return node;
   }

   void setMode(int val) {
      mOperatorType = val;
   }

   void renderSamples(int numberSamples) {
      if(mInputNodes.size() == 2) {
         ProcessingNode *inputNode1 = mInputNodes[0].node;
         int inputSlot1 = mInputNodes[0].slot;
         ProcessingNode *inputNode2 = mInputNodes[1].node;
         int inputSlot2 = mInputNodes[1].slot;

         if((inputNode1 == nullptr) || (inputNode2 == nullptr))
            return;

         float *vals1 = &inputNode1->mWorkBuffers[inputSlot1][0];
         float *vals2 = &inputNode2->mWorkBuffers[inputSlot2][0];
         float *out = &mWorkBuffers[0][0];

         switch(mOperatorType) {
         case 0:              // +
            for(int i=0; i<numberSamples; ++i) {
              *(out++) = *(vals1++) + *(vals2++);
            }
            break;
         case 1:              // -
            for(int i=0; i<numberSamples; ++i) {
              *(out++) = *(vals1++) - *(vals2++);
            }
            break;
         case 2:              // *
            for(int i=0; i<numberSamples; ++i) {
              *(out++) = *(vals1++) * *(vals2++);
            }
            break;
         case 3:              // /
            for(int i=0; i<numberSamples; ++i) {
              *(out++) = *(vals1++) / *(vals2++);
            }
            break;
         default:
            break;
         }
      }
   }
};

}; //namespace ImGui

#endif   //#ifndef __NODES_OPERATOR_H__
