
#ifndef __NODES_DX7_OSC_H__
#define __NODES_DX7_OSC_H__

#include "src/utils.h"

// ripped from here: https://github.com/mmontag/dx7-synth-js

namespace ImGui {

#define OCTAVE_1024 1.0006771307          // exp(log(2)/1024)


class Dx7OscNode : public ProcessingNode {
   Dx7OscNode() : ProcessingNode(1) {}
   static const int TYPE = SNT_DX7OSC;

   const char* getTooltip() const {return "DX7 Oscillator";}
   const char* getInfo() const {return "DX7 Oscillator.\n\nSimple model used for this node!";}

   //parameters:
   int mOscMode;          // [0;1]
   float mDetune;         // [-7..7]

   int mFreqCoarse;       //osc==0: [0..31]     osc==1: [0..3]
   int mFreqFine;         //[0..99]

   float mOutputLevel;    //[0..99]
   float mFeedbackRatio;  //[0..7]

   int mVelocitySens;     //[0..7]

   bool mSelfModulation;


   //state:
   float mPhase;
   float mPhaseStep;
   float mFreqRatio;      //only when oscMode == 0
   float mFreqFixed;      //only when oscMode == 1

   float mWorkOutputLevel;
   float mWorkFeedbackRatio;

   void updateFrequency(float baseFrequency) {              //called if baseFrequency(note) changes
      float freq = mOscMode ? mFreqFixed : baseFrequency * mFreqRatio * pow(OCTAVE_1024, mDetune);
      mPhaseStep = 2*cPI*freq / cSampleRate;
   }

public:
   static Dx7OscNode* Create(const ImVec2& pos) {
      Dx7OscNode* node = (Dx7OscNode*) ImGui::MemAlloc(sizeof(Dx7OscNode));
      IM_PLACEMENT_NEW (node) Dx7OscNode();

      node->init("DX7 Osc", pos, "freq;mod;lfoPitch;lfoAmp", "snd", TYPE);

      node->fields.addField(&node->mOscMode, 1, "osc mode", nullptr, 0, 0, 1);
      node->fields.addField(&node->mDetune, 1, "detune", nullptr, 0, -7.0f, 7.0f);
      node->fields.addField(&node->mFreqCoarse, 1, "freq coarse", nullptr, 0, 0, 31);
      node->fields.addField(&node->mFreqFine, 1, "freq fine", nullptr, 0, 0, 99);
      node->fields.addField(&node->mOutputLevel, 1, "output level", nullptr, 0, 0, 99);
      node->fields.addField(&node->mFeedbackRatio, 1, "feedback ratio", nullptr, 0, 0, 7);
      node->fields.addField(&node->mVelocitySens, 1, "velocity sens", nullptr, 0, 0, 7);

      node->mOscMode = 0;
      node->mDetune = 0;
      node->mFreqCoarse = 0;
      node->mFreqFine = 0;
      node->mOutputLevel = 50;
      node->mFeedbackRatio = 0;
      node->mVelocitySens = 0;
      node->mSelfModulation = false;

      node->mPhase = node->mPhaseStep = node->mFreqRatio = node->mFreqFixed = node->mWorkOutputLevel = node->mWorkFeedbackRatio = 0;

      return node;
   }

   void setOscMode(int mode) {
      assert(mode == 0 || mode == 1);
      mOscMode = mode;
   }
   void setDetune(float detune) {
      assert(detune >= -7 && detune <= 7);
      mDetune = detune;
   }
   void setFreqCoarse(int freq) {
      mFreqCoarse = freq;
   }
   void setFreqFine(int freq) {
      assert(freq >= 0 && freq <= 99);
      mFreqFine = freq;
   }
   void setOutputLevel(float level) {
      assert(level >= 0 && level <= 99);
      mOutputLevel = level;
   }
   void setFeedbackRatio(float ratio) {
      assert(ratio >= 0 && ratio <= 7);
      mFeedbackRatio = ratio;
   }
   void setVelocitySens(int sens) {
      assert(sens >= 0 && sens <= 7);
      mVelocitySens = sens;
   }
   void setSelfModulation(bool flag) {
      mSelfModulation = flag;
   }

   void noteOn() {
      float velocity = 0.5f;              //?!? comes from midi in what format?

      mWorkOutputLevel = (1+(velocity-1)*(mVelocitySens / 7.0f)) * mOutputLevel;
      mWorkFeedbackRatio = pow(2, (mFeedbackRatio-7.0f));
   }

   void renderSamples(int numberSamples) {
      if (mInputNodes.size() != 0) {
         ProcessingNode *inputNode = mInputNodes[0].node;      //freq
         int freqInputSlot = mInputNodes[0].slot;
         if(inputNode == nullptr)
            return;

         float *modBuffer = nullptr;
         int modSlot = 0;
         ProcessingNode *modNode = mInputNodes[1].node;
         modSlot = mInputNodes[1].slot;
         if(modNode != nullptr) {
            modBuffer = &modNode->mWorkBuffers[modSlot][0];
         }

         float *lfoPitchBuffer = nullptr;
         int lfoPitchSlot = 0;
         ProcessingNode *lfoPitchNode = mInputNodes[2].node;
         lfoPitchSlot = mInputNodes[2].slot;
         if(lfoPitchNode != nullptr) {
            lfoPitchBuffer = &lfoPitchNode->mWorkBuffers[lfoPitchSlot][0];
         } else {
            return;
         }

         float *lfoAmpBuffer = nullptr;
         int lfoAmpSlot = 0;
         ProcessingNode *lfoAmpNode = mInputNodes[3].node;
         lfoAmpSlot = mInputNodes[2].slot;
         if(lfoAmpNode != nullptr) {
            lfoAmpBuffer = &lfoAmpNode->mWorkBuffers[lfoAmpSlot][0];
         } else {
            return;
         }

         float *srcBuffer = &inputNode->mWorkBuffers[freqInputSlot][0];
         float *destBuffer = &mWorkBuffers[0][0];

         if(mOscMode == 0) {
            float calcCoarse = mFreqCoarse;
            if(calcCoarse == 0) {
               calcCoarse = 0.5f;
            }
            mFreqRatio = calcCoarse * (1.0f + mFreqFine / 100.0f);
         } else {
            mFreqFixed = pow(10, mFreqCoarse % 4) * (1.0f+(mFreqFine/99.0f)*8.772f);
         }

         updateFrequency(*srcBuffer);              //update only with the first value
         
         if(modBuffer == nullptr) {
            for(int i=0; i<numberSamples; ++i) {
               float val = sin(mPhase) * *(lfoAmpBuffer++); //mLfo.renderAmp();   // * envelope
               mPhase += mPhaseStep * *(lfoPitchBuffer++);  //mLfo.renderPitch();
               if(mPhase >= 2*cPI)
                  mPhase -= 2*cPI;

               *(destBuffer++) = val;
            }
         } else {
            float mod = 0;
            for(int i=0; i<numberSamples; ++i) {
               float val = sin(mPhase+mod) * *(lfoAmpBuffer++); //mLfo.renderAmp();   // * envelope
               mPhase += mPhaseStep * *(lfoPitchBuffer++);      //mLfo.renderPitch();
               if(mPhase >= 2*cPI)
                  mPhase -= 2*cPI;

               *(destBuffer++) = val;

               if(mSelfModulation) {
                  //TODO!
                  mod += val * mWorkFeedbackRatio;                //take own value! envelope handling is not correct here :/
               } else {
                  mod += *(modBuffer++) * mWorkOutputLevel;
               }
            }
         }
      }
   }

 };

}; //namespace ImGui

#endif   //#ifndef __NODES_DX7_OSC_H__
