
#ifndef __NODES_GENERATOR_H__
#define __NODES_GENERATOR_H__

#include "src/utils.h"
#include <math.h>

namespace ImGui {

//https://github.com/skei/s2/blob/master/src/audio/waveforms/s2_waveform_polyblep.h


//http://martin-finke.de/blog/articles/audio-plugins-018-polyblep-oscillator/
//https://github.com/martinfinke/PolyBLEP/blob/master/PolyBLEP.cpp

template<typename T> inline int64_t truncate(const T &t) {
   return static_cast<int64_t>(t) | 0;
}

inline float blep(float t, float dt) {
   if(t < dt) {
      t = t / dt - 1;
      return -t*t;
   } else if(t > 1 - dt) {
      t = (t-1) / dt + 1;
      return t*t;
   } else {
      return 0;
   }
}
inline float blamp(float t, float dt) {
   if(t < dt) {
      t = t / dt - 1;
      return -1.0f / 3.0f * t*t*t;
   } else if(t > 1 - dt) {
      t = (t-1) / dt + 1;
      return 1.0f / 3.0f * t*t*t;
   } else {
      return 0;
   }
}

class GeneratorNode : public ProcessingNode {
protected:
   GeneratorNode() : ProcessingNode(1) {}
   static const int TYPE = SNT_GENERATOR;

   const char* getTooltip() const {return "Generator";}
   const char* getInfo() const {return "Generator info.\n\nThis node only reads the frequency one time from the buffer!";}

   static bool getTextFromEnumIndex(void* , int value, const char** pTxt) {
      if (!pTxt)
         return false;
      static const char* values[] = {"SIN", "SQUARE", "SAW", "TRIANGLE", "HALF(BLEP)", "FULL(BLEP)",
                        "TRI(BLEP)", "TRI2(BLEP)", "TRIP(BLEP)", "TRAP(BLEP)", "TRAP2(BLEP)", "SQR(BLEP)",
                        "SQR2(BLEP)", "RECT(BLEP)", "SAW(BLEP)", "RAMP(BLEP)"};
      static int numValues = (int)(sizeof(values) / sizeof(values[0]));
      if (value >= 0 && value < numValues)
         *pTxt = values[value];
      else
         *pTxt = "UNKNOWN";
      return true;
   }

   //parameters:
   int mGeneratorType;
   float mFreqMul;
   float mAmplitude;

   float mPulseWidth;   //used for tri2, trip, trap2, sqr2, rect, 

   //state:
   float phase;

public:
   static GeneratorNode* Create(const ImVec2& pos) {
      GeneratorNode* node = (GeneratorNode*) ImGui::MemAlloc(sizeof(GeneratorNode));
      IM_PLACEMENT_NEW (node) GeneratorNode();

      node->init("Generator", pos, "freq", "snd", TYPE);
      node->fields.addFieldEnum(&node->mGeneratorType, 16, &getTextFromEnumIndex, "Type", "Choose your favourite");
      node->fields.addField(&node->mFreqMul, 1, "FreqMul", nullptr, 3, 0.00001f, 1000.0f);
      node->fields.addField(&node->mAmplitude, 1, "Amp", nullptr, 3, 0.0f, 1000.0f);
      node->fields.addField(&node->mPulseWidth, 1, "PulseWidth", nullptr, 3, 0.0f, 1.0f);
      node->mGeneratorType = 0;
      node->mFreqMul = 1.0f;
      node->mAmplitude = 0.2f;

      return node;
   }

   void renderSamples(int numberSamples) {
      if (mInputNodes.size() != 0) {
         assert(mInputNodes.size() == 1);

         ProcessingNode *inputNode = mInputNodes[0].node;
         int inputSlot = mInputNodes[0].slot;
         if(inputNode == nullptr)
            return;
         switch (mGeneratorType) {
         case 0: {            //sin
            float freq = inputNode->mWorkBuffers[inputSlot][0] * mFreqMul;
            float phaseStep = 2 * cPI * freq / cSampleRate;
            for (int i = 0; i < numberSamples; ++i) {
               float value = sin(phase) * mAmplitude;
               mWorkBuffers[0][i] = value;
               phase += phaseStep;
            }
            //renormalize phase
            while (phase >= 2 * cPI)
               phase -= 2 * cPI;
            while (phase < 0)
               phase += 2 * cPI;
         }
         break;
         case 1: {            //square
            //with aliasing:
            //TODO: can be heavily optimized
            float freq = inputNode->mWorkBuffers[inputSlot][0] * mFreqMul;
            float phaseStep = freq / cSampleRate;
            for (int i = 0; i < numberSamples; ++i) {
               while (phase > 1.0f)
                  phase -= 1.0f;
               while (phase < 0.0f)
                  phase += 1.0f;

               float val;
               if (phase <= 0.5f)
                  val = -mAmplitude;
               else
                  val = mAmplitude;

               mWorkBuffers[0][i] = val;
               phase += phaseStep;
            }
         }
         break;
         case 2: {            //saw
            //with aliasing:
            //TODO: can be heavily optimized
            float freq = inputNode->mWorkBuffers[inputSlot][0] * mFreqMul;
            float phaseStep = freq / cSampleRate;
            for (int i = 0; i < numberSamples; ++i) {
               while (phase > 1.0f)
                  phase -= 1.0f;
               while (phase < 0.0f)
                  phase += 1.0f;

               float val = phase * 2.0f - 1.0f;
               val *= mAmplitude;
               mWorkBuffers[0][i] = val;
               phase += phaseStep;
            }
         }
         break;
         case 3: {            //triangle
            //with aliasing:
            //TODO: can be heavily optimized
            float freq = inputNode->mWorkBuffers[inputSlot][0] * mFreqMul;
            float phaseStep = freq / cSampleRate;
            for (int i = 0; i < numberSamples; ++i) {
               while (phase > 1.0f)
                  phase -= 1.0f;
               while (phase < 0.0f)
                  phase += 1.0f;

               float val;
               if (phase <= 0.5f)
                  val = phase * 2;
               else
                  val = (1.0f - phase) * 2;
               val = (val * 2.0f) - 1.0f;
               val *= mAmplitude;
               mWorkBuffers[0][i] = val;
               phase += phaseStep;
            }
         }
         break;

         case 4: {     //Half wave rectified sine
            float freq = inputNode->mWorkBuffers[inputSlot][0] * mFreqMul;
            float phaseStep = freq / cSampleRate;
            for (int i = 0; i < numberSamples; ++i) {
               float t2 = phase+0.5f;
               t2 -= truncate(t2);

               float y = (phase<0.5f ? 2*sin(phase*cTwoPI) - 2 / cPI : -2.0f/cPI);
               y += cTwoPI * phaseStep * (blamp(phase, phaseStep)+blamp(t2, phaseStep));
               mWorkBuffers[0][i] = y * mAmplitude;

               // inc:
               phase += phaseStep;
               phase -= truncate(phase);
            }
         }
         break;

         case 5: {     //Full wave rectified sine
            float freq = inputNode->mWorkBuffers[inputSlot][0] * mFreqMul;
            float phaseStep = freq / cSampleRate;
            for (int i = 0; i < numberSamples; ++i) {
               float t2 = phase+0.25f;
               t2 -= truncate(t2);

               float y = 2.0f*sin(t2*cPI) - 4 / cPI;
               y += cTwoPI * phaseStep * blamp(t2, phaseStep);
               mWorkBuffers[0][i] = y * mAmplitude;

               // inc:
               phase += phaseStep;
               phase -= truncate(phase);
            }
         }
         break;

         case 6: {     //Tri
            float freq = inputNode->mWorkBuffers[inputSlot][0] * mFreqMul;
            float phaseStep = freq / cSampleRate;
            for (int i = 0; i < numberSamples; ++i) {
               float t1 = phase+0.25f;
               t1 -= truncate(t1);

               float t2 = phase+0.75f;
               t2 -= truncate(t2);

               float y = phase * 4.0f;
               if(y >= 3.0f) {
                  y -= 4.0f;
               } else if(y > 1.0f) {
                  y = 2.0f - y;
               }
               y += 4.0f * phaseStep * (blamp(t1, phaseStep) - blamp(t2, phaseStep));

               mWorkBuffers[0][i] = y * mAmplitude;

               // inc:
               phase += phaseStep;
               phase -= truncate(phase);
            }
         }
         break;

         case 7: {     //Tri2
            float freq = inputNode->mWorkBuffers[inputSlot][0] * mFreqMul;
            float phaseStep = freq / cSampleRate;
            mPulseWidth = max(0.0001f, min(0.9999f, mPulseWidth));
            for (int i = 0; i < numberSamples; ++i) {
               float t1 = phase+0.5f*mPulseWidth;
               t1 -= truncate(t1);

               float t2 = phase+1.0f-0.5f*mPulseWidth;
               t2 -= truncate(t2);

               float y = phase * 2.0f;
               if(y >= 2.0f-mPulseWidth) {
                  y = (y-2.0f)/mPulseWidth;
               } else if(y >= mPulseWidth) {
                  y = 1.0f - (y-mPulseWidth) / (1.0f-mPulseWidth);
               } else {
                  y /= mPulseWidth;
               }

               y += phaseStep / (mPulseWidth-mPulseWidth*mPulseWidth) * (blamp(t1, phaseStep) - blamp(t2, phaseStep));

               mWorkBuffers[0][i] = y * mAmplitude;

               // inc:
               phase += phaseStep;
               phase -= truncate(phase);
            }
         }
         break;

         case 8: {     //Trip
            float freq = inputNode->mWorkBuffers[inputSlot][0] * mFreqMul;
            float phaseStep = freq / cSampleRate;
            for (int i = 0; i < numberSamples; ++i) {
               float t1 = phase+0.75f+0.5f*mPulseWidth;
               t1 -= truncate(t1);

               float y;
               if(t1 >= mPulseWidth) {
                  y = - mPulseWidth;
               } else {
                  y = 4.0f * t1;
                  y = (y >= 2.0f*mPulseWidth ? 4 - y/mPulseWidth-mPulseWidth : y/mPulseWidth-mPulseWidth);
               }

               if(mPulseWidth > 0.0f) {
                  float t2 = t1 + 1.0f - 0.5f*mPulseWidth;
                  t2 -= truncate(t2);

                  float t3 = t1 + 1.0f - mPulseWidth;
                  t3 -= truncate(t3);

                  y += 2.0f*phaseStep / mPulseWidth * (blamp(t1, phaseStep) - 2*blamp(t2, phaseStep) + blamp(t3, phaseStep));
               }

               mWorkBuffers[0][i] = y * mAmplitude;

               // inc:
               phase += phaseStep;
               phase -= truncate(phase);
            }
         }
         break;

         case 9: {     //Trap
            float freq = inputNode->mWorkBuffers[inputSlot][0] * mFreqMul;
            float phaseStep = freq / cSampleRate;

            for (int i = 0; i < numberSamples; ++i) {
               float y = 4.0f * phase;
               if(y >= 3.0f) {
                  y -= 4.0f;
               } else if(y > 1.0f) {
                  y = 2.0f - y;
               }
               y = max(-1.0f, min(1.0f, 2.0f*y));

               float t1 = phase + 0.125f;
               t1 -= truncate(t1);

               float t2 = t1 + 0.5f;
               t2 -= truncate(t2);

               //triangle #1
               y += 4.0f*phaseStep*(blamp(t1, phaseStep)-blamp(t2, phaseStep));

               t1 = phase + 0.375f;
               t1 -= truncate(t1);

               t2 = t1 + 0.5f;
               t2 -= truncate(t2);

               //triangle #2
               y += 4.0f*phaseStep*(blamp(t1, phaseStep)-blamp(t2, phaseStep));

               mWorkBuffers[0][i] = y * mAmplitude;

               // inc:
               phase += phaseStep;
               phase -= truncate(phase);
            }
         }
         break;

         case 10: {    //Trap2
            float freq = inputNode->mWorkBuffers[inputSlot][0] * mFreqMul;
            float phaseStep = freq / cSampleRate;
            mPulseWidth = min(0.9999f, mPulseWidth);
            float scale = 1.0f / (1.0f-mPulseWidth);

            for (int i = 0; i < numberSamples; ++i) {
               float y = 4.0f * phase;
               if(y >= 3.0f) {
                  y -= 4.0f;
               } else if(y > 1.0f) {
                  y = 2.0f - y;
               }
               y = max(-1.0f, min(1.0f, scale*y));

               float t1 = phase + 0.25f-0.25f*mPulseWidth;
               t1 -= truncate(t1);

               float t2 = t1 + 0.5f;
               t2 -= truncate(t2);

               //triangle #1
               y += scale*2.0f*phaseStep*(blamp(t1, phaseStep)-blamp(t2, phaseStep));

               t1 = phase + 0.25f + 0.25f*mPulseWidth;
               t1 -= truncate(t1);

               t2 = t1 + 0.5f;
               t2 -= truncate(t2);

               //triangle #2
               y += scale*2.0f*phaseStep*(blamp(t1, phaseStep)-blamp(t2, phaseStep));

               mWorkBuffers[0][i] = y * mAmplitude;

               // inc:
               phase += phaseStep;
               phase -= truncate(phase);
            }
         }
         break;

         case 11: {    //SQR
            float freq = inputNode->mWorkBuffers[inputSlot][0] * mFreqMul;
            float phaseStep = freq / cSampleRate;

            for (int i = 0; i < numberSamples; ++i) {
               float t2 = phase + 0.5f;
               t2 -= truncate(t2);

               float y = phase < 0.5f ? 1.0f : -1.0f;
               y += blep(phase, phaseStep) - blep(t2, phaseStep);

               mWorkBuffers[0][i] = y * mAmplitude;

               // inc:
               phase += phaseStep;
               phase -= truncate(phase);
            }
         }
         break;

         case 12: {    //SQR2
            float freq = inputNode->mWorkBuffers[inputSlot][0] * mFreqMul;
            float phaseStep = freq / cSampleRate;

            for (int i = 0; i < numberSamples; ++i) {
               float t1 = phase + 0.875f+0.25f*(mPulseWidth-0.5f);
               t1 -= truncate(t1);

               float t2 = phase + 0.375f+0.25f*(mPulseWidth-0.5f);
               t2 -= truncate(t2);

               //Square #1
               float y = t1 < 0.5f ? 1.0f : -1.0f;
               y += blep(t1, phaseStep) - blep(t2, phaseStep);

               t1 += 0.5f*(1.0f-mPulseWidth);
               t1 -= truncate(t1);

               t2 += 0.5f*(1.0f-mPulseWidth);
               t2 -= truncate(t2);

               //Square #2
               y += t1 < 0.5f ? 1.0f : -1.0f;
               y += blep(t1, phaseStep) - blep(t2, phaseStep);

               mWorkBuffers[0][i] = y * mAmplitude * 0.5f;

               // inc:
               phase += phaseStep;
               phase -= truncate(phase);
            }
         }
         break;

         case 13: {    //RECT
            float freq = inputNode->mWorkBuffers[inputSlot][0] * mFreqMul;
            float phaseStep = freq / cSampleRate;

            for (int i = 0; i < numberSamples; ++i) {
               float t2 = phase + 1.0f - mPulseWidth;
               t2 -= truncate(t2);

               float y = -2.0f * mPulseWidth;
               if(phase < mPulseWidth) {
                  y += 2.0f;
               }
               y += blep(phase, phaseStep) - blep(t2, phaseStep);

               mWorkBuffers[0][i] = y * mAmplitude;

               // inc:
               phase += phaseStep;
               phase -= truncate(phase);
            }
         }
         break;

         case 14: {    //SAW
            float freq = inputNode->mWorkBuffers[inputSlot][0] * mFreqMul;
            float phaseStep = freq / cSampleRate;

            for (int i = 0; i < numberSamples; ++i) {
               float t2 = phase + 0.5f;
               t2 -= truncate(t2);

               float y = 2.0f * t2 - 1.0f;
               y -= blep(t2, phaseStep);

               mWorkBuffers[0][i] = y * mAmplitude;

               // inc:
               phase += phaseStep;
               phase -= truncate(phase);
            }
         }
         break;

         case 15: {    //RAMP
            float freq = inputNode->mWorkBuffers[inputSlot][0] * mFreqMul;
            float phaseStep = freq / cSampleRate;

            for (int i = 0; i < numberSamples; ++i) {
               float y = 1.0f - 2.0f * phase;
               y += blep(phase, phaseStep);

               mWorkBuffers[0][i] = y * mAmplitude;

               // inc:
               phase += phaseStep;
               phase -= truncate(phase);
            }
         }
         break;

         default:
            break;
         }
      }
   }

 };

}; //namespace ImGui

#endif   //#ifndef __NODES_GENERATOR_H__
