
#ifndef __NODES_DX7_ENV_H__
#define __NODES_DX7_ENV_H__

#include "src/utils.h"

// ripped from here: https://github.com/mmontag/dx7-synth-js

#define LG_N 6
#define N (1 << LG_N) 

namespace ImGui {

class Dx7EnvNode : public ProcessingNode {
   Dx7EnvNode() : ProcessingNode(1) {}
   static const int TYPE = SNT_DX7ENV;

   const char* getTooltip() const {return "DX7 Envelope";}
   const char* getInfo() const {return "DX7 Envelope.\n\nSimple model used for this node!";}

   //parameters:
   int mRates[4];
   int mLevels[4];
   int mRateScaling;

   //state:
   int mLevel;
   int mIndex;
   int mTargetLevel;
   int mInc;
   bool mDown;
   bool mRising;

   static float sOutputLUT[4096];

   void initOutputLUT() {
      if(sOutputLUT[1000] == 0) {
         for(int i=0; i<4096; ++i) {
            float dB = (i - 3824) * 0.0235f;
            sOutputLUT[i] = pow(20, dB/20.0f);
         }
      }
   }

   int scaleOutLevel(int outlevel) {
      const int levellut[] = {
         0, 5, 9, 13, 17, 20, 23, 25, 27, 29, 31, 33, 35, 37, 39, 41, 42, 43, 45, 46 
      };
      return outlevel >= 20 ? 28+outlevel : levellut[outlevel];
   }

   void advance(int newIndex) {
      mIndex = newIndex;
      if(mIndex < 4) {
         int newLevel = mLevels[newIndex];
         mTargetLevel = max(0, (scaleOutLevel(newLevel) << 5) - 224);         //1 -> -192; 99->127->3840
         mRising = (mTargetLevel > mLevel);


         float qrate = min(63, mRateScaling + ((mRates[mIndex]*41) >> 6));    // 5 -> 3; 49 -> 31; 99 -> 63
         mInc = pow(2, qrate/4) / 2048;
         //BUG if mInc become 0 :/
      }
   }

public:
   static Dx7EnvNode* Create(const ImVec2& pos) {
      Dx7EnvNode* node = (Dx7EnvNode*) ImGui::MemAlloc(sizeof(Dx7EnvNode));
      IM_PLACEMENT_NEW (node) Dx7EnvNode();

      node->initOutputLUT();

      node->init("DX7 Env", pos, "snd", "snd", TYPE);
      node->fields.addField(&node->mRates[0], 4, "rates", nullptr, 0, 0, 99);
      node->fields.addField(&node->mLevels[0], 4, "levels", nullptr, 0, 0, 99);
      node->fields.addField(&node->mRateScaling, 1, "rate scaling", nullptr, 0, 0, 99);

      node->mRateScaling = 0;          //?? not used in dx7-synth-js

/*      node->mRates[0] = 96;
      node->mRates[1] = 0;
      node->mRates[2] = 12;
      node->mRates[3] = 70;
      node->mLevels[0] = 99;
      node->mLevels[1] = 95;
      node->mLevels[2] = 95;
      node->mLevels[3] = 0;
*/
      node->mRates[0] = 60;
      node->mRates[1] = 80;
      node->mRates[2] = 10;
      node->mRates[3] = 70;
      node->mLevels[0] = 99;
      node->mLevels[1] = 40;
      node->mLevels[2] = 70;
      node->mLevels[3] = 0;

      node->mLevel = 0;
      node->mDown = true;
      node->mInc = 0;
      node->advance(0);

      return node;
   }

   void setRate(int index, int rate) {
      assert(index < 4);
      mRates[index] = rate;
   }
   void setLevel(int index, int level) {
      assert(index < 4);
      mLevels[index] = level;
   }
   void setRateScaling(int scaling) {
      mRateScaling = scaling;
   }


   void noteOn() {
      mDown = true;
      advance(0);
   }
   void noteOff() {
      mDown = false;
      advance(3);
   }

   void renderSamples(int numberSamples) {
      if (mInputNodes.size() != 0) {
         assert(mInputNodes.size() == 1);

         ProcessingNode *inputNode = (ProcessingNode *)mInputNodes[0].node;
         if(inputNode == nullptr)
            return;

         int inputSlot = mInputNodes[0].slot;
         float *srcBuffer = &inputNode->mWorkBuffers[inputSlot][0];
         float *destBuffer = &mWorkBuffers[0][0];
         
         for(int i=0; i<numberSamples; ++i) {
            if(mIndex < 3 || (mIndex < 4) && !mDown) {
               if(mRising) {
                  mLevel += mInc * (2 + (mTargetLevel - mLevel) / 256);
                  if(mLevel >= mTargetLevel) {
                     mLevel = mTargetLevel;
                     advance(mIndex + 1);
                  }
               } else {
                  mLevel -= mInc;
                  if(mLevel <= mTargetLevel) {
                     mLevel = mTargetLevel;
                     advance(mIndex + 1);
                  }
               }
            }
            *(destBuffer++) = *(srcBuffer++) * sOutputLUT[mLevel];
         }         
      }
   }

 };

}; //namespace ImGui

#endif   //#ifndef __NODES_DX7_ENV_H__
