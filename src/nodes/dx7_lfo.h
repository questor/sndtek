#ifndef __NODES_DX7_LFO_H__
#define __NODES_DX7_LFO_H__

#include "random.h"

#define LFO_RATE 441
#define LFO_SAMPLE_PERIOD (cSampleRate / LFO_RATE)

class ImportSysex;

namespace ImGui {

class Dx7LfoNode : public ProcessingNode {
   friend class ImportSysex;

protected:
   Dx7LfoNode() : ProcessingNode(2) {}
   static const int TYPE = SNT_DX7LFO;

   const char* getTooltip() const {return "DX7 LFO";}
   const char* getInfo() const {return "DX7 LFO.\n\nSimple model used for this node!";}

   //state:
   int mRenderSampleCounter;
   int mDelayState;
   int mDelayTimes[3];
   float mDelayIncrements[3];
   float mDelayVals[3];
   float mDelayValue;
   float mPhase;
   float mPhaseStep;

   float mAmpValue;
   float mAmpValTarget;
   float mAmpIncrement;
   float mAmpModDepth;

   float pitchVal;         //used to store calculated value

   float mSampleHoldRandom;

   pcg32_random_t mRandomState;

   static bool getTextFromEnumIndex(void* , int value, const char** pTxt) {
      if (!pTxt)
         return false;
      static const char* values[] = {"TRIANGLE", "SAW DOWN", "SAW UP", "SQUARE", "SINE", "SAMPLE HOLD"};
      static int numValues = (int)(sizeof(values) / sizeof(values[0]));
      if (value >= 0 && value < numValues)
         *pTxt = values[value];
      else
         *pTxt = "UNKNOWN";
      return true;
   }

public:
   //parameters:
   int lfoPitchModSens;    //[0..7]
   int lfoPitchModDepth;   //[0..99]
   float controllerModVal; //can be used to overdrive, but max 27 % allowed (min(1.27, modval))
   int lfoSpeed;           //[0..99]

   int lfoAmpModDepth;     //[0..99]
   float lfoAmpModSens;    //[-3..3]
   int lfoDelay;           //[0..99]

   enum {
      eTriangle = 0,       //has to match sysex format spec!
      eSawDown = 1,
      eSawUp = 2,
      eSquare = 3,
      eSine = 4,
      eSampleHold = 5
   };
   int mWaveform;

   static Dx7LfoNode* Create(const ImVec2& pos) {
      Dx7LfoNode* node = (Dx7LfoNode*) ImGui::MemAlloc(sizeof(Dx7LfoNode));
      IM_PLACEMENT_NEW (node) Dx7LfoNode();

      node->init("DX7 Lfo", pos, "", "pitch;amp", TYPE);

      node->fields.addField(&node->lfoPitchModSens, 1, "lfo pitch mod sens", nullptr, 0, 0, 7);
      node->fields.addField(&node->lfoPitchModDepth, 1, "lfo pitch mod depth", nullptr, 0, 0, 99);
      node->fields.addField(&node->controllerModVal, 1, "controller mod val", nullptr, 0, 0, 1.27);
      node->fields.addField(&node->lfoSpeed, 1, "lfo speed", nullptr, 0, 0, 99);
      node->fields.addField(&node->lfoAmpModDepth, 1, "lfo amp mod depth", nullptr, 0, 0, 99);
      node->fields.addField(&node->lfoAmpModSens, 1, "lfo amp mod sens", nullptr, 0, -3.0f, 3.0f);
      node->fields.addField(&node->lfoDelay, 1, "lfo delay", nullptr, 0, 0, 99);
      node->fields.addFieldEnum(&node->mWaveform, 6, &getTextFromEnumIndex, "waveform", "Choose your favourite");

      node->mRenderSampleCounter = 0;
      node->mDelayState = 0;
      node->mDelayTimes[0] = node->mDelayTimes[1] = node->mDelayTimes[2] = 0;
      node->mDelayIncrements[0] = node->mDelayIncrements[1] = node->mDelayIncrements[2] = 0.0f;
      node->mDelayVals[0] = node->mDelayVals[1] = node->mDelayVals[2] = 0.0f;
      node->mDelayValue = 0.0f;
      node->mPhase = 0.0f;
      node->mPhaseStep = 0.0f;
      node->mAmpValue = 0.0f;
      node->mAmpValTarget = 0.0f;
      node->mAmpIncrement = 0.0f;
      node->mAmpModDepth = 0.0f;
      node->pitchVal = 0.0f;
      node->mSampleHoldRandom = 0.0f;

      node->lfoPitchModSens = 0;
      node->lfoPitchModDepth = 50;
      node->controllerModVal = 1.0f;
      node->lfoSpeed = 50;
      node->lfoAmpModDepth = 50;
      node->lfoAmpModSens = 0;
      node->lfoDelay = 50;
      node->mWaveform = eTriangle;

      node->mRandomState.state = 0x853c49e6748fea9bULL;
      node->mRandomState.inc = 0xda3e39cb94b95bdbULL;

      return node;
   }

   void noteOn() {
      mDelayValue = 0;
      mDelayState = 0;      //ONSET
      mPhase = 0;
      pitchVal = 0;
      mAmpValue = 1;
      mAmpValTarget = 1;
      mAmpIncrement = 0;

      mRenderSampleCounter = 0;

      controllerModVal = 0.0f;            //TODO!

      float cLfoFreqTable[] = {
         0.062506,  0.124815,  0.311474,  0.435381,  0.619784,
         0.744396,  0.930495,  1.116390,  1.284220,  1.496880,
         1.567830,  1.738994,  1.910158,  2.081322,  2.252486,
         2.423650,  2.580668,  2.737686,  2.894704,  3.051722,
         3.208740,  3.366820,  3.524900,  3.682980,  3.841060,
         3.999140,  4.159420,  4.319700,  4.479980,  4.640260,
         4.800540,  4.953584,  5.106628,  5.259672,  5.412716,
         5.565760,  5.724918,  5.884076,  6.043234,  6.202392,
         6.361550,  6.520044,  6.678538,  6.837032,  6.995526,
         7.154020,  7.300500,  7.446980,  7.593460,  7.739940,
         7.886420,  8.020588,  8.154756,  8.288924,  8.423092,
         8.557260,  8.712624,  8.867988,  9.023352,  9.178716,
         9.334080,  9.669644, 10.005208, 10.340772, 10.676336,
         11.011900, 11.963680, 12.915460, 13.867240, 14.819020,
         15.770800, 16.640240, 17.509680, 18.379120, 19.248560,
         20.118000, 21.040700, 21.963400, 22.886100, 23.808800,
         24.731500, 25.759740, 26.787980, 27.816220, 28.844460,
         29.872700, 31.228200, 32.583700, 33.939200, 35.294700,
         36.650200, 37.812480, 38.974760, 40.137040, 41.299320,
         42.461600, 43.639800, 44.818000, 45.996200, 47.174400,
      }; 

      float frequency = cLfoFreqTable[lfoSpeed];
      mPhaseStep = 2 * cPI * frequency / LFO_RATE;
      mAmpModDepth = lfoAmpModDepth * 0.01;
      
      mDelayTimes[0] = (LFO_RATE * 0.001753 * pow(lfoDelay, 3.10454)+169.344-168) / 1000;
      mDelayTimes[1] = (LFO_RATE * 0.321877 * pow(lfoDelay, 2.01163)+494.201-168) / 1000;
      mDelayTimes[2] = 0;
      mDelayIncrements[0] = 0;
      mDelayIncrements[1] = 1.0f / (mDelayTimes[1]-mDelayTimes[0]);
      mDelayIncrements[2] = 0;
      mDelayVals[0] = 0;
      mDelayVals[1] = 0;
      mDelayVals[2] = 1;
   }

   void processLfoDelay() {
      switch(mDelayState) {
      case 0:           //ONSET
      case 1:           //RAMP
         mDelayValue += mDelayIncrements[mDelayState];
         if(mRenderSampleCounter / LFO_SAMPLE_PERIOD > mDelayTimes[mDelayState]) {
            mDelayState += 1;
            mDelayValue = mDelayVals[mDelayState];
         }
         break;
      default:          //COMPLETE
         break;
      }
   }

   float renderLfo() {
      float amp;
      switch(mWaveform) {
      case eTriangle:
         if(mPhase < cPI)
            amp = 4 * mPhase * (1/(2*cPI)) - 1;
         else
            amp = 3 - 4 * mPhase * (1/(2*cPI));
         break;
      case eSawDown:
         amp = 1 - 2*mPhase * (1/(2*cPI));
         break;
      case eSawUp:
         amp = 2 * mPhase * (1/(2*cPI))-1;
         break;
      case eSquare:
         amp = (mPhase < cPI ? -1 : 1);
         break;
      case eSine:
         amp = sin(mPhase);
         break;
      case eSampleHold:
         amp = mSampleHoldRandom;
         break;
      default:
         break;
      }
      return amp;
   }

   float renderPitch() {
      if(mRenderSampleCounter % LFO_SAMPLE_PERIOD == 0) {
         float amp = renderLfo();
         
         processLfoDelay();

         amp *= mDelayValue;

         float LFO_PITCH_MOD_TABLE[] = {0.0f, 0.0264f, 0.0534f, 0.0889f, 0.1612f, 0.2769f, 0.4967f, 1.0f};
         float pitchModDepth = 1 + LFO_PITCH_MOD_TABLE[lfoPitchModSens] * (controllerModVal + lfoPitchModDepth/99.0f);
         pitchVal = pow(pitchModDepth, amp);

         float ampSensDepth = abs(lfoAmpModSens) * 0.33333333f;
         float _phase = lfoAmpModSens > 0 ? 1 : -1;

         mAmpValTarget = 1 - ((mAmpModDepth + controllerModVal) * ampSensDepth * (amp * _phase+1)*0.5f);
         mAmpIncrement = (mAmpValTarget - mAmpValue)/LFO_SAMPLE_PERIOD;

         mPhase += mPhaseStep;
         if(mPhase >= 2*cPI) {
            uint32_t rnd = pcg32_random_r(&mRandomState);
            rnd &= 0xffff;       //get down the range to 0..65k
            mSampleHoldRandom = (((float)rnd)/32767.0f)-1.0f;      //generate random number in [-1;+1]
            mPhase -= 2*cPI;
         }
      }
      mRenderSampleCounter += 1;
      return pitchVal;
   }
   float renderAmp() {
      mAmpValue += mAmpIncrement;
      return mAmpValue;
   }

   void renderSamples(int numberSamples) {
      for(int i=0; i<numberSamples; ++i) {
         mWorkBuffers[0][i] = renderPitch();
         mWorkBuffers[1][i] = renderAmp();
      }
   }
};

}; //namespace ImGui

#endif   //#ifndef __NODES_DX7_LFO_H__
