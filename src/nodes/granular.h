



float pitchInput = 0.1f;		// [-1..+1]

float shapeInput += (newshapeinput-shapeinput)*0.1f;
float pitchInput += (newPitchInput-pitchInput)*0.25f;

float softclip(float x) {
	return x / sqrtf(1.0f + x*x);
}


//======================================================================== 
// http://www.rossbencina.com/static/code/granular-synthesis/BencinaAudioAnecdotes310801.pdf
// 
ParabolicEnvelope:
init:
	amplitude = 0;
	rdur = 1.0f / durationSamples
	tdur2 = rdur*rdur
	slope = 4.0 * grainAmplitude * (rdur-rdur2)
	curve = -8*granAmplitude*rdur2
process:
	amplitude = amplitude + slope
	slope = slope + curve


Trapezoidal Envelope:
process:
	attack:
		amplitudeIncrement = grainAmplitude / attackSamples
	sustain:
		amplitudeIncrement = 0
	release:
		amplitudeIncrement = -(grainAmplitude/releaseSamples)

	nextAmplitude = proviousAmplitude + amplitudeIncrement


Raised Cosine Bell Envelope:
	attack:
		amplitude = (1.0 + cos(PI+(PI*(i/attackSamples)*(grainApmplitude / 2.0)
	sustain:
		amplitude = grainAmplitude
	release:
		amplitude = (1+cos(PI*(i/releaseSamples)) * (grainAmplitude/2.0)

	perSample:
		init (w is phase increment, ip initial phase, both in radians):
			b1 = 2.0 * cos(w)
			y1 = sin(ip-w)
			y2 = sin(ip-2.0*w)
		process:
			y0 = b1*y1-y2
			y2 = y1
			y1 = y0
			
