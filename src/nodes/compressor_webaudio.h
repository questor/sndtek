
#ifndef __NODES_COMPRESSOR_WEBAUDIO_H__
#define __NODES_COMPRESSOR_WEBAUDIO_H__

#include "src/utils.h"
#include <math.h>

namespace ImGui {

//taken from https://github.com/voidqk/sndfilter/

//maximum number of samples in the delay buffer
#define COMPRESSOR_MAXDELAY 1024

//samples per update; the compressor works by dividing the input chunks into even smaller sizes,
// and performs heavier calculations after each mini-chunk to adjust the final envelope
#define COMPRESSOR_SPU 32

// not sure what this does exactly, but it is part of the release curve
#define COMPRESSOR_SPACINGDB  5.0f

class CompressorWebaudioNode : public ProcessingNode {
protected:
   CompressorWebaudioNode() : ProcessingNode(1) {}
   static const int TYPE = SNT_COMPRESSOR_WEBAUDIO;

   const char* getTooltip() const {return "CompressorWebAudio";}
   const char* getInfo() const {return "CompressorWebAudio info.\n\n";}

   //parameters:
   float pregain;             //dB, amount to boost the signal before applying compression [0 to 100]
   float threshold;           //dB, level where compression kicks in [-100 to 0]
   float knee;                //dB, width of the knee [0 to 40]
   float ratio;               //unitless, amount to inversely scale the output when applying comp [1 to 20]
   float attack;              //seconds, length of the attack phase [0 to 1]
   float release;             //seconds, length of the release phase [0 to 1]
   float predelay;            //seconds, length of the predelay buffer [0 to 1]
   float releasezone1;        //release zones should be increasing between 0 and 1, and are a fraction
   float releasezone2;        // of the release time depending on the input dB -- these parameters define
   float releasezone3;        // the adaptive release curve, which is discussed in further detail in the
   float releasezone4;        // demo: adaptive-release-curve.html
   float postgain;            //dB, amount of gain to apply after compression [0 to 100]
   float wet;                 //amount to apply the effect [0 completely dry to 1 completely wet]

   //state:
   float meterrelease;
   float linearpregain;
   float linearthreshold;
   float slope;
   float attacksamplesinv;
   float satreleasesamplesinv;
   float dry;
   float k;
   float kneedboffset;
   float linearthresholdknee;
   float mastergain;
   float a,b,c,d;                               // adaptive release polynomial coefficients
   float detectoravg;
   float compgain;
   float maxcompdiffdb;
   int delaybufsize;
   int delaywritepos;
   int delayreadpos;
   float delaybuf[COMPRESSOR_MAXDELAY];      // predelay buffer 

   inline float kneecurve(float x, float k, float linearthreshold) {
      return linearthreshold + (1.0f - expf(-k * (x - linearthreshold))) / k;
   }
   inline float kneeslope(float x, float k, float linearthreshold) {
      return k * x / ((k * linearthreshold + 1.0f) * expf(k * (x - linearthreshold)) - 1.0f);
   }

   inline float compcurve(float x, float k, float slope, float linearthreshold, float linearthresholdknee, float threshold, float knee, float kneedboffset) {
      if(x < linearthreshold)
         return x;
      if(knee <= 0.0f)        //no knee in curve
         return db2linear(threshold + slope * (linear2db(x) - threshold));
      if(x < linearthresholdknee)
         return kneecurve(x, k, linearthreshold);
      return db2linear(kneedboffset + slope * (linear2db(x) - threshold - knee));
   }

   inline float adaptivereleasecurve(float x, float a, float b, float c, float d) {
      //a*x^3 + b*x^2 + c*x + d
      float x2 = x*x;
      return a*x2*x + b*x2 + c*x + d;
   }

   inline float fixf(float v, float def) {
      // fix NaN and infinity values that sneak in... not sure why this is needed, but it is
      if(isnan(v) || isinf(v))
         return def;
      return v;
   }

   void initState() {
      memset(delaybuf, 0, sizeof(float)*COMPRESSOR_MAXDELAY);

      detectoravg = 0.0f;
      compgain = 1.0f;
      maxcompdiffdb = -1.0f;
      delaywritepos = 0;
      delayreadpos = delaybufsize > 1 ? 1 : 0;
   }

   void setupState() {
      delaybufsize = cSampleRate * predelay;
      if(delaybufsize < 1) {
         delaybufsize = 1;
      } else if(delaybufsize > COMPRESSOR_MAXDELAY) {
         delaybufsize = COMPRESSOR_MAXDELAY;
      }

      linearpregain = db2linear(pregain);
      linearthreshold = db2linear(threshold);
      slope = 1.0f / ratio;
      attacksamplesinv = 1.0f / (cSampleRate * attack);
      float releasesamples = cSampleRate * release;
      float satrelease = 0.0025f;                           // in seconds
      satreleasesamplesinv = 1.0f / ((float)cSampleRate * satrelease);
      dry = 1.0f - wet; 

      // calculate knee curve parameters
      k = 5.0f;         // initial guess
      if (knee > 0.0f){ // if a knee exists, search for a good k value
         float xknee = db2linear(threshold + knee);
         float mink = 0.1f;
         float maxk = 10000.0f;
         // search by comparing the knee slope at the current k guess, to the ideal slope
         for (int i = 0; i < 15; i++){
            if (kneeslope(xknee, k, linearthreshold) < slope)
               maxk = k;
            else
               mink = k;
            k = sqrtf(mink * maxk);
         }
         kneedboffset = linear2db(kneecurve(xknee, k, linearthreshold));
         linearthresholdknee = db2linear(threshold + knee);
      }
    
      // calculate a master gain based on what sounds good
      float fulllevel = compcurve(1.0f, k, slope, linearthreshold, linearthresholdknee, threshold, knee, kneedboffset);
      mastergain = db2linear(postgain) * powf(1.0f / fulllevel, 0.6f);

      // calculate the adaptive release curve parameters
      // solve a,b,c,d in `y = a*x^3 + b*x^2 + c*x + d`
      // interescting points (0, y1), (1, y2), (2, y3), (3, y4)
      float y1 = releasesamples * releasezone1;
      float y2 = releasesamples * releasezone2;
      float y3 = releasesamples * releasezone3;
      float y4 = releasesamples * releasezone4;
      a = (-y1 + 3.0f * y2 - 3.0f * y3 + y4) / 6.0f;
      b = y1 - 2.5f * y2 + 2.0f * y3 - 0.5f * y4;
      c = (-11.0f * y1 + 18.0f * y2 - 9.0f * y3 + 2.0f * y4) / 6.0f;
      d = y1; 
   }

public:
   static CompressorWebaudioNode* Create(const ImVec2& pos) {
      CompressorWebaudioNode* node = (CompressorWebaudioNode*) ImGui::MemAlloc(sizeof(CompressorWebaudioNode));
      IM_PLACEMENT_NEW (node) CompressorWebaudioNode();

      node->pregain      = 0.0f;
      node->threshold    = -24.0f;
      node->knee         = 30.0f;
      node->ratio        = 12.0f;
      node->attack       = 0.003f;
      node->release      = 0.250f;
      node->predelay     = 0.006f;
      node->releasezone1 = 0.090f;
      node->releasezone2 = 0.160f;
      node->releasezone3 = 0.420f;
      node->releasezone4 = 0.980f;
      node->postgain     = 0.0f;
      node->wet          = 1.0f;

      node->init("Compressor WebAudio", pos, "freq", "snd", TYPE);

      node->fields.addField(&node->pregain, 1, "pregain", nullptr, 3, 0.0f, 100.0f);
      node->fields.addField(&node->threshold, 1, "threshold", nullptr, 3, -100.0f, 0.0f);
      node->fields.addField(&node->knee, 1, "knee", nullptr, 3, 0.0f, 40.0f);
      node->fields.addField(&node->ratio, 1, "ratio", nullptr, 3, 1.0f, 20.0f);
      node->fields.addField(&node->attack, 1, "attack", nullptr, 3, 0.0f, 1.0f);
      node->fields.addField(&node->release, 1, "release", nullptr, 3, 0.0f, 1.0f);
      node->fields.addField(&node->predelay, 1, "predelay", nullptr, 3, 0.0f, 1.0f);
      node->fields.addField(&node->releasezone1, 1, "releasezone1", nullptr, 3, 0.0f, 1.0f);
      node->fields.addField(&node->releasezone2, 1, "releasezone2", nullptr, 3, 0.0f, 1.0f);
      node->fields.addField(&node->releasezone3, 1, "releasezone3", nullptr, 3, 0.0f, 1.0f);
      node->fields.addField(&node->releasezone4, 1, "releasezone4", nullptr, 3, 0.0f, 1.0f);
      node->fields.addField(&node->postgain, 1, "postgain", nullptr, 3, 0.0f, 100.0f);
      node->fields.addField(&node->wet, 1, "wet", nullptr, 3, 0.0f, 1.0f);

      node->initState();

      return node;
   }

   void renderSamples(int numberSamples) {
      if (mInputNodes.size() != 0) {
         assert(mInputNodes.size() == 1);

         ProcessingNode *inputNode = (ProcessingNode *)mInputNodes[0].node;
         int inputSlot = mInputNodes[0].slot;
         if(inputNode == nullptr)
            return;

         setupState();           //to do per block?
                                 
         int chunks = numberSamples / COMPRESSOR_SPU;
         assert(chunks*COMPRESSOR_SPU == numberSamples);          //no rest allowed!

         float ang90 = cPI * 0.5f;
         float ang90inv = 2.0f / cPI;
         int samplepos = 0;

         for(int ch = 0; ch < chunks+1; ch++) {                      //one block more!
            int samplesToProcess = COMPRESSOR_SPU;
            if(ch == chunks) {                                       //last block?
               samplesToProcess = numberSamples - COMPRESSOR_SPU*chunks;
               if(samplesToProcess == 0) {                              //last block empty?
                  break;
               }
            }

            detectoravg = fixf(detectoravg, 1.0f);
            float desiredgain = detectoravg;
            float scaleddesiredgain = asinf(desiredgain) * ang90inv;
            float compdiffdb = linear2db(compgain / scaleddesiredgain);

            // calculate envelope rate based on whether we're attacking or releasing
            float enveloperate;
            if (compdiffdb < 0.0f){ // compgain < scaleddesiredgain, so we're releasing
               compdiffdb = fixf(compdiffdb, -1.0f);
               maxcompdiffdb = -1; // reset for a future attack mode
               // apply the adaptive release curve
               // scale compdiffdb between 0-3
               float x = (clamp(compdiffdb, -12.0f, 0.0f) + 12.0f) * 0.25f;
               float releasesamples = adaptivereleasecurve(x, a, b, c, d);
               enveloperate = db2linear(COMPRESSOR_SPACINGDB / releasesamples);
            }
            else{ // compresorgain > scaleddesiredgain, so we're attacking
               compdiffdb = fixf(compdiffdb, 1.0f);
               if (maxcompdiffdb == -1 || maxcompdiffdb < compdiffdb)
                  maxcompdiffdb = compdiffdb;
               float attenuate = maxcompdiffdb;
               if (attenuate < 0.5f)
                  attenuate = 0.5f;
               enveloperate = 1.0f - powf(0.25f / attenuate, attacksamplesinv);
            }

            // process the chunk
            for (int chi = 0; chi < samplesToProcess; 
                     chi++, samplepos++, delayreadpos = (delayreadpos + 1) % delaybufsize, delaywritepos = (delaywritepos + 1) % delaybufsize){

               float input = inputNode->mWorkBuffers[inputSlot][samplepos] * linearpregain;
               delaybuf[delaywritepos] = input;

               input = abs(input);

               float attenuation;
               if (input < 0.0001f) {
                  attenuation = 1.0f;
               } else {
                  float inputcomp = compcurve(input, k, slope, linearthreshold, linearthresholdknee, threshold, knee, kneedboffset);
                  attenuation = inputcomp / input;
               }

               float rate;
               if (attenuation > detectoravg){                             // if releasing
                  float attenuationdb = -linear2db(attenuation);
                  if (attenuationdb < 2.0f)
                     attenuationdb = 2.0f;
                  float dbpersample = attenuationdb * satreleasesamplesinv;
                  rate = db2linear(dbpersample) - 1.0f;
               } else {
                  rate = 1.0f;
               }

               detectoravg += (attenuation - detectoravg) * rate;
               if (detectoravg > 1.0f) {
                  detectoravg = 1.0f;
               }
               detectoravg = fixf(detectoravg, 1.0f);

               if (enveloperate < 1) {                                     // attack, reduce gain
                  compgain += (scaleddesiredgain - compgain) * enveloperate;
               } else {                                                    // release, increase gain
                  compgain *= enveloperate;
                  if (compgain > 1.0f) {
                     compgain = 1.0f;
                  }
               }

               // the final gain value!
               float premixgain = sinf(ang90 * compgain);
               float gain = dry + wet * mastergain * premixgain;

               // apply the gain
               mWorkBuffers[0][samplepos] = delaybuf[delayreadpos] * gain;
            }
         }
      }
   }

 };

}; //namespace ImGui

#endif   //#ifndef __NODES_COMPRESSOR_WEBAUDIO_H__

