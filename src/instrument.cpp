
#include "instrument.h"

#include "importsysex.h"


void Instrument::importSysex(char *filename) {
   mNodeGraphEditor->clear();
   ImportSysex imp;
   imp.importFromFile(filename, 0, *this);
}
