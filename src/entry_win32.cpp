
#include "entry_win32.h"

#include <stdio.h>

static EntryWin32 *gEntryWin32 = 0;

EntryWin32::EntryWin32() {
   gEntryWin32 = this;
}

extern LRESULT ImGui_ImplDX10_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
LRESULT WINAPI WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    if (ImGui_ImplDX10_WndProcHandler(hWnd, msg, wParam, lParam))
        return true;

    switch (msg)
    {
    case WM_SIZE:
        if (gEntryWin32->g_pd3dDevice != NULL && wParam != SIZE_MINIMIZED)
        {
            ImGui_ImplDX10_InvalidateDeviceObjects();
            gEntryWin32->CleanupRenderTarget();
            gEntryWin32->g_pSwapChain->ResizeBuffers(0, (UINT)LOWORD(lParam), (UINT)HIWORD(lParam), DXGI_FORMAT_UNKNOWN, 0);
            gEntryWin32->CreateRenderTarget();
            ImGui_ImplDX10_CreateDeviceObjects();
        }
        return 0;
    case WM_SYSCOMMAND:
        if ((wParam & 0xfff0) == SC_KEYMENU) // Disable ALT application menu
            return 0;
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        return 0;
    }
    return DefWindowProc(hWnd, msg, wParam, lParam);
} 

void EntryWin32::init() {
    wc = { sizeof(WNDCLASSEX), CS_CLASSDC, WndProc, 0L, 0L, GetModuleHandle(NULL), NULL, LoadCursor(NULL, IDC_ARROW), NULL, NULL, _T("ImGui Example"), NULL };
    RegisterClassEx(&wc);
    hwnd = CreateWindow(_T("ImGui Example"), _T("ImGui DirectX10 Example"), WS_OVERLAPPEDWINDOW, 100, 100, 1280, 800, NULL, NULL, wc.hInstance, NULL);

    // Initialize Direct3D
    if (CreateDeviceD3D(hwnd) < 0) {
        CleanupDeviceD3D();
        UnregisterClass(_T("ImGui Example"), wc.hInstance);
        return;
    }

    // Show the window
    ShowWindow(hwnd, SW_SHOWDEFAULT);
    UpdateWindow(hwnd);

    // Setup ImGui binding
    ImGui_ImplDX10_Init(hwnd, g_pd3dDevice); 

    // Load Fonts
    // (there is a default font, this is only if you want to change it. see extra_fonts/README.txt for more details)
    //ImGuiIO& io = ImGui::GetIO();
    //io.Fonts->AddFontDefault();
    //io.Fonts->AddFontFromFileTTF("../../extra_fonts/Cousine-Regular.ttf", 15.0f);
    //io.Fonts->AddFontFromFileTTF("../../extra_fonts/DroidSans.ttf", 16.0f);
    //io.Fonts->AddFontFromFileTTF("../../extra_fonts/ProggyClean.ttf", 13.0f);
    //io.Fonts->AddFontFromFileTTF("../../extra_fonts/ProggyTiny.ttf", 10.0f);
    //io.Fonts->AddFontFromFileTTF("c:\\Windows\\Fonts\\ArialUni.ttf", 18.0f, NULL, io.Fonts->GetGlyphRangesJapanese());
}

void EntryWin32::deinit() {
  ImGui_ImplDX10_Shutdown();
  CleanupDeviceD3D();
  UnregisterClass(_T("ImGui Example"), wc.hInstance);
}

bool EntryWin32::doMainLoop() {
    static MSG msg;
    ZeroMemory(&msg, sizeof(msg));
    while((msg.message != WM_QUIT) && (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))) {
       TranslateMessage(&msg);
       DispatchMessage(&msg);
    }
    return (msg.message == WM_QUIT);
}

void EntryWin32::imguiBeginFrame() {
    ImGui_ImplDX10_NewFrame(); 
}

void EntryWin32::imguiEndFrame() {
      ImVec4 clear_col = ImColor(114, 144, 154); 
      g_pd3dDevice->ClearRenderTargetView(g_mainRenderTargetView, (float*)&clear_col); 
      ImGui::Render();
      g_pSwapChain->Present(0, 0); 
}


void EntryWin32::CreateRenderTarget() {
    DXGI_SWAP_CHAIN_DESC sd;
    g_pSwapChain->GetDesc(&sd);

    // Create the render target
    ID3D10Texture2D* pBackBuffer;               
    D3D10_RENDER_TARGET_VIEW_DESC render_target_view_desc;
    ZeroMemory(&render_target_view_desc, sizeof(render_target_view_desc));
    render_target_view_desc.Format = sd.BufferDesc.Format;
    render_target_view_desc.ViewDimension = D3D10_RTV_DIMENSION_TEXTURE2D;
    g_pSwapChain->GetBuffer(0, __uuidof(ID3D10Texture2D), (LPVOID*)&pBackBuffer);
    g_pd3dDevice->CreateRenderTargetView(pBackBuffer, &render_target_view_desc, &g_mainRenderTargetView);
    g_pd3dDevice->OMSetRenderTargets(1, &g_mainRenderTargetView, NULL);
    pBackBuffer->Release(); 
}

void EntryWin32::CleanupRenderTarget() {
    if (g_mainRenderTargetView) { 
      g_mainRenderTargetView->Release(); g_mainRenderTargetView = NULL; 
    } 
}    

HRESULT EntryWin32::CreateDeviceD3D(HWND hWnd) {
    // Setup swap chain
    DXGI_SWAP_CHAIN_DESC sd;
    {
        ZeroMemory(&sd, sizeof(sd));
        sd.BufferCount = 2;
        sd.BufferDesc.Width = 0;
        sd.BufferDesc.Height = 0;
        sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
        sd.BufferDesc.RefreshRate.Numerator = 60;
        sd.BufferDesc.RefreshRate.Denominator = 1;
        sd.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
        sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
        sd.OutputWindow = hWnd;
        sd.SampleDesc.Count = 1;
        sd.SampleDesc.Quality = 0;
        sd.Windowed = TRUE;
        sd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
    }

    UINT createDeviceFlags = 0;
    //createDeviceFlags |= D3D10_CREATE_DEVICE_DEBUG;
    if (D3D10CreateDeviceAndSwapChain(NULL, D3D10_DRIVER_TYPE_HARDWARE, NULL, createDeviceFlags, D3D10_SDK_VERSION, &sd, &g_pSwapChain, &g_pd3dDevice) != S_OK)
        return E_FAIL;

    // Setup rasterizer
    {
        D3D10_RASTERIZER_DESC RSDesc;
        memset(&RSDesc, 0, sizeof(D3D10_RASTERIZER_DESC));
        RSDesc.FillMode = D3D10_FILL_SOLID;
        RSDesc.CullMode = D3D10_CULL_NONE;
        RSDesc.FrontCounterClockwise = FALSE;
        RSDesc.DepthBias = 0;
        RSDesc.SlopeScaledDepthBias = 0.0f;
        RSDesc.DepthBiasClamp = 0;
        RSDesc.DepthClipEnable = TRUE;
        RSDesc.ScissorEnable = TRUE;
        RSDesc.AntialiasedLineEnable = FALSE;
        RSDesc.MultisampleEnable = (sd.SampleDesc.Count > 1) ? TRUE : FALSE;

        ID3D10RasterizerState* pRState = NULL;
        g_pd3dDevice->CreateRasterizerState(&RSDesc, &pRState);
        g_pd3dDevice->RSSetState(pRState);
        pRState->Release();
    }

    CreateRenderTarget();

    return S_OK; 
}

void EntryWin32::CleanupDeviceD3D() {
    CleanupRenderTarget();
    if (g_pSwapChain) { g_pSwapChain->Release(); g_pSwapChain = NULL; }
    if (g_pd3dDevice) { g_pd3dDevice->Release(); g_pd3dDevice = NULL; } 
} 

bool EntryWin32::isKeyPressed(KeyCode keycode) {
    int osCode;
    switch(keycode) {
    case Entry::KeyCode::eESC:
        osCode = VK_ESCAPE;
        break;
    default:
        break;
    }
    return GetAsyncKeyState(osCode)>0;
}
