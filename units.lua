
require "tundra.syntax.glob"
require "tundra.syntax.files"
local native = require "tundra.native"

local pkgconfig = require "tundra.syntax.pkgconfig"
local glfw3 = pkgconfig.Configure("glfw3", ExternalLibrary)


Program {
   Name = "SndTek",
   Sources = {
      "src/main.cpp",
      "src/player.cpp",
      "src/instrument.cpp",
      "src/random.cpp",
      "src/nodegrapheditor.cpp",
      "src/nodes/dx7_env.cpp",
      "src/importmidi.cpp",
      {Config="linux_x86-*-*"; "src/entry_linux_glfw.cpp", "src/extlibs/entry/libs/gl3w/GL/gl3w.c"},
      {Config="win32-*-*"; "src/entry_win32.cpp"},

      Glob {
         Dir = "src/extlibs/soloud/src/core",
         Extensions = {
            ".h", ".cpp"
         },
      },
      Glob {
         Dir = "src/extlibs/soloud/src/filter",
         Extensions = {
            ".h", ".cpp"
         },
      },
      {Config="win32-*-*";
         Glob {
            Dir = "src/extlibs/soloud/src/backend/winmm",
            Extensions = {
               ".h", ".c",
            },
         },
      },
      {Config="linux_x86-*-*";
         Glob {
            Dir = "src/extlibs/soloud/src/backend/alsa",
            Extensions = {
               ".h", ".cpp",
            },
         },
      },
      "src/extlibs/imgui/imgui.cpp",
      "src/extlibs/imgui/imgui_draw.cpp",
--      Glob { Dir = "src/extlibs/imgui/flix_addons/imguifilesystem", Extensions = { ".h", ".cpp" }, },
--      Glob { Dir = "src/extlibs/imgui/flix_addons/imguinodegrapheditor", Extensions = { ".h", ".cpp" }, },
--      Glob { Dir = "src/extlibs/imgui/flix_addons/imguistyleserializer", Extensions = { ".h", ".cpp" }, },
--      Glob { Dir = "src/extlibs/imgui/flix_addons/imguitoolbar", Extensions = { ".h", ".cpp" }, },
      "src/extlibs/nativefiledialog/nfd_common.c",
      {Config="linux_x86-*-*"; 
         "src/extlibs/entry/imgui_impl_glfw.cpp",
         "src/extlibs/entry/imgui_impl_opengl3.cpp",
         "src/extlibs/nativefiledialog/nfd_gtk.c",
      },
      {Config="win32-*-*"; 
         "src/extlibs/entry/directx10_example/imgui_impl_dx10.cpp",
         "src/extlibs/nativefiledialog/nfd_win.cpp",
      },
   },
   Libs = {
      {Config="win32-*-*"; "User32.lib", "d3d10.lib", "d3dcompiler.lib", "shell32.lib"},
      -- "asan" as first library to enable memchecking
      {Config="linux_x86-*-*"; "glfw", "dl", "pthread", "X11", "Xxf86vm", "Xrandr", "Xi", "Xcursor", "GL", "asound", "gtk-x11-2.0", "gobject-2.0", "glib-2.0"},
   },
   Env = {
      PROGOPTS = {
         --{ "/Subsystem:WINDOWS", }
      },
      CPPDEFS = {
         { 
            "SOLOUD_STATIC", 
               {Config="linux_x86-*-*"; "WITH_ALSA"},
               {Config="win32-*-*"; "WITH_WINMM"},
            "UNICODE", "_UNICODE", 
            "IMGUI_INCLUDE_IMGUI_USER_H", "IMGUI_INCLUDE_IMGUI_USER_INL",
            "IMGUINODEGRAPHEDITOR_NOTESTDEMO",
         },
      },
      CPPPATH = {
         ".", 
         "src/extlibs/soloud/include",
         "src/extlibs/imgui", 
         "src/extlibs/reflect",
         "src",
         {Config="linux_x86-*-*"; "src/extlibs/entry", "src/extlibs/entry/libs/gl3w", "src/extlibs/portaudio/src/os/unix", 
                                  "/usr/include/gtk-2.0", "/usr/include/glib-2.0", "/usr/lib/x86_64-linux-gnu/glib-2.0/include/", "/usr/include/cairo/",
                                  "/usr/include/pango-1.0/", "/usr/lib/x86_64-linux-gnu/gtk-2.0/include/", "/usr/include/gdk-pixbuf-2.0/",
                                  "/usr/include/atk-1.0/", "/usr/include/harfbuzz/", "/usr/local/include/",
},
         {Config="win32-*-*"; "src/extlibs/entry/directx10_example", "src/extlibs/portaudio/src/os/win", }, 
      },
   },
}
Default "SndTek"
